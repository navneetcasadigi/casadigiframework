// Copyright (c) 2017 CasaDigi. All rights reserved.

import UIKit
import CasadigiFramework
// Sorting
let AlphabetsSorting = "Alphabets"
let ReleaseDataSorting = "Release Date"
let DateAddedSorting = "Date Added"
let RecentlyAddedOfflineMode = "Recently Added"

// Font
let ProximaNovaRegular: String = "ProximaNova-Regular"
let ProximaNovaSemibold: String = "ProximaNova-Semibold"
let ProximaNovaLight: String = "ProximaNova-Light"


//Request API :
let GetHostNameAPI: String = "https://studio.casadigi.com/casadigi-cloud-api/Info"

// Socket Communication Port
let communicationPort: UInt16 = 7777

// Singleton Instance
//let contentHandler: ContentHandler = ContentHandler.shared
let databaseInterface: DatabaseInterface = DatabaseInterface.shared
let roomManager: RoomManager = RoomManager.shared
//var socketConnection: SocketConnection = SocketConnection.shared
// Disabled Fast Image Cache Generation
// let fastImageCache: FICImageCache = FICImageCache.shared()


// Cloud API support version
let maxApiVersionSupportByApp: Double = 2.2

// Application Configuration
var isDemoMode: Bool {
    set {
        UserDefaults.standard.set(newValue, forKey: "DemoMode")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.bool(forKey: "DemoMode")
    }
    // return UserDefaults.standard.bool(forKey: DemoMode)
}

// Partner Logo Permission
var isPartnerLogoAvailable: Bool {
    set {
        UserDefaults.standard.set(newValue, forKey: "PartnerLogo")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.bool(forKey: "PartnerLogo")
    }
}
// VENDORID
let VENDORID = UIDevice.current.identifierForVendor!.uuidString.replacingOccurrences(of: "-", with: "")

// Java Package Version
var latestJavaPackageVersion: Int {
    set {
        UserDefaults.standard.set(newValue, forKey: "LatestJavaPackageVersion")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.integer(forKey: "LatestJavaPackageVersion")
    }
}

// Entertainment Package Version
var latestEntertainmentPackageVersion: Int {
    set {
        UserDefaults.standard.set(newValue, forKey: "LatestEntertainmentPackageVersion")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.integer(forKey: "LatestEntertainmentPackageVersion")
    }
}

// Configuration Package Version
var latestConfigurationPackageVersion: Int {
    set {
        UserDefaults.standard.set(newValue, forKey: "LatestConfigurationPackageVersion")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.integer(forKey: "LatestConfigurationPackageVersion")
    }
}

// Consolidated Package Version
var currentPackageVersion: Int {
    set {
        UserDefaults.standard.set(newValue, forKey: "CurrentPackageVersion")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.integer(forKey: "CurrentPackageVersion")
    }
}

// DemoContent Host URL
var demoContentHostUrl: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "DemoContentHostUrl")
        UserDefaults.standard.synchronize()
    }
    get {
        if let domoUrl = UserDefaults.standard.string(forKey: "DemoContentHostUrl"){
            return domoUrl
        }
        return "" //Need to update
        
    }
}


var sqlite_path: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "sqlite_path")
        UserDefaults.standard.synchronize()
    }
    get {
        return UserDefaults.standard.string(forKey: "sqlite_path")!
    }
}

var lastEntUpdateTimestamp: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "LastEntUpdateTime")
    }
    
    get {
        if UserDefaults.standard.string(forKey: "LastEntUpdateTime") == nil {
            let date = Date()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.locale = Locale.current
            dateFormatter.timeZone = TimeZone(abbreviation: "IST")
            let datestr = dateFormatter.string(from: date)
            //          print("datestr \(datestr)")
            //        dateFormatter.locale = Locale.current
            //        let dateNew = dateFormatter.date(from: datestr)
            //        print("str date \(Date().description(with: Locale.current))")
            
            UserDefaults.standard.set(datestr, forKey: "LastEntUpdateTime")
        }
        return UserDefaults.standard.string(forKey: "LastEntUpdateTime")!
    }
}

var pushToken: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "pushToken")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "pushToken") != nil)
        {
            return UserDefaults.standard.string(forKey: "pushToken")!
        }
        else
        {
            return "NA"
        }
    }
}

var TVSeriesSorting: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "TVSeriesSorting")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "TVSeriesSorting") != nil)
        {
            return UserDefaults.standard.string(forKey: "TVSeriesSorting")!
        }
        else
        {
            return DateAddedSorting
        }
    }
}

var MovieSorting: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "MovieSorting")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "MovieSorting") != nil)
        {
            return UserDefaults.standard.string(forKey: "MovieSorting")!
        }
        else
        {
            return DateAddedSorting
        }
    }
}

var SortingSongs: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "SortingSongs")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "SortingSongs") != nil)
        {
            return UserDefaults.standard.string(forKey: "SortingSongs")!
        }
        else
        {
            return "Alphabetically ASC"
        }
    }
}

var SortingGenre: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "SortingGenre")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "SortingGenre") != nil)
        {
            return UserDefaults.standard.string(forKey: "SortingGenre")!
        }
        else
        {
            return "Alphabetically ASC"
        }
    }
}

var SortingAlbums: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "SortingAlbums")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "SortingAlbums") != nil)
        {
            return UserDefaults.standard.string(forKey: "SortingAlbums")!
        }
        else
        {
            return "Alphabetically ASC"
        }
    }
}
var SortingArtist: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "SortingArtist")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "SortingArtist") != nil)
        {
            return UserDefaults.standard.string(forKey: "SortingArtist")!
        }
        else
        {
            return "Alphabetically ASC"
        }
    }
}
/*
 
 
 
 
 */

var TVEpisodeSorting: Int {
    set {
        UserDefaults.standard.set(newValue, forKey: "TVEpisodeSorting")
        UserDefaults.standard.synchronize()
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "TVEpisodeSorting") != nil)
        {
            return UserDefaults.standard.integer(forKey: "TVEpisodeSorting")
        }
        else
        {
            return 1
        }
    }
}

var casaDigiStudioVersion: String {
    set {
        UserDefaults.standard.set(newValue, forKey: "casaDigiStudioVersion")
    }
    
    get {
        if(UserDefaults.standard.string(forKey: "casaDigiStudioVersion") == nil)
        {
            
            UserDefaults.standard.set("2.0", forKey: "casaDigiStudioVersion")
            
        }
        return UserDefaults.standard.string(forKey: "casaDigiStudioVersion")!
    }
}


var languageCode: String {
    return UserDefaults.standard.string(forKey: "LanguageCode") ?? "EN"
}

var isConfigurationAvailable: Bool {
    return FileManager.default.fileExists(atPath: applicationBundleAppContentDirectoryURL.appendingPathComponent("configuration.sqlite").path)
}

var isBackgroundUpdateRunning: Bool = false
//{
//    set {
//        print("ibgupdate \(isBackgroundUpdateRunning)")
//        UIApplication.shared.isNetworkActivityIndicatorVisible = newValue
//    }
//
//    get {
//        return UIApplication.shared.isNetworkActivityIndicatorVisible
//    }
//}

// Application Constant
var serverContentDirectoryName: String {
    
    return "CasadigiStudio"
    
}

let applicationBundleAppContentDirectoryURL: URL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0].appendingPathComponent("App Content")
//let applicationDelegate: ApplicationDelegate = UIApplication.shared.delegate as! ApplicationDelegate

// Feature Constant
var kCasaDigiModuleLights: String {
    
    return "light"
    
}

var kCasaDigiModuleCurtains: String {
    
    return "curtain"
    
}

let kCasaDigiModuleAC: String = "ac"
let kCasaDigiModuleFan: String = "fan"
var kCasaDigiModuleTelevision: String {
    
    return "channelprovider"
    
}

let kCasaDigiModuleVideos: String = "entertainment"
var kCasaDigiModuleMovies: String {
    
    return "movie"
    
}

let kCasaDigiModulePhotos: String = "photo"

var kCasaDigiModulePersonalVideos: String {
    
    return "personalvideo"
    
}

var kCasaDigiModuleTVShows: String {
    
    return "tvshow"
    
}



var kCasaDigiModuleDvd: String {
    
    return "discplayer"
    
}

let kCasaDigiModuleQube: String = "qube"
let kCasaDigiModuleDigitalMedia: String = "digitalmedia"
let kCasaDigiModuleDigitalCinema: String = "digitalcinema"
let kCasaDigiModuleVideoConfrencing: String = "videoconfrencing"
let kCasaDigiModuleVideoWall: String = "videowall"
let kCasaDigiModuleMusic: String = "music"
let kCasaDigiModuleFM: String = "fm"
let kCasaDigiModuleRadio: String = "radio"
let kCasaDigiModulewebRadio: String = "webradio"
let kCasaDigiModuleMultiZoneMusic: String = "multizonemusic"
var kCasaDigiDeviceiPlayer: String {
    
    return "videoondemand"
    
}

var kCasaDigiDeviceCasaPlayer: String {
    
    return "musicondemand"
    
}

let kCasaDigiDeviceAirPlay: String = "airplay"
let kCasaDigiModuleSurveillance: String = "surveillance"
var kCasaDigiModuleHomeAmbiance: String {
    
    return "homemood"
    
}




var kCasaDigiModuleUtilities: String
{
    
    return "utility"
    
}

let kCasaDigiModuleFireAlarm: String = "firealarm"
let kCasaDigiModuleDoorSensor: String = "doorsensor"
let kCasaDigiModuleEquipment: String = "equipment"

// Directory Name
let kDirectoryRoomsIconsDefault: String = "Rooms Icons - Default"
let kDirectoryRoomsPartnerLogo: String = "Partner Logo"
let kDirectoryRoomsIconsSelected: String = "Rooms Icons - Selected"
let kDirectoryRoomsIconsImages: String = "Rooms Icons - Images"
let kDirectoryLightsIconPanoDefault: String = "Lights Icons Pano - Default"
let kDirectoryLightsIconPanoSelected: String = "Lights Icons Pano - Selected"
let kDirectoryLightsIconNonPanoDefault: String = "Lights Icons Non-Pano - Default"
let kDirectoryLightsIconNonPanoSelected: String = "Lights Icons Non-Pano - Selected"
let kDirectoryMoodsIconDefault: String = "Moods Icons - Default"
let kDirectoryMoodsIconSelected: String = "Moods Icons - Selected"
let kDirectoryAmbienceIconDefault: String = "Ambience Icons - Default"
let kDirectoryAmbienceIconSelected: String = "Ambience Icons - Selected"
let kDirectoryLightsImages: String = "Lights Images"
let kDirectoryCurtainsImages: String = "Curtains Images"
let kDirectoryTVChannelThumbs: String = "TV Channels"
let kDirectorySurveillanceImages: String = "Surveillance Images"
let kDirectoryUtilitiesIcons: String = "Utilities Icons"
let kDirectoryACModeImages: String = "AC Mode Images"
let kDirectoryMoviePosters: String = "Movie Posters"
let kDirectoryPhotos: String = "photos" //this is just for offline case
let kDirectoryPersonalVideos: String = "PersonalVideos"
let kDirectoryTVShowsSeriesPosters: String = "TV-Shows Series Posters"
let kDirectoryTVShowsEpisodesPosters: String = "TV-Shows Episodes Posters"
let kDirectoryDigitalMediaInner: String = "Digital Media Inner"
let kDirectoryDigitalMediaOuter: String = "Digital Media Outer"

let kDirectoryVideosMedia: String = "Videos Media"

let kDirectoryWebRadioStationThumbs: String = "Web Radio StationThumbs"
let kDirectoryMusicAlbumThumbs: String = "Music Album Thumbs"
let kDirectoryWebRadioOfflineStationThumbs: String = "Radio Station Thumbs"
let kDirectoryMusicSongThumbs: String = "Music Song Thumbs"
let kDirectoryMusicSongLargeThumbs: String = "Music Song Large Thumbs"
let kDirectoryMusicSubcatThumbs: String = "Music Subcat Thumbs"
let kDirectoryFMChannelThumbs: String = "FM Channels"
let kDirectoryLanguageContent: String = "Language"
let kDirectoryUsersImagesLarge: String = "UsersLarge"
let kDirectoryUsersImagesSmall: String = "UsersSmall"

// Push Update
let kBackgroundConfigurationUpdate: String = "configuration"
let kBackgroundEntertainmentUpdate: String = "entertainment"
let kBackgroundAutomationUpdate: String = "automation"
let kBackgroundUsersContentUpdate: String = "user"
let kBackgroundIconsUpdate: String = "icons"
let kBackgroundImagesUpdate: String = "images"
let kBackgroundTVChannelsUpdate: String = "tvchannels"
let kBackgroundPersonalVideoUpdate: String = "personalvideos"
let kBackgroundTVShowsUpdate: String = "tvshows"
let kBackgroundTVShowsPosterUpdate: String = "tvshowposter"
let kBackgroundPhotosUpdate: String = "photos"
let kBackgroundMovieThumbsUpdate: String = "moviealbumart"
let kBackgroundMusicThumbsUpdate: String = "musicalbumart"
let kBackgroundEntertainmentAll: String = "entertainmentall"
let kBackgroundMusicContentUpdate: String = "musicContent"
let kBackgroundMovieContentUpdate: String = "moviewContent"
let kBackgroundTvShowContentUpdate: String = "tvshowContent"
let kBackgroundPVContentUpdate: String = "pvContent"
let kBackgroundPhotoContentUpdate: String = "photoContent"


// Offline Dashboard Status String
var dashbOfflineNowPlayingType = ""
var dashbOfflineNowplayingFile = ""

// User Login Info
let kLoggedInUserInfoChanged: String = "LoggedInUserInfoChanged"
let kLoggedInUserInfoUpdated: String = "LoggedInUserInfoUpdated"

// Utility Function

//func isNetworkReachable() -> Bool {
//    return (Reachability()!.currentReachabilityStatus != Reachability.NetworkStatus.notReachable)
//}

func isInternetAvailble() -> Bool{
    var status : Bool = false
    let scriptUrl = URL.init(string: "https://www.google.com")
    var data: Data? = nil
    if let anUrl = scriptUrl {
        do {
            data = try Data.init(contentsOf: anUrl)
        }catch {
            status = false
        }
    }
    if data != nil {
        status = true
    } else {
        status = false
    }
    return status
}

func isHostConnected(_ hostAddress: String) -> Bool {
    let request = NSMutableURLRequest(url: URL(string: hostAddress)!)
    // Set timeout to 10 second to check given IP.
    request.timeoutInterval = 10.0
    //request.httpMethod = "HEAD"
    
    var responseCode: Int = -1
    let group: DispatchGroup = DispatchGroup()
    group.enter()
    
    URLSession.shared.dataTask(with: request as URLRequest) {
        data, response, error in
        
        if error != nil {
            #if DEBUG
            print("#ERROR", #file, #function, "Line#", #line, ": ", error?.localizedDescription as Any)
            #endif
            
            
        }
        if(response != nil)
        {
            if let httpResponse: HTTPURLResponse = response! as? HTTPURLResponse {
                responseCode = httpResponse.statusCode
            }
        }
        group.leave()
        
        }.resume()
    
    _ = group.wait(timeout: DispatchTime.distantFuture)
    
    return (responseCode == 200)
}

func getIPAddress() -> String? {
    var address: String?
    var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
    if getifaddrs(&ifaddr) == 0 {
        var ptr = ifaddr
        while ptr != nil {
            defer {
                ptr = ptr?.pointee.ifa_next
            }
            let interface = ptr?.pointee
            let addrFamily = interface?.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                if let name = String(validatingUTF8: (interface?.ifa_name)!), name == "en0" {
                    var addr = interface?.ifa_addr.pointee
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(&addr!, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                    
                    if addrFamily == UInt8(AF_INET) {
                        address = String(cString: hostname)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
    }
    
    return address
}

func getCurrentTimeStamp() -> String {
    let date = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    dateFormatter.locale = Locale.current
    dateFormatter.timeZone = TimeZone(abbreviation: "IST")
    let datestr = dateFormatter.string(from: date)
    
    return datestr
}

func urlEncodeString(_ str: String) -> String {
    //    let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted)
    
    let characters: NSMutableCharacterSet = (CharacterSet.urlQueryAllowed as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
    characters.removeCharacters(in: "!*'();@&=+$,?%#[] ")
    
    guard let encodedString: String = str.addingPercentEncoding(withAllowedCharacters: characters as CharacterSet) else {
        return str
    }
    
    return encodedString
}

func getLingualPlaceHolder(placeholderText: String) -> String
{
    var placeholderText = placeholderText
    let languageInfo = databaseInterface.languageInfo
    if let languageInfoDictionary = languageInfo[placeholderText] as? [String:String],
        let lingualText = languageInfoDictionary[languageCode] {
        placeholderText = lingualText
    }
    return placeholderText
}

func resizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
    let sourceImage: UIImage = image
    var newImage: UIImage? = nil
    let imageSize: CGSize = sourceImage.size
    let width: CGFloat = imageSize.width
    let height: CGFloat = imageSize.height
    let targetWidth: CGFloat = targetSize.width
    let targetHeight: CGFloat = targetSize.height
    var scaleFactor: CGFloat = 0.0
    var scaledWidth: CGFloat = targetWidth
    var scaledHeight: CGFloat = targetHeight
    var thumbnailPoint: CGPoint = CGPoint(x: 0.0, y: 0.0)
    
    if imageSize.equalTo(targetSize) == false {
        let widthFactor: CGFloat = targetWidth / width
        let heightFactor: CGFloat = targetHeight / height
        
        if widthFactor > heightFactor {
            scaleFactor = widthFactor
        } else {
            scaleFactor = heightFactor
        }
        
        scaledWidth = width * scaleFactor
        scaledHeight = height * scaleFactor
        
        if widthFactor > heightFactor {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5
        } else {
            if widthFactor < heightFactor {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5
            }
        }
        
        UIGraphicsBeginImageContext(targetSize)
        
        var thumbnailRect = CGRect.zero
        thumbnailRect.origin = thumbnailPoint
        thumbnailRect.size.width  = scaledWidth
        thumbnailRect.size.height = scaledHeight
        sourceImage.draw(in: thumbnailRect)
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
    }
    
    
    
    
    if(newImage == nil)
    {
        newImage = sourceImage
    }
    return newImage!
}

// Get max supported version for Cloud communication
func setMaxSupportedVersion(jsonDictionaryVersion : [Double])
{
    let maxVersionApiSupport = jsonDictionaryVersion.max()!
    // If App and studio have same max version
    if maxVersionApiSupport == maxApiVersionSupportByApp
    {
        casaDigiStudioVersion = String(describing: maxApiVersionSupportByApp)
    }
        // If App support hieger version than studio
    else if maxVersionApiSupport > maxApiVersionSupportByApp
    {
        casaDigiStudioVersion = String(describing: maxApiVersionSupportByApp)
    }
        // If App support lower version than studio
    else if maxVersionApiSupport < maxApiVersionSupportByApp
    {
        casaDigiStudioVersion = String(describing: maxVersionApiSupport)
    }
}


/**
 Convert Second to HoursMinutesSeconds
 
 - Parameters:
 - seconds: The Int value of seconds
 */

func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
    return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

/**
 Convert HoursMinutesSeconds to second
 */
func second(forTime string: String) -> Int {
    let components = string.components(separatedBy: "-")
    let hours = Int(components[0]) ?? 0
    let minutes = Int(components[1]) ?? 0
    let seconds = Int(components[2]) ?? 0
    return (hours * 60 * 60) + (minutes * 60) + seconds
}

/**
 TO Resize image into target size
 - Parameters:
 - image: The Int value of seconds
 - targetSize: The required size for image
 */
func RBResizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    let widthRatio = targetSize.width/image.size.width
    let heightRatio = targetSize.height/image.size.height
    var newSize: CGSize
    
    if widthRatio > heightRatio {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    }
    
    let rect = CGRect(x:0.0,y: 0.0,width: newSize.width,height: newSize.height)
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}




