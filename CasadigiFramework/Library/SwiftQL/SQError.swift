// Copyright (c) 2016 Ryan Fowler. All rights reserved.


public struct SQError {
    
    public static func printSQLError(_ main: String, errCode: Int32, errMsg: String?) {
        print("SwiftData Error -> \(main)")
        print("                -> Code: \(errCode) - \(getSQLErrorMsg(errCode))")
        if let msg = errMsg {
            print("                -> Detail: \(msg)")
        }
        getAdvice(errCode)
    }
    
    public static func printError(_ main: String, next: String ...) {
        print("SwiftData Error -> \(main)")
        for item in next {
            print("                -> \(item)")
        }
    }
    
    public static func printWarning(_ main: String, next: String ...) {
        print("SwiftData Warning -> \(main)")
        for item in next {
            print("                  -> \(item)")
        }
    }
    
    private static func getAdvice(_ errCode: Int32) {
        
        if errCode == 27 || errCode == 7 {
            print("                -> Possible cause: Did you call open()?")
        }
        
        if errCode == 5 || errCode == 6 {
            print("                -> Possible cause: Only one 'update' can occur at a time")
        }
        
    }
    
    private static func getSQLErrorMsg(_ code: Int32) -> String {
        
        switch code{
            
            //SQLite error codes and descriptions as per: http://www.sqlite.org/c3ref/c_abort.html
        case 0: //
            return "Successful result"
        case 1:
            return "SQL error or missing database"
        case 2:
            return "Internal logic error in SQLite"
        case 6:
            return "Access permission denied"
        case 4:
            return "Callback routine requested an abort"
        case 5:
            return "The database file is locked"
        case 6:
            return "A table in the database is locked"
        case 7:
            return "A malloc() failed"
        case 8:
            return "Attempt to write a readonly database"
        case 9:
            return "Operation terminated by sqlite3_interrupt()"
        case 10:
            return "Some kind of disk I/O error occurred"
        case 11:
            return "The database disk image is malformed"
        case 12:
            return "Unknown opcode in sqlite3_file_control()"
        case 13:
            return "Insertion failed because database is full"
        case 14:
            return "Unable to open the database file"
        case 15:
            return "Database lock protocol error"
        case 16:
            return "Database is empty"
        case 17:
            return "The database schema changed"
        case 18:
            return "String or BLOB exceeds size limit"
        case 19:
            return "Abort due to constraint violation"
        case 20:
            return "Data type mismatch"
        case 21:
            return "Library used incorrectly"
        case 22:
            return "Uses OS features not supported on host"
        case 23:
            return "Authorization denied"
        case 24:
            return "Auxiliary database format error"
        case 25:
            return "2nd parameter to sqlite3_bind out of range"
        case 26:
            return "File opened that is not a database file"
        case 27:
            return "Notifications from sqlite3_log()"
        case 28:
            return "Warnings from sqlite3_log()"
        case 100:
            return "sqlite3_step() has another row ready"
        case 101:
            return "sqlite3_step() has finished executing"
        default:
            return "Unknown SQLite error"
        }
        
    }
    
}

// fix for Swift limitations with C
private let SQLITE_NOTICE: Int32 = 27
private let SQLITE_WARNING: Int32 = 28
