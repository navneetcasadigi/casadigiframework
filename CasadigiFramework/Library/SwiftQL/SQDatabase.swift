// Copyright (c) 2016 Ryan Fowler. All rights reserved.

import Foundation
import SQLite3

// MARK: SQDatabase

class SQDatabase {
    
    var database: OpaquePointer? = nil
    var databasePath: String?
    var Cursors: [NSValue] = []
    
    class func defaultPath() -> String {
        let libPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0] as String
        return libPath + "SwiftQL.sqlite"
        //return libPath.stringByAppendingPathComponent("SwiftQL.sqlite")
    }

    /**
    Create an SQDatabase instance with the default path
    
    The default path is a file called "SwiftQL.sqlite" in the "Library Driectory"
    
    :returns:   An initialized SQDatabase instance
    */
    public init() {
        databasePath = SQDatabase.defaultPath()
    }
    
    /**
    Create an SQDatabase instance with the specified path
    
    If nil is provided as the path, an in-memory database is created
    
    :param:     path    The path to the database. If the database does not exist, it will be created.
    
    :returns:   An initialized SQDatabase instance
    */
    public init(path: String?) {
        databasePath = path
    }
    
    deinit {
        close()
    }
    
    
    // MARK: - Open/Close
    
    /**
    Open a connection to the database
    
    :returns:   True if connection successfully opened, false otherwise
    */
     func open() -> Bool {
        
        if database != nil {
            return true
        }
        if databasePath == nil {
            databasePath = ":memory:"
        }
        let status = sqlite3_open(databasePath!, &database)
        if status != SQLITE_OK {
            SQError.printSQLError("While opening database", errCode: status, errMsg: String(cString: sqlite3_errmsg(database)))
            return false
        }
        
        return true
    }
    
    /**
    Open a connection to the database with flags
    
    If a connection is already opened, it is closed and a new connection with flags is created
    
    :returns:   True if connection successfully opened with flags, false otherwise
    */
    @discardableResult func openWithFlags(_ flags: Flag) -> Bool {
        
        if database != nil {
            close()
        }
        if databasePath == nil {
            databasePath = ":memory:"
        }
        let status = sqlite3_open_v2(databasePath!, &database, flags.toInt(), nil)
        if status != SQLITE_OK {
            SQError.printSQLError("While opening database with flags", errCode: status, errMsg: String(cString: sqlite3_errmsg(database)))
            return false
        }
        
        return true
    }
    
    /**
    Available flags when opening a connection to the SQLite database
    
    Options are ReadOnly, ReadWrite, and ReadWriteCreate.
    Information at https://sqlite.org/c3ref/open.html
    */
    public enum Flag {
        case readOnly
        case readWrite
        case readWriteCreate        
        func toInt() -> Int32 {
            switch self {
            case .readOnly:
                return Int32(SQLITE_OPEN_READONLY)
            case .readWrite:
                return Int32(SQLITE_OPEN_READWRITE)
            case .readWriteCreate:
                return Int32(SQLITE_OPEN_READWRITE) | Int32(SQLITE_OPEN_CREATE)
            }
        }
    }
    
    /**
    Closes the connection to the database
    */
    func close() {
        
        if database == nil {
            return
        }
        
        closeAllCursors()
        
        let status = sqlite3_close(database)
        if status != SQLITE_OK {
            SQError.printSQLError("While closing database", errCode: status, errMsg: String(cString: sqlite3_errmsg(database)))
        }
        
        database = nil
    }
    
    
    // MARK: Execute An Update
    
    /**
    Execute a non-query SQL statement
    
    :param: sql     The String of SQL to execute
    
    :returns:   True if executed successfully, false if there was an error
    */
    func update(_ sql: String) -> Bool {
        return update(sql, withObjects: [])
    }
    
    /**
    Execute a non-query SQL statement with object bindings
    
    :param: sql     The String of SQL to execute
    :param: withObjects     The Array of objects to bind with the sql string
    
    :returns:   True if executed successfully, false if there was an error
    */
    func update(_ sql: String, withObjects objects: [Any?]) -> Bool {
        
        var pStmt: OpaquePointer? = nil
        
        var status = sqlite3_prepare_v2(database, sql, -1, &pStmt, nil)
        if status != SQLITE_OK {
            SQError.printSQLError("While preparing SQL statement: \(sql)", errCode: status, errMsg: String(cString: sqlite3_errmsg(database)))
            sqlite3_finalize(pStmt)
            return false
        }
        
        if objects.count != 0 {
            if sqlite3_bind_parameter_count(pStmt) != Int32(objects.count) {
                SQError.printError("While binding SQL statement: \(sql)", next: "Improper number of objects provided to bind")
                sqlite3_finalize(pStmt)
                return false
            }
            var i: Int32 = 1
            for obj in objects {
                bindObject(obj, toStatement: pStmt!, withColumn: i)
                i += 1
            }
        }
        
        status = sqlite3_step(pStmt)
        if status != SQLITE_DONE && status != SQLITE_OK {
            SQError.printSQLError("While stepping through SQL statement: \(sql)", errCode: status, errMsg: String(cString: sqlite3_errmsg(database)))
            sqlite3_finalize(pStmt)
            return false
        }
        
        sqlite3_finalize(pStmt)
        
        return true
    }
    
    
    // MARK: Execute Multiple Updates
    
    /**
    Execute multiple non-query SQL statements
    
    :param: sql     The Array of SQL Strings to execute
    
    :returns:   True if all statements executed successfully, false if there was an error
    */
    func updateMany(_ sql: [String]) -> Bool {
        
        var errMsg: UnsafeMutablePointer<Int8>? = nil
        
        var finalStr = ""
        for obj in sql {
            finalStr += obj + ";"
        }
        
        let status = sqlite3_exec(database, finalStr, nil, nil, &errMsg)
        if status != SQLITE_OK || errMsg != nil {
            SQError.printSQLError("While executing multiple statements", errCode: status, errMsg: String(cString: errMsg!))
            sqlite3_free(errMsg)
            return false
        }
        
        return true
    }
    
    
    // MARK: Execute A Query
    
    /**
    Execute a query SQL statement
    
    :param: sql     The String of SQL to execute
    
    :returns:   An Optional SDCursor object if executed successfully, nil if there was an error
    */
    func query(_ sql: String) -> SQCursor? {
        return query(sql, withObjects: [])
    }
    
    /**
    Execute a query SQL statement with object bindings
    
    :param: sql     The String of SQL to execute
    :param: withObjects     The Array of objects to bind with the sql string
    
    :returns:   An Optional SDCursor object if executed successfully, nil if there was an error
    */
    func query(_ sql: String, withObjects objects: [Any?]) -> SQCursor? {
        
        var pStmt: OpaquePointer? = nil
        
        let status = sqlite3_prepare_v2(database, sql, -1, &pStmt, nil)
        if status != SQLITE_OK {
            SQError.printSQLError("While preparing SQL statement: \(sql)", errCode: status, errMsg: String(cString: sqlite3_errmsg(database)))
            sqlite3_finalize(pStmt)
            return nil
        }
        
        if objects.count != 0 {
            if sqlite3_bind_parameter_count(pStmt) != Int32(objects.count) {
                SQError.printError("While binding SQL statement: \(sql)", next: "Improper number of objects provided to bind")
                sqlite3_finalize(pStmt)
                return nil
            }
            var i: Int32 = 1
            for obj in objects {
                bindObject(obj, toStatement: pStmt!, withColumn: i)
                i += 1
            }
        }
        
        return createCursor(pStmt!, sql: sql)
    }
    
    
    // MARK: Transaction Functions
    
    /**
    Begin an exclusive transaction
    
    :returns:   True if transaction has successfully begun, false if otherwise
    */
    @discardableResult func beginTransaction() -> Bool {
        return update("BEGIN EXCLUSIVE TRANSACTION")
    }
    
    /**
    Begin a deferred transaction
    
    :returns:   True if transaction has successfully begun, false if otherwise
    */
    func beginDeferredTransaction() -> Bool {
        return update("BEGIN DEFERRED TRANSACTION")
    }
    
    /**
    Commit the currently open transaction
    
    :returns:   True if transaction has successfully been committed, false if otherwise
    */
    func commitTransaction() -> Bool {
        return update("COMMIT TRANSACTION")
    }
    
    /**
    Rollback the currently open transaction
    
    :returns:   True if transaction has successfully been rolled back, false if otherwise
    */
    @discardableResult func rollbackTransaction() -> Bool {
        return update("ROLLBACK TRANSACTION")
    }
    
    
    // MARK: Savepoint Functions
    
    /**
    Begin a savepoint
    
    :param: name    The savepoint name
    
    :returns:   True if savepoint successfully started, false if otherwise
    */
    func startSavepoint(_ name: String) -> Bool {
        return update("SAVEPOINT \(escapeIdentifier(name))")
    }
    
    /**
    Release a savepoint
    
    :param: name    The savepoint name
    
    :returns:   True if savepoint has been successfully released, false if otherwise
    */
    func releaseSavepoint(_ name: String) -> Bool {
        return update("RELEASE SAVEPOINT \(escapeIdentifier(name))")
    }
    
    /**
    Rollback a savepoint
    
    :param: name    The savepoint name
    
    :returns:   True if savepoint has been successfully rolled back, false if otherwise
    */
    func rollbackToSavepoint(_ name: String) -> Bool {
        return update("ROLLCAK TO SAVEPOINT \(escapeIdentifier(name))")
    }
    
    private func escapeIdentifier(_ identifier: String) -> String {
        return identifier.replacingOccurrences(of: "'", with: "''")
    }
    
    
    // MARK: Misc
    
    /**
    Obtain the last inserted row id
    
    :returns:   An Int64 of the last inserted row id
    */
    func lastInsertId() -> Int64 {
        return sqlite3_last_insert_rowid(database) as Int64
    }
    
    /**
    Obtain the number of rows modified by the last operation
    
    :returns:   An Int indicating the number of rows modified
    */
    func rowsChanged() -> Int {
        return Int(sqlite3_changes(database))
    }
    
    /**
    Obtain the last error code
    
    :returns:   An Int indicating the last error code
    */
    func lastErrorCode() -> Int {
        return Int(sqlite3_errcode(database))
    }
    
    /**
    Obtain the last error message
    
    :returns:   A String indicating the last error message
    */
    func lastErrorMessage() -> String {
        if let mes = String(validatingUTF8: sqlite3_errmsg(database)) {
            return mes
        }
        return ""
    }
    
    /**
    Obtain the sqlite version
    
    :returns:   A String indicating the sqlite version
    */
    func sqliteVersion() -> String {
        if let ver = String(validatingUTF8: sqlite3_libversion()) {
            return ver
        }
        return ""
    }
    
    /**
    Change the journal_mode of the database
    
    :param: mode    A JournalMode case (.Delete, .Truncate, .Persist, .Memory, .WAL, .Off
    
    :returns:   True if journal_mode was successfully changed, false otherwise
    */
    func useJournalMode(_ mode: JournalMode) -> Bool {
        //let sql = "PRAGMA journal_mode=\(mode.toString())"
        let sql = "PRAGMA journal_mode=\(mode)"
        var errMsg: UnsafeMutablePointer<Int8>? = nil
        let status = sqlite3_exec(database, sql, nil, nil, &errMsg)
        if (status != SQLITE_OK && status != SQLITE_DONE) || errMsg != nil {
            SQError.printSQLError("While changing to journaling mode: \(mode)", errCode: status, errMsg: String(cString: errMsg!))
            sqlite3_free(errMsg)
            return false
        }
        return true
    }
    
    /**
    Journal mode options as specified at: https://sqlite.org/pragma.html#pragma_journal_mode
    */
    public enum JournalMode {
        case delete
        case truncate
        case persist
        case memory
        case wal
        case off
        
        private func toString() -> String {
            switch self {
            case .delete:
                return "DELETE"
            case .truncate:
                return "TRUNCATE"
            case .persist:
                return "PERSIST"
            case .memory:
                return "MEMORY"
            case .wal:
                return "WAL"
            case .off:
                return "OFF"
            }
        }
    }
    
    
    // MARK: Binding
    
    private func bindObject(_ obj: Any?, toStatement pStmt: OpaquePointer, withColumn col: Int32) {
        
        switch obj {
        case let val as String:
            sqlite3_bind_text(pStmt, col, val, -1, SQLITE_TRANSIENT)
        case let val as Int64:
            sqlite3_bind_int64(pStmt, col, val)
        case let val as Int:
            sqlite3_bind_int64(pStmt, col, Int64(val))
        case let val as Int32:
            sqlite3_bind_int(pStmt, col, val)
        case let val as Double:
            sqlite3_bind_double(pStmt, col, val)
        case let val as Bool:
            var intVal: Int32 = 0
            if val {
                intVal = 1
            }
            sqlite3_bind_int(pStmt, col, intVal)
        case let val as Data:
            let blob = (val as NSData).bytes
//            if blob == nil {
//                blob = nil
//            }
            sqlite3_bind_blob(pStmt, col, blob, -1, SQLITE_TRANSIENT)
        case let val as Date:
            sqlite3_bind_double(pStmt, col, val.timeIntervalSince1970)
        case nil:
            sqlite3_bind_null(pStmt, col)
        default:
            sqlite3_bind_null(pStmt, col)
            SQError.printWarning("While binding object: \(String(describing: obj))", next: "Unsupported object type, binding null")
        }
    }
    
    
    // MARK: SDCursor Functions
    
    func closeCursor(_ cursor: SQCursor) {
//        let cursorValue = NSValue(nonretainedObject: cursor)
//        for i in 0 ..< openCursors.count {
//            if openCursors[i] == cursorValue {
//                openCursors.remove(at: i)
//                return
//            }
//        }
    }
    
    private func closeAllCursors() {
//        while !openCursors.isEmpty {
//            if let cs = openCursors[0].nonretainedObjectValue as? SQCursor {
//                cs.close()
//            }
//        }
    }
    
    private func createCursor(_ statement: OpaquePointer, sql: String) -> SQCursor {
        let cursor = SQCursor(statement: statement, fromDatabase: self, withSQL: sql)
        //let cursorValue = NSValue(nonretainedObject: cursor)
        //openCursors.append(cursorValue)
        return cursor
    }
    
}

// fix for Swift limitations with C

internal let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
internal let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
