// Copyright (c) 2016 Ryan Fowler. All rights reserved.

import Foundation
import SQLite3
// MARK: SDCursor

 class SQCursor {
    
    var database: SQDatabase?
    var pStmt: OpaquePointer?
    let sqlStr: String
    lazy var columnNames: [String: Int32] = {
        [unowned self] in
        var dict: [String: Int32] = [:]
        let count = sqlite3_column_count(self.pStmt)
        for i: Int32 in 0 ..< count {
            if let colName = String(validatingUTF8: sqlite3_column_name(self.pStmt, i)) {
                dict[colName] = i
            }
        }
        return dict
    }()
    
    init(statement: OpaquePointer, fromDatabase db: SQDatabase, withSQL: String) {
        database = db
        pStmt = statement
        sqlStr = withSQL
    }
    
    deinit {
        close()
    }
    
    /**
    Returns true if another row exists
    */
     func next() -> Bool {
        
        let status = sqlite3_step(pStmt)
        if status != SQLITE_ROW {
            if status == SQLITE_DONE {
                return false
            }
            if let dbPointer = database?.database {
                SQError.printSQLError("While stepping through SQLite statement: \(sqlStr)", errCode: status, errMsg: String(cString: sqlite3_errmsg(dbPointer)))
                return false
            }
            SQError.printSQLError("While stepping through SQLite statement: \(sqlStr)", errCode: status, errMsg: nil)
            return false
        }
        
        return true
    }
    
    func close() {
        
        if pStmt == nil {
            return
        }
        
        sqlite3_finalize(pStmt)
        pStmt = nil
        database?.closeCursor(self)
        database = nil
    }
    
    func columnIndexForName(_ name: String) -> Int32 {

        if let val = columnNames[name] {
            return val
        }

        return -1
    }
    
    func intForColumn(_ name: String) -> Int? {
        return intForColumnIndex(columnIndexForName(name))
    }
    func int32ForColumn(_ name: String) -> Int32? {
        return int32ForColumnIndex(columnIndexForName(name))
    }
    func int64ForColumn(_ name: String) -> Int64? {
        return int64ForColumnIndex(columnIndexForName(name))
    }
     func doubleForColumn(_ name: String) -> Double? {
        return doubleForColumnIndex(columnIndexForName(name))
    }
     func stringForColumn(_ name: String) -> String? {
        return stringForColumnIndex(columnIndexForName(name))
    }
     func boolForColumn(_ name: String) -> Bool? {
        return boolForColumnIndex(columnIndexForName(name))
    }
     func dateForColumn(_ name: String) -> Date? {
        return dateForColumnIndex(columnIndexForName(name))
    }
     func dataForColumn(_ name: String) -> Data? {
        return dataForColumnIndex(columnIndexForName(name))
    }
    
     func intForColumnIndex(_ index: Int32) -> Int? {
        if index < 0 || sqlite3_column_type(pStmt, index) == SQLITE_NULL {
            return nil
        }
        return Int(sqlite3_column_int64(pStmt, index))
    }
     func int32ForColumnIndex(_ index: Int32) -> Int32? {
        if index < 0 || sqlite3_column_type(pStmt, index) == SQLITE_NULL {
            return nil
        }
        return sqlite3_column_int(pStmt, index)
    }
     func int64ForColumnIndex(_ index: Int32) -> Int64? {
        if index < 0 || sqlite3_column_type(pStmt, index) == SQLITE_NULL {
            return nil
        }
        return Int64(sqlite3_column_int64(pStmt, index))
    }
     func doubleForColumnIndex(_ index: Int32) -> Double? {
        if index < 0 || sqlite3_column_type(pStmt, index) == SQLITE_NULL {
            return nil
        }
        return sqlite3_column_double(pStmt, index)
    }
    public func stringForColumnIndex(_ index: Int32) -> String? {
        if index < 0 || sqlite3_column_type(pStmt, index) == SQLITE_NULL {
            return nil
        }
        let text = sqlite3_column_text(pStmt, index)
        return String(cString: text!)
    }
     func boolForColumnIndex(_ index: Int32) -> Bool? {
        if index < 0 || sqlite3_column_type(pStmt, index) == SQLITE_NULL {
            return nil
        }
        let val = sqlite3_column_int(pStmt, index)
        if val == 0 {
            return false
        }
        return true
    }
     func dateForColumnIndex(_ index: Int32) -> Date? {
        if index < 0 || sqlite3_column_type(pStmt, index) == 5 {
            return nil
        }
        return Date(timeIntervalSince1970: sqlite3_column_double(pStmt, index))
    }
     func dataForColumnIndex(_ index: Int32) -> Data? {
        if index < 0 || sqlite3_column_type(pStmt, index) == 5 {
            return nil
        }
        let dataSize = Int(sqlite3_column_bytes(pStmt, index))
        let blob = sqlite3_column_blob(pStmt, index)
        if blob == nil {
            return nil
        }
        return Data(bytes: UnsafeRawPointer(blob)!, count: dataSize)
    }
    
}
