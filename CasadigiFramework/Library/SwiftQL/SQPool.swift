// Copyright (c) 2016 Ryan Fowler. All rights reserved.

import Foundation

// MARK: SQPool

public class SQPool {
    
    private let path: String?
    private var connIndex = 1
    private var flags = SQDatabase.Flag.readWriteCreate
    private var connPool: [Int:SQDatabase] = [:]
    private var inUsePool: [Int:SQDatabase] = [:]
    
    // Queue for database write operations
    //var writeQueue = dispatch_queue_create("swiftql.write", DISPATCH_QUEUE_SERIAL)
    var writeQueue = DispatchQueue(label: "swiftql.write")
    
    // Queue for getting/releasing databases in pool
    // To prevent weird behaviour from accessing properties from multiple threads
    //var poolQueue = dispatch_queue_create("swiftql.pool", DISPATCH_QUEUE_SERIAL)
    var poolQueue = DispatchQueue(label: "swiftql.pool")
    /**
    The maximum number of connections to be kept in the connection pool
    
    Note: this only limits the number of available connections in the connection pool.
    Connections will be created as required and returned to the connection pool, or destroyed if the connection pool contains the maximum connection limit.
    */
    public var maxSustainedConnections = 5
    
    /**
    Returns the number of idle connections
    
    Note: Will always be less than or equal to maxSustainedConnections
    */
    public func numberOfFreeConnections() -> Int {
        return connPool.count
    }
    
    /**
    Create an SQPool instance with the default path
    
    The default path is a file called "SwiftQL.sqlite" in the "Library Driectory"
    
    :returns:   An initialized SQPool instance
    */
    public init() {
        path = SQDatabase.defaultPath()
        useWALMode()
    }
    
    /**
    Create an SQPool instance with the specified path and flags
    
    :returns:   An initialized SQPool instance
    */
    init(path: String?, withFlags flags: SQDatabase.Flag) {
        self.path = path
        self.flags = flags
        useWALMode()
    }
    
    deinit {
        connPool = [:]
        inUsePool = [:]
    }
    
    // Does not use the proper getConnection/releaseConnection - only meant for use in init()
    // Only call in init!
    private func useWALMode() {
        let db = SQDatabase(path: path)
        db.openWithFlags(flags)
        if !db.useJournalMode(.wal) {
            SQError.printWarning("While opening an SQPool instance", next: "Cannot verify that the database is in WAL mode")
        }
        
    }
    
    // Obtain an SQDatabase object from the connection pool,
    // otherwise create a new connection
    private func getConnection() -> (Int, SQDatabase) {
        var db: SQDatabase?
        var index = 0
        poolQueue.sync(execute: {
            if self.connPool.isEmpty {
                self.connIndex = +1
                let database = SQDatabase(path: self.path)
                database.openWithFlags(self.flags)
                self.inUsePool[self.connIndex] = database
                db = database
                index = self.connIndex
                return
            }
            for ind in self.connPool.keys {
                let database = self.connPool.removeValue(forKey: ind)!
                self.inUsePool[ind] = database
                db = database
                index = ind
                return
            }
        })
        return (index, db!)
    }
    
    // Release an SQDatabase object to the connection pool,
    // or delete it if connPool is greater than the maxSustainedConnections
    private func releaseConnection(index: Int) {
        poolQueue.sync(execute: {
            if self.connPool.count < self.maxSustainedConnections {
                let database = self.inUsePool.removeValue(forKey: index)!
                self.connPool[index] = database
                return
            }
            self.inUsePool[index] = nil
        })
    }
    
    /**
    Execute a write (non-query) operation(s) on the database
    
    Note: write() should be used over read() if ANY operation in the closure is a non-query.
    Otherwise, locked database errors can occur.
    
    :param: closure     A closure that accepts an SQDatabase instance to be used for non-query operations
    */
    func write(closure: @escaping (SQDatabase)->Void) {
        let (index, db) = getConnection()
        writeQueue.sync(execute: {
            closure(db)
        })
        releaseConnection(index: index)
    }
    
    /**
    Execute a write (non-query) operation(s) on the database asynchronously
    
    Note: write() should be used over read() if ANY operation in the closure is a non-query.
    Otherwise, locked database errors can occur.
    
    This function will return immediately and the closure will run on a background thread.
    
    :param: closure     A closure that accepts an SQDatabase instance to be used for non-query operations
    */
    func writeAsync(closure: @escaping (SQDatabase)->Void) {
        DispatchQueue.main.async( execute: {
            self.write(closure: closure)
        })
//        Queue.main.execute {
//            self.write(closure: closure)
//        }
    }
    
    /**
    Execute read (query) operations on the database
    
    Note: write() should be used over read() if ANY operation in the closure is a non-query.
    Otherwise, locked database errors can occur.
    
    :param: closure     A closure that accepts an SQDatabase instance to be used for query operations
    */
    func read(closure: (SQDatabase)->Void) {
        let (index, db) = getConnection()
        closure(db)
        releaseConnection(index: index)
    }
    
    /**
    Execute read (query) operations on the database asynchronously
    
    Note: write() should be used over read() if ANY operation in the closure is a non-query.
    Otherwise, locked database errors can occur.
    
    This function will return immediately and the closure will run on a background thread.
    
    :param: closure     A closure that accepts an SQDatabase instance to be used for query operations
    */
    func readAsync(closure: @escaping (SQDatabase)->Void) {
        //dispatch_async(dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0), {self.read(closure)})
        
        DispatchQueue.main.async( execute: {
            self.read(closure: closure)
        })
    }
    
    /**
    Execute a transaction
    
    :param: closure     A closure that accepts an SQDatabase instance that returns true to commit, or false to rollback
    
    :returns:   True if the transaction was successfully committed, false if rolled back
    */
    func transaction(closure: @escaping (SQDatabase)->Bool) -> Bool {
        
        var status = false
        let (index, db) = self.getConnection()
        writeQueue.sync(execute: {
            db.beginTransaction()
            if closure(db) {
                if db.commitTransaction() {
                    status = true
                } else {
                    db.rollbackTransaction()
                }
            } else {
                db.rollbackTransaction()
            }
        })
        releaseConnection(index: index)
        
        return status
    }
    
    /**
    Execute a transaction asynchronously
    
    Note: This function will return immediately and the closure will run on a background thread.
    
    :param: closure     A closure that accepts an SQDatabase instance that returns true to commit, or false to rollback
    */
    func transactionAsync(closure: @escaping (SQDatabase)->Bool) {
        //Queue.main.execute {_ = self.transaction(closure: closure)  }
        //dispatch_async(dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0), {_ = self.transaction(closure)})
        DispatchQueue.main.async( execute: {
            _ = self.transaction(closure: closure)
        })
    }
    
}
