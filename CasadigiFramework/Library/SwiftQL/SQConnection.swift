// Copyright (c) 2016 Ryan Fowler. All rights reserved.

import Foundation

// MARK: SQConnection

class SQConnection {
    
    var database:SQDatabase    
    var databaseQueue = DispatchQueue(label: "swiftql.connection")
    //var databaseQueue = DispatchQueue(label: "swiftql.connection", attributes: [])
    
    /**
    Create an SQConnection instance with the default path
    
    The default path is a file called "SwiftQL.sqlite" in the "Library Driectory"
    
    :returns:   An initialized SDConnection instance
    */
    public init() {
        database = SQDatabase()
        _ = database.open()     // Consider making this a failable initializer
    }
    
    /**
    Create an SQConnection instance with the specified path
    
    If nil is provided as the path, an in-memory database is created
    
    :param:     path    The path to the database. If the database does not exist, it will be created.
    
    :returns:   An initialized SDConnection instance
    */
    public init(path: String?, withFlags flags: SQDatabase.Flag) {
        database = SQDatabase(path: path)
        database.openWithFlags(flags)   // Consider making this a failable initializer
    }
    
    /**
    Execute functions within a closure
    
    :param: closure     The closure that accepts an SQDatabase object
    */
    open func execute(_ closure: @escaping (SQDatabase)->Void) {  // Consider making the closure return a success variable
        
        //databaseQueue.sync(execute:).sync(execute: {closure(self.database)})
    }
    
    /**
    Execute functions within a closure asynchronously
    
    Note: This function will return immediately and the closure will run on a background thread.
    
    :param: closure     The closure that accepts an SQDatabase object to execute asynchronously
    */
    func executeAsync(_ closure: @escaping (SQDatabase)->Void) {
        DispatchQueue.main.async( execute: {
            self.execute(closure)
        })
    }
    
    /**
    Execute a transaction
    
    :param: closure     The transaction closure that accepts an SQDatabase object and returns true to commit, or false to rollback
    
    :returns:   True if transaction has been successfully committed, false if rolled back
    */
     func transaction(_ closure: (SQDatabase)->Bool) -> Bool {
        
        var status = false
        databaseQueue.sync(execute: {
            self.database.beginTransaction()
            if closure(self.database) {
                if self.database.commitTransaction() {
                    status = true
                } else {
                    self.database.rollbackTransaction()
                }
            } else {
                self.database.rollbackTransaction()
            }
        })
        
        return status
    }
    
    /**
    Execute a transaction asynchronously
    
    Note: This function will return immediately and the closure will run on a background thread.
    
    :param: closure     The transaction closure that accepts an SQDatabase object and returns true to commit, or false to rollback
    */
     func transactionAsync(_ closure: @escaping (SQDatabase)->Bool) {
        //Dispatch_
        DispatchQueue.main.async( execute: {
            _ = self.transaction(closure)
        })
        //Queue.main.execute {_ = self.transaction(closure)}
    }
    
}
