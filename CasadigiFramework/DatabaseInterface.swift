 // Copyright (c) 2017 CasaDigi. All rights reserved.
 
 import UIKit
 import SQLite3
  
 final public class DatabaseInterface {
    static public let shared: DatabaseInterface = DatabaseInterface()
    
    private init() {
        try? FileManager.default.createDirectory(atPath: applicationBundleAppContentDirectoryURL.path, withIntermediateDirectories: true, attributes: nil)
        
        if isConfigurationAvailable {
            self.openConfigurationDatabase()
            self.openEntertainmentDatabase()
            self.loadLanguageFile()
            
            print("current time \(lastEntUpdateTimestamp)")
        }
    }
    
    public var configurationDatabase: OpaquePointer? = nil
    public var entertainmentDatabase: OpaquePointer? = nil
    public var offlineVideoPlaylistDatabase: OpaquePointer? = nil
    
    public let configurationDatabasePath = applicationBundleAppContentDirectoryURL.appendingPathComponent("configuration.sqlite").path
    public let entertainmentDatabasePath = applicationBundleAppContentDirectoryURL.appendingPathComponent("entertainment.sqlite").path
    public let offlineMusicPlaylistsDatabaseURL = applicationBundleAppContentDirectoryURL.appendingPathComponent("offlineMusicPlaylist.sqlite")
    public let offlineVideoPlaylistDatabaseURL = applicationBundleAppContentDirectoryURL.appendingPathComponent("offlineVideoPlaylist.sqlite")
    public let offlineMusicPlaylistNewDatabaseURL = applicationBundleAppContentDirectoryURL.appendingPathComponent("MusicOfflinePlaylist.sqlite")
    
    public var profileID = "-1"
    public var deviceID = "-1"
    public var deviceVendorID = UIDevice.current.identifierForVendor!.uuidString.replacingOccurrences(of: "-", with: "")
    public var languageInfo = [String: Any]()
    public var webRadioBaseUrl = "http://casadigi.vtuner.com:8088/"
    public var isTVChannelsUpdated: Bool = false
    public var isEntertainmentUpdated: Bool = false
    public var isTVShowUpdated: Bool = false
    public var isPhotoUpdate: Bool = false
    public var isPVUpdate: Bool = false
    public var isMusicUpdate: Bool = false
    public var isMovieUpdate: Bool = false
    
    public func openConfigurationDatabase() {
        let status = sqlite3_open(self.configurationDatabasePath, &self.configurationDatabase)
        
        if status == SQLITE_OK {
            //  self.setDeviceInfo()
        }
    }
    
    public func closeConfigurationDatabase() {
        sqlite3_close(self.configurationDatabase)
    }
    
    public func openEntertainmentDatabase() {
        sqlite3_open(self.entertainmentDatabasePath, &self.entertainmentDatabase)
    }
    
    public func closeEntertainmentDatabase() {
        sqlite3_close(self.entertainmentDatabase)
    }
    
    public func loadLanguageFile() {
        let languageFilePath = applicationBundleAppContentDirectoryURL.appendingPathComponent(kDirectoryLanguageContent + "/language.json").path
        
        if languageFilePath != "" {
            if let data = try? Data(contentsOf: URL(fileURLWithPath: languageFilePath)) {
                if let languageJSON = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any] {
                    self.languageInfo.removeAll()
                    self.languageInfo = languageJSON
                }
            }
        }
    }
    
    public func getLingualText(_ baseText: String) -> String {
        if let languageInfoDic = self.languageInfo[baseText] as? [String:String], let languageCode = CDDatabaseInterfaceManager.shared.languageCode(),
            let lingualText = languageInfoDic[languageCode] {
            return lingualText
        }
        
        return baseText
    }
    
    private func setDeviceInfo() {
        
    }
    
    public func getInfoForVideoWallDeviceSingle(_ device: String, inObject infoObj:DeviceInfo, forRoomId roomId: String, tiedId: String) {
        let query = "SELECT user_device_config_id, paragon_module_id, scn_id, is_volumebar, scn_modules FROM user_device_configuration_mc WHERE driver_type LIKE 'tv' AND paragon_module_id IN (SELECT paragon_module_id FROM user_device_configuration_mc WHERE driver_type LIKE '\(device)' AND room_id IN (\(roomId)) ) AND user_device_config_id = \(tiedId)"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    infoObj.unitId = rs.stringForColumnIndex(0) ?? ""
                    infoObj.moduleId = rs.stringForColumnIndex(1) ?? ""
                    infoObj.sceneId = rs.stringForColumnIndex(2) ?? ""
                    infoObj.showVolumeBar = Int(rs.int64ForColumnIndex(3)!).toBool()
                    infoObj.remoteControlType = rs.stringForColumnIndex(4) ?? ""
                }
            }
        }
    }
    
    public func getInfoForVideoWallDevice(_ device: String, inObject infoObj:DeviceInfo, forRoomId roomId: String, tiedId: [String]) {
        var query = "SELECT user_device_config_id, paragon_module_id, scn_id, is_volumebar, scn_modules FROM user_device_configuration_mc WHERE driver_type LIKE 'tv' AND paragon_module_id IN (SELECT paragon_module_id FROM user_device_configuration_mc  WHERE driver_type LIKE '\(device)' AND room_id IN (\(roomId)) )"
        
        let tiedIds = tiedId.filter {$0 != ""}
        
        if tiedIds.count > 0 {
            query = query + " AND user_device_config_id NOT IN (\(tiedIds.joined(separator: ","))) "
        }
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    infoObj.unitId = rs.stringForColumnIndex(0) ?? ""
                    infoObj.moduleId = rs.stringForColumnIndex(1) ?? ""
                    infoObj.sceneId = rs.stringForColumnIndex(2) ?? ""
                    infoObj.showVolumeBar = Int(rs.int64ForColumnIndex(3)!).toBool()
                    infoObj.remoteControlType = rs.stringForColumnIndex(4) ?? ""
                }
            }
        }
    }
    
    public func getVideoWallRowColumns(forRoomId roomId: String) -> String {
        var rowCols = ""
        let query = "SELECT scn_modules FROM user_device_configuration_mc WHERE driver_type LIKE 'Projector' AND room_id IN (\(roomId))"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    rowCols = rs.stringForColumnIndex(0) ?? ""
                }
            }
        }
        
        return rowCols
    }
    
    public func getInfoForDevice(_ device: String, inObject infoObj:DeviceInfo, forRoomId roomId: String,forDigitalCinema displayName: String = "",entertainment_id: String = "") {
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        var query = ""
        if database.open() {
            if entertainment_id != ""
            {
                query = "SELECT entertainment_id, room_controller_id, is_volumebar FROM entertainment WHERE entertainment_id = '\(entertainment_id)' AND room_id IN (\(roomId))"
            }
            else
            {
                if displayName == ""{
                    query = "SELECT entertainment_id, room_controller_id, is_volumebar FROM entertainment WHERE device_category LIKE '\(device)' AND room_id IN (\(roomId))"
                }else{
                    //Query required for digitial cinema to differentiate which type of Digital cinema it is (required display name)
                    query = "SELECT entertainment_id, room_controller_id, is_volumebar FROM entertainment WHERE device_category LIKE '\(device)' AND room_id IN (\(roomId)) AND display_name LIKE '\(displayName)'"
                    
                }
            }
            //print("getInfoForDevice query - \(query)")
            if let rs = database.query(query) {
                while rs.next() {
                    infoObj.unitId = rs.stringForColumnIndex(0) ?? ""
                    infoObj.moduleId = rs.stringForColumnIndex(1) ?? ""
                    infoObj.sceneId = ""
                    infoObj.showVolumeBar = Int(rs.int64ForColumnIndex(2)!).toBool()
                }
            }
        }
        
    }
    
    
    public func isPanoramicEnableForModuleType(_ moduleType: String, forRoomId roomId: String) -> Bool {
        var isEnable = false
        
        var query = "SELECT " + ((moduleType == kCasaDigiModuleLights) ? "room_light_bg" : "room_curtain_bg") + " FROM room_info WHERE room_id=" + roomId
        
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    isEnable = Int(rs.int64ForColumnIndex(0)!).toBool()
                }
            }
        }
        
        return isEnable
    }
    
    public func getRoomImageForType(_ roomType: String, forRoomId roomId: String) -> UIImage? {
        var roomImg: UIImage?
        
        let query = "SELECT default_image FROM room_image_icon WHERE image_id IN (SELECT image_id FROM room_images WHERE type LIKE '\(roomType)' AND room_id = \(roomId))"
        let directoryName = (roomType == "light") ? kDirectoryLightsImages : kDirectoryCurtainsImages
        let filePath = applicationBundleAppContentDirectoryURL.appendingPathComponent(directoryName)
        let scale: CGFloat = 2.0
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    let imageName = rs.stringForColumnIndex(0) ?? ""
                    
                    if let actualImage = UIImage(contentsOfFile: filePath.appendingPathComponent(imageName).path) {
                        roomImg = UIImage(cgImage: (actualImage.cgImage)!, scale: scale, orientation: .up)
                    }
                }
            }
        }
        
        return roomImg
    }
    
    public func getRoomImageForTypeV2(_ roomType: String, forRoomId roomId: String) -> Dictionary<String, String>? {
        var dic: Dictionary<String, String>?
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            let query = "SELECT image_name, image_width, image_height FROM automation_bg_image WHERE room_id = \(roomId) and category like '\(roomType)'"
            
            if let rs = database.query(query) {
                while rs.next() {
                    var imageName = ""
                    var imageWidth = ""
                    var imageHt = ""
                    
                    if let tempimageName = rs.stringForColumnIndex(0) {
                        imageName = tempimageName
                    }
                    
                    if let tempimageWidth = rs.stringForColumnIndex(1) {
                        imageWidth = tempimageWidth
                    }
                    
                    if let tempimageHt = rs.stringForColumnIndex(2) {
                        imageHt = tempimageHt
                    }
                    
                    dic = [
                        "imageHt": imageHt,
                        "imageWidth": imageWidth,
                        "imageName": imageName
                    ]
                }
            }
        }
        
        return dic
    }
    
    public func getiProRoomImageForType(_ roomType: String, forRoomId roomId: String) -> UIImage? {
        var roomImg: UIImage?
        
        let query = "SELECT default_image FROM room_image_icon WHERE image_id IN (SELECT image_id FROM room_images WHERE type LIKE '\(roomType)' AND room_id = \(roomId))"
        let directoryName = (roomType == "light") ? "ipadProLightImages" : "ipadProCurtainsImages"
        let filePath = applicationBundleAppContentDirectoryURL.appendingPathComponent(directoryName)
        let scale: CGFloat = 2.0
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    let imageName = rs.stringForColumnIndex(0) ?? ""
                    
                    if let actualImage = UIImage(contentsOfFile: filePath.appendingPathComponent(imageName).path) {
                        roomImg = UIImage(cgImage: (actualImage.cgImage)!, scale: scale, orientation: .up)
                    }
                }
            }
        }
        
        return roomImg
    }
    
    public func getConnectionInfoForModuleID(_ moduleId: String) -> [String:String]? {
        var connectionInfo: [String:String]?
        
        let  query = "SELECT ip, controller_name FROM room_controller WHERE controller_id = \(moduleId)"
        
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    let ipAddress = rs.stringForColumnIndex(0)
                    let controllerName = rs.stringForColumnIndex(1)
                    
                    connectionInfo = [
                        "IP": ipAddress!,
                        "ControllerName": controllerName!
                    ]
                }
            }
        }
        
        
        
        return connectionInfo
    }
    /*Use method to get roomid from database
     -Parameters:
     ipAddress : String parameter pass controller ipaddress.
     */
    public func getRoomIDForIpAddress(_ ipAddress: String) -> String {
        var connectionId: String = ""
        
        let query = "SELECT room_id FROM room_controller WHERE ip = '\(ipAddress)'"
        
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    let roomid = rs.stringForColumnIndex(0) ?? ""
                    connectionId = roomid
                }
            }
        }
        
        return connectionId
    }
    public func getPartnerLogoName() -> String
    {
        var logoName = ""
        
        let sqlQuery = "SELECT default_icon FROM CD_icon WHERE category = 'partnerlogo' AND is_disable = 0"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        if !database.open() {
            return logoName
        } else {
            if let rs = database.query(sqlQuery){
                while rs.next() {
                    logoName = rs.stringForColumnIndex(0)!
                }
            }
        }
        return logoName
    }
    public func isPartnerLogoPermissionAvailable()
    {
        
        var sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'partnerlogo' AND userdevice_type = 'ipad'"
        if UIScreen.main.bounds.width > 1024
        {
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'partnerlogo' AND userdevice_type = 'ipadpro'"
        }
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        if !database.open() {
            isPartnerLogoAvailable = false
        } else {
            if let rs = database.query(sqlQuery){
                while rs.next() {
                    isPartnerLogoAvailable = true
                }
            }
        }
        
    }
    public func getRoomsInfoForCurrentDevice() -> (roomInfo: [[String:Any]], floorInfo: [String], homeFeatureArray: [String]) {
        
        var roomInfo            = [[String:Any]]()
        var floorInfoArray      = [String]()
        var homeFeatureArray    = [String]()
        var room_id             = [String]()
        
        
        var roomInfoDictionary =  Dictionary<String, Any>()
        var flooreInfoDictionary = [[String:Any]]()
        var permissions = [String:Any]()
        var roomLevel = [String:Any]()
        var homePermission_other = [String]()
        var homeLevel = [String:Any]()
        let path = applicationBundleAppContentDirectoryURL.appendingPathComponent("roomInfo").path as String
        if path != "" {
            let archive = FileManager.default.contents(atPath: path)
            
            if archive != nil {
                roomInfoDictionary = NSKeyedUnarchiver.unarchiveObject(with: archive!)! as! Dictionary<String, Any>
                
                permissions             = roomInfoDictionary["permission"] as! [String:Any]
                if(permissions["roomlevel"] != nil)
                {
                    roomLevel               = permissions["roomlevel"] as! [String:Any]
                    flooreInfoDictionary    = roomLevel["floors"] as! [[String:Any]]
                }
                if(permissions["homelevel"] != nil)
                {
                    homeLevel               = permissions["homelevel"] as! [String:Any]
                }
                
                if(homeLevel.count > 0)
                {
                    let homeLevelPermission = homeLevel["home_permissions"] as! [String:Any]
                    homePermission_other = homeLevelPermission["others"] as! [String]
                    let homePermission_surveillance = homeLevelPermission["surveillance"] as! [String]
                    
                    homeFeatureArray.append(contentsOf: homePermission_other)
                    if(homePermission_surveillance.count > 0)
                    {
                        homeFeatureArray.append("surveillance")
                    }
                }
                for flooreInfo in flooreInfoDictionary
                {
                    //print(flooreInfo)
                    
                    let rooms = flooreInfo["rooms"] as? [[String: Any]]
                    for room in rooms!
                    {
                        room_id.append(room["room_id"] as! String)
                    }
                    
                    floorInfoArray.append(flooreInfo["floor_name"]! as! String)
                    
                    
                }
            }
        }
        
        
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open(){
            return (roomInfo, floorInfoArray, homeFeatureArray)
        } else {
            //let roomsQuery = "SELECT RI.room_id, RI.room_name, FI.floor_name, II.default_icon, II.selected_icon, RI.upload_icon_name FROM room_info AS RI, floor_info AS FI, CD_icon as II WHERE RI.parent_room_id = 0 AND RI.floor_id = FI.floor_id AND RI.room_icon_id = II.icon_id ORDER BY FI.position, RI.position AND RI.room_id IN (\(room_id.joined(separator: ",")))"
            let roomsQuery = "SELECT RI.room_id, RI.room_name, FI.floor_name, CI.default_icon, CI.selected_icon, RI.upload_icon_name FROM room_info AS RI inner join floor_info AS FI on FI.floor_id=RI.floor_id inner join CD_icon AS CI on CI.icon_id=RI.room_icon_id where RI.parent_room_id = 0  AND RI.room_id IN (\(room_id.joined(separator: ","))) ORDER BY FI.position, RI.position"
            if let rs = database.query(roomsQuery) {
                while rs.next() {
                    let room_Id = rs.stringForColumnIndex(0)!
                    let roomName = rs.stringForColumnIndex(1)!
                    let floorName = rs.stringForColumnIndex(2)!
                    let iconName = rs.stringForColumnIndex(3)!
                    let iconNameSel = rs.stringForColumnIndex(4)!
                    let iconNameImg = rs.stringForColumnIndex(5)!
                    //                        let subRoomQuery = "SELECT room_id, room_name FROM room_info WHERE parent_room_id = '\(room_Id)' AND room_id IN (SELECT room_id FROM room_profile_permission WHERE room_profile_id = \(profileID)) ORDER BY position"
                    
                    var subRoomInfoArry = [[String:String]]()
                    var dashboardSubRoomInfoArry = [[String:Any]]()
                    var featuresInfoArry: [(RoomId: String, ModuleType: String)] = []
                    var featuresSet = Set<String> ()
                    featuresSet.removeAll()
                    let keyArrayForRoomPermission = ["others","audio","video"]
                    for flooreInfo in flooreInfoDictionary
                    {
                        if let rooms = flooreInfo["rooms"] as? [[String:Any]]
                        {
                            for room in rooms
                            {
                                if(room_Id == room["room_id"] as! String)
                                {
                                    if(room["is_subroom"]! as! Float == 0)
                                    {
                                        let featuresArr = room["room_permissions"] as! [String:Any]
                                        // let featuresArr =  featuresMainArr[0] as! [String:Any]
                                        if(homePermission_other.count > 0 && homePermission_other.contains(kCasaDigiModuleMultiZoneMusic))
                                        {
                                            let info = (RoomId: room["room_id"] as! String, ModuleType: kCasaDigiModuleMultiZoneMusic)
                                            featuresInfoArry.append(info)
                                        }
                                        for key in keyArrayForRoomPermission
                                        {
                                            let others = featuresArr[key] as! [String]
                                            
                                            for features in others
                                            {
                                                
                                                let info = (RoomId: room["room_id"] as! String, ModuleType: features)
                                                featuresInfoArry.append(info)
                                                
                                                switch features
                                                {
                                                case kCasaDigiModuleMusic:
                                                    featuresSet.insert(kCasaDigiModuleMusic)
                                                    
                                                case kCasaDigiModulewebRadio:
                                                    featuresSet.insert(kCasaDigiModulewebRadio)
                                                    
                                                case kCasaDigiModuleVideos, kCasaDigiModuleDvd, kCasaDigiModuleMovies, kCasaDigiModuleTVShows,kCasaDigiModulePersonalVideos,kCasaDigiModuleDigitalCinema:
                                                    featuresSet.insert(kCasaDigiModuleVideos)
                                                    
                                                case kCasaDigiModuleLights, "lightmood", "pool_spa":
                                                    featuresSet.insert(kCasaDigiModuleLights)
                                                    
                                                case kCasaDigiModuleCurtains, "curtainmood":
                                                    featuresSet.insert(kCasaDigiModuleCurtains)
                                                    
                                                case kCasaDigiModuleAC, kCasaDigiModuleFan:
                                                    featuresSet.insert(kCasaDigiModuleAC)
                                                    
                                                case kCasaDigiModuleTelevision:
                                                    featuresSet.insert(kCasaDigiModuleTelevision)
                                                    
                                                default:
                                                    if(features == kCasaDigiModuleDigitalMedia)
                                                    {
                                                        featuresSet.insert(kCasaDigiModuleVideos)
                                                    }
                                                    featuresSet.insert(features)
                                                }
                                            }
                                            
                                        }
                                    }
                                    else if(room["is_subroom"]! as! Float == 1)
                                    {
                                        //subrooms
                                        let subrooms = room["subrooms"] as! [[String:Any]]
                                        
                                        for subroom in subrooms
                                        {
                                            let featuresArr = subroom["room_permissions"] as! [String:Any]
                                            var subRoomFeatureArray = [String]()
                                            for key in keyArrayForRoomPermission
                                            {
                                                let others = featuresArr[key] as! [String]
                                                if subroom["is_module_exists"] != nil{
                                                    for features in others
                                                    {
                                                        
                                                        let info = (RoomId: subroom["room_id"] as! String, ModuleType: features)
                                                        featuresInfoArry.append(info)
                                                        
                                                        switch features
                                                        {
                                                        case kCasaDigiModuleMusic:
                                                            subRoomFeatureArray.append(kCasaDigiModuleMusic)
                                                            
                                                        case kCasaDigiModulewebRadio:
                                                            subRoomFeatureArray.append(kCasaDigiModulewebRadio)
                                                            
                                                        case kCasaDigiModuleVideos, kCasaDigiModuleDvd, kCasaDigiModuleMovies, kCasaDigiModuleTVShows,kCasaDigiModulePersonalVideos,kCasaDigiModuleDigitalCinema:
                                                            subRoomFeatureArray.append(kCasaDigiModuleVideos)
                                                            
                                                        case kCasaDigiModuleLights, "lightmood":
                                                            featuresSet.insert(kCasaDigiModuleLights)
                                                            
                                                        case kCasaDigiModuleCurtains, "curtainmood":
                                                            featuresSet.insert(kCasaDigiModuleCurtains)
                                                            
                                                        case kCasaDigiModuleAC, kCasaDigiModuleFan:
                                                            featuresSet.insert(kCasaDigiModuleAC)
                                                            
                                                        case kCasaDigiModuleTelevision:
                                                            subRoomFeatureArray.append(kCasaDigiModuleTelevision)
                                                            
                                                        default:
                                                            if(features == kCasaDigiModuleDigitalMedia && key != "audio")
                                                            {
                                                                subRoomFeatureArray.append(kCasaDigiModuleVideos)
                                                            }
                                                            featuresSet.insert(features)
                                                            
                                                        }
                                                        
                                                        if(homePermission_other.count > 0 && homePermission_other.contains(kCasaDigiModuleMultiZoneMusic))
                                                        {
                                                            let info = (RoomId: subroom["room_id"] as! String, ModuleType: kCasaDigiModuleMultiZoneMusic)
                                                            featuresInfoArry.append(info)
                                                            
                                                        }
                                                    }
                                                } else{
                                                    for features in others
                                                    {
                                                        let info = (RoomId: subroom["room_id"] as! String, ModuleType: features)
                                                        featuresInfoArry.append(info)
                                                        
                                                        switch features
                                                        {
                                                        case kCasaDigiModuleMusic:
                                                            featuresSet.insert(kCasaDigiModuleMusic)
                                                            
                                                            
                                                        case kCasaDigiModulewebRadio:
                                                            featuresSet.insert(kCasaDigiModulewebRadio)
                                                            
                                                            
                                                        case kCasaDigiModuleVideos, kCasaDigiModuleDvd, kCasaDigiModuleMovies, kCasaDigiModuleTVShows,kCasaDigiModulePersonalVideos,kCasaDigiModuleDigitalCinema:
                                                            featuresSet.insert(kCasaDigiModuleVideos)
                                                            
                                                            
                                                        case kCasaDigiModuleLights, "lightmood":
                                                            featuresSet.insert(kCasaDigiModuleLights)
                                                            
                                                        case kCasaDigiModuleCurtains, "curtainmood":
                                                            featuresSet.insert(kCasaDigiModuleCurtains)
                                                            
                                                        case kCasaDigiModuleAC, kCasaDigiModuleFan:
                                                            featuresSet.insert(kCasaDigiModuleAC)
                                                            
                                                        case kCasaDigiModuleTelevision:
                                                            featuresSet.insert(kCasaDigiModuleTelevision)
                                                            
                                                            
                                                        default:
                                                            if(features == kCasaDigiModuleDigitalMedia && key != "audio")
                                                            {
                                                                featuresSet.insert(kCasaDigiModuleVideos)
                                                                
                                                            }
                                                            featuresSet.insert(features)
                                                            
                                                        }
                                                        
                                                        if(homePermission_other.count > 0 && homePermission_other.contains(kCasaDigiModuleMultiZoneMusic))
                                                        {
                                                            let info = (RoomId: subroom["room_id"] as! String, ModuleType: kCasaDigiModuleMultiZoneMusic)
                                                            featuresInfoArry.append(info)
                                                            
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                            
                                            subRoomInfoArry.append([
                                                "SubRoomId" : subroom["room_id"] as! String,
                                                "SubRoomName" : subroom["room_name"] as! String
                                                ])
                                            // For backword compatability
                                            if subroom["is_module_exists"] != nil{
                                                if(subroom["is_module_exists"]! as! Bool){
                                                    dashboardSubRoomInfoArry.append([
                                                        "SubRoomId" : subroom["room_id"] as! String,
                                                        "SubRoomName" : subroom["room_name"] as! String,
                                                        "SubRoomFeature" : subRoomFeatureArray
                                                        ])
                                                }
                                            }
                                            
                                            
                                            
                                        }
                                    }
                                }
                                
                            }
                        }
                        
                    }
                    
                    let infoDic: Dictionary <String, Any> = [
                        "RoomId": room_Id,
                        "RoomName": roomName,
                        "FloorName": floorName,
                        "IconsInfo": (iconName, iconNameSel, iconNameImg),
                        "SubRoomInfo": subRoomInfoArry,
                        "DashboardSubRoomInfo": dashboardSubRoomInfoArry,
                        "RoomFeatures": Array(featuresSet),
                        "RoomFeaturesInfo": featuresInfoArry,
                        
                        ]
                    roomInfo.append(infoDic)
                    
                    
                }
            }
            
            
            
            return (roomInfo, floorInfoArray, homeFeatureArray)
        }
        
    }
    
    public func getAllModuleIdsInfo() -> [(String, String)] {
        var moduleIds = [(String, String)]()
        let sqlQuery = "SELECT room_id, controller_id FROM room_controller"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        if !database.open() {
            return moduleIds
        } else {
            if let rs = database.query(sqlQuery){
                while rs.next() {
                    var roomId = "0"
                    var moduleId = "0"
                    
                    if let temproomId = rs.stringForColumnIndex(0) {
                        roomId = temproomId
                    }
                    
                    if let tempmoduleId = rs.stringForColumnIndex(1) {
                        moduleId = tempmoduleId
                    }
                    
                    moduleIds.append((roomId, moduleId))
                }
            }
        }
        
        return moduleIds
    }
    
    public func getModuleIdsForRoomEquip() -> [(String, String, String)] {
        var moduleIds = [(String, String, String)]()
        
        let sqlQuery = "SELECT CASE WHEN r.parent_room_id = 0 THEN r.room_id ELSE r.parent_room_id end as room_id, p.controller_id, p.room_id FROM `room_controller` as p, room_info as r WHERE p.room_id = r.room_id"
        
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open(){
            return moduleIds
        } else {
            if let rs = database.query(sqlQuery){
                while rs.next() {
                    var roomId = "0"
                    var moduleId = "0"
                    var roomToSend = "0"
                    
                    if let temproomId = rs.stringForColumnIndex(0) {
                        roomId = temproomId
                    }
                    
                    if let tempmoduleId = rs.stringForColumnIndex(1) {
                        moduleId = tempmoduleId
                    }
                    
                    if let tempRoomToSend = rs.stringForColumnIndex(2) {
                        roomToSend = tempRoomToSend
                    }
                    
                    moduleIds.append((roomId, moduleId, roomToSend))
                }
            }
        }
        
        return moduleIds
    }
    
    public func getMasterModuleId() -> String {
        return "1"
    }
    
    public func getMusicInfoForFileName(_ fileName: String) -> MusicInfoData? {
        let sqlQuery = "SELECT song_album, title, song_thumb FROM song_detail WHERE song_title = '\(fileName)'"
        
        var musicInfo: MusicInfoData?
        var statement: OpaquePointer? = nil
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        if !database.open() {
            return musicInfo
        } else {
            defer{
                database.close()
            }
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    musicInfo = MusicInfoData()
                    
                    if let tempmusicAlbum = rs.stringForColumnIndex(0) {
                        musicInfo!.musicAlbum = tempmusicAlbum
                    } else {
                        musicInfo!.musicAlbum = ""
                    }
                    
                    if let tempmusicTitle = rs.stringForColumnIndex(1) {
                        musicInfo!.musicTitle = tempmusicTitle
                    } else {
                        musicInfo!.musicTitle = ""
                    }
                    
                    if let tempmusicThumbImg = rs.stringForColumnIndex(2) {
                        musicInfo!.musicThumbImg = tempmusicThumbImg
                    } else {
                        musicInfo!.musicThumbImg = ""
                    }
                }
            }
            
            return musicInfo
        }
    }
    
    public func getFMInfoForStationId(_ stationId: String,room_id: String = "") -> FMInfoData? {
        var fmInfo: FMInfoData?
        
        
        var fmInfoArry = [FMInfoData]()
        let sqlQuery = "SELECT room_controller_id, value FROM fm where room_id = \(room_id)"
        let statement: OpaquePointer? = nil
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return fmInfo
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let moduleId = rs.stringForColumnIndex(0) ?? ""
                    let value = rs.stringForColumnIndex(1) ?? ""
                    let json = JSON.init(parseJSON: value)
                    
                    for fmchannels in json.array! {
                        let fmInfoObj = FMInfoData()
                        fmInfoObj.stationId = fmchannels["fm_id"].stringValue
                        fmInfoObj.displayName = fmchannels["name"].stringValue
                        fmInfoObj.frequency = fmchannels["frequency"].stringValue
                        fmInfoObj.iconName = fmchannels["default_icon"].stringValue
                        fmInfoObj.moduleId = moduleId
                        fmInfoArry.append(fmInfoObj)
                    }
                }
            }
            
            sqlite3_finalize(statement)
        }
        var index = 0
        for fmId  in fmInfoArry {
            if fmId.stationId == stationId{
                fmInfo = fmInfoArry[index]
                break
            }
            index += 1
            
            
        }
        
        return fmInfo
        
        
    }
    
    public func getControllerId() -> Bool{
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        if !database.open(){
            return false
        } else {
            let sqlQuery = "select controller_type_id from room_controller where controller_type_id = 6"
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    return true
                }
                
            }else{
                return false
            }
            
        }
        
        
        return false
    }
    
    public func getLightDetailsForType(_ type: String?, roomId: String) -> [Dictionary <String, AnyObject>] {
        
        var lightsArry = [Dictionary <String, AnyObject>]()
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open(){
            return lightsArry
        } else {
            let sqlQuery = "SELECT automation_id, value from light where room_id = \(roomId)"
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let key = rs.stringForColumnIndex(0)!
                    var value = rs.stringForColumnIndex(1)!
                    value = value.replacingOccurrences(of: "\\", with: "")
                    let data = value.data(using: String.Encoding.utf8)
                    
                    do {
                        let jsonVal = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,AnyObject>
                        
                        if let jsonDic = jsonVal[key] as? NSDictionary {
                            
                            let defImgName = jsonDic["default_icon"]!
                            let selImgName = jsonDic["selected_icon"]!
                            let xVal = jsonDic["x_cordinate"]!
                            let yVal = jsonDic["y_cordinate"]!
                            
                            var roomControllerId = "1"
                            if let temproomControllerId = jsonDic["room_controller_id"] {
                                roomControllerId = "\(temproomControllerId)"
                            }
                            let automationId = jsonDic["automation_id"]!
                            let driverType = jsonDic["device_category"]!
                            let displayName = jsonDic["display_name"]!
                            let lightbtn = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
                            let room_image_name = jsonDic["image_name"]!
                            
                            var position = 0
                            
                            if let tempposition = jsonDic["position"] as? String {
                                position = Int(tempposition)!
                            }
                            
                            var is_color = false
                            
                            if let tempColor = jsonDic["is_color"] as? String {
                                is_color = tempColor.toBool()
                            }
                            
                            var isPoolSpaLight = false
                            print("ispool \(jsonDic["is_pool_spa"])")
                            if let tempIsPoolSpaLight = jsonDic["is_pool_spa"] as? String {
                                isPoolSpaLight = tempIsPoolSpaLight.toBool()
                            }
                            
                            let colorImgName = jsonDic["color_image"] as? String ?? ""
                            
                            let lightDic = [
                                "defImageName": defImgName,
                                "selImageName": selImgName,
                                "xVal": xVal,
                                "yVal": yVal,
                                "moduleId": roomControllerId,
                                "unitId": automationId,
                                "lightbtn": lightbtn,
                                "driverType": driverType,
                                "displayName": displayName,
                                "room_image_name": room_image_name,
                                "position": position,
                                "type": type!,
                                "is_color": is_color,
                                "color_image": colorImgName,
                                "isPoolSpaLight": isPoolSpaLight
                            ]
                            
                            if isPoolSpaLight {
                                let poolSpa = roomManager.getRoomsInfoForModuleType("pool_spa")
                                if poolSpa.count > 0 {
                                    if let firstObj = poolSpa.first {
                                        let poolspaRoomid = firstObj["RoomId"]
                                        if poolspaRoomid == roomId {
                                            lightsArry.append(lightDic as [String : AnyObject])
                                        }
                                    }
                                }
                            }
                            else {
                                lightsArry.append(lightDic as [String : AnyObject])
                            }
                            
                        }
                    } catch {
                        //FIXME: HANDLE EXCEPTION.
                    }
                }
            }
            
            lightsArry.sort {
                item1, item2 in
                let str1 = item1["position"]! as! Int
                let str2 = item2["position"]! as! Int
                
                return str1 < str2
            }
            
            return lightsArry
        }
        
    }
    
    public func getControllerType(){
        
        //let sqlQuery = "SELECT automation_id, value from \(moodType) where room_id = \(roomId)"
    }
    
    public func getMoodsInfoForRoomId(_ roomId: String, moodType:String = "NA", moodDefine: String = "") -> [[String:String]] {
        
        let sqlQuery = "SELECT automation_id, value from \(moodType) where room_id = \(roomId)"
        
        var moodsArry = [[String:String]]()
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open(){
            return moodsArry
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let key = rs.stringForColumnIndex(0)!
                    var value = rs.stringForColumnIndex(1)!
                    value = value.replacingOccurrences(of: "\\", with: "")
                    let data = value.data(using: String.Encoding.utf8)
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,AnyObject>
                        
                        if let jsonDic = json[key] as? NSDictionary {
                            let unitId = jsonDic["automation_id"]! as! String
                            let roomControllerId = jsonDic["room_controller_id"]! as! String
                            let displayName = jsonDic["display_name"]! as! String
                            let defImageName = jsonDic["default_icon"]! as! String
                            let selImageName = jsonDic["selected_icon"]! as! String
                            let device_category = jsonDic["device_category"]! as! String
                            let position = jsonDic["position"] as! String
                            let moodDic = [
                                "defImageName": defImageName,
                                "selImageName": selImageName,
                                "displayName": displayName,
                                "moduleId": roomControllerId,
                                "unitId": unitId,
                                "position": position,
                                "device_category": device_category
                            ]
                            moodsArry.append(moodDic)
                        }
                    } catch {
                        //FIXME: HANDLE EXCEPTION.
                    }
                }
            }
            
            moodsArry.sort {
                item1, item2 in
                
                let str1 = item1["position"]! as String
                let str2 = item2["position"]! as String
                
                return Int(str1)! < Int(str2)!
            }
            
            return moodsArry
        }
        
    }
    
    public func getLightIdsLinkedWithMoodId(_ mood_id: String) -> [Dictionary<String, String>] {
        
        var arr = [Dictionary<String, String>]()
        let sqlQuery = "SELECT automation_id, value FROM custom_definition where custom_id = \(mood_id)"
        print("sql \(sqlQuery)")
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return arr
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let unitId = rs.stringForColumnIndex(0)
                    let value = rs.stringForColumnIndex(1)
                    let lightDic = [
                        "unitId": unitId,
                        "value": value
                    ]
                    // print("unit id \(unitId) and val \(value)")
                    arr.append(lightDic as! [String : String])
                }
            }
        }
        
        return arr
        
        
    }
    
    public func getCurtainsInfoForRoomId(_ roomId: String, isPanoramic: Bool) -> [CurtainsInfoData] {
        
        var curtainsArry = [CurtainsInfoData]()
        let sqlQuery = "SELECT automation_id, value from curtain where room_id = \(roomId)"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open(){
            return curtainsArry
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = CurtainsInfoData()
                    let key = rs.stringForColumnIndex(0)!
                    var value = rs.stringForColumnIndex(1)!
                    value = value.replacingOccurrences(of: "\\", with: "")
                    let data = value.data(using: String.Encoding.utf8)
                    
                    do {
                        let jsonVal = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,AnyObject>
                        
                        if let jsonDic = jsonVal[key] as? NSDictionary {
                            let xVal = jsonDic["x_cordinate"]!
                            let yVal = jsonDic["y_cordinate"]!
                            
                            infoObj.moduleId = jsonDic["room_controller_id"]! as! String
                            infoObj.unitId = jsonDic["automation_id"]! as! String
                            infoObj.driverType = jsonDic["device_category"]! as! String
                            infoObj.displayName = jsonDic["display_name"]! as! String
                            infoObj.room_image_name = jsonDic["image_name"]! as! String
                            infoObj.centerX = (xVal as AnyObject).doubleValue ?? 0.0
                            infoObj.centerY = (yVal as AnyObject).doubleValue ?? 0.0
                            
                            if let tempposition = jsonDic["position"] as? String {
                                infoObj.position = Int(tempposition)!
                            }
                            
                            if let uitypeNo = jsonDic["curtain_ui_type_id"] as? NSString {
                                infoObj.typeId = uitypeNo.integerValue
                            }
                            
                            curtainsArry.append(infoObj)
                        }
                    } catch {
                        //FIXME: HANDLE EXCEPTION.
                    }
                }
            }
            
            curtainsArry.sort {
                item1, item2 in
                
                let str1 = item1.position
                let str2 = item2.position
                
                return str1 < str2
            }
            
            return curtainsArry
        }
        
    }
    
    public func getACDeviceInforRoomId(_ roomId: String) -> [ACDeviceInfo] {
        
        print("config db path \(self.configurationDatabasePath)")
        var infoObjArr = [ACDeviceInfo]()
        let sqlQuery = "SELECT automation_id, value from ac where room_id = \(roomId)"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return infoObjArr
        } else {
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    var unitId = "", moduleId = "", displayName = "", minTemp = "", maxTemp = "", modeStr = ""
                    var isFanSpeed = true, isOnOff = true, isACTemp = true, isUnitChanging = false, isRoomTemp = true
                    var defaultUnit = "C"
                    var fanSpeed: String?
                    
                    let infoObj = ACDeviceInfo()
                    let key = rs.stringForColumnIndex(0)!
                    var value = rs.stringForColumnIndex(1)!
                    value = value.replacingOccurrences(of: "\\", with: "")
                    let data = value.data(using: String.Encoding.utf8)
                    
                    var position = 0
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,AnyObject>
                        
                        if let jsonDic = json[key] as? NSDictionary {
                            unitId = jsonDic["automation_id"]! as! String
                            moduleId = jsonDic["room_controller_id"]! as! String
                            displayName = jsonDic["display_name"]! as! String
                            minTemp = jsonDic["min_temp"]! as! String
                            maxTemp = jsonDic["max_temp"]! as! String
                            
                            if let tempFanSpeed = jsonDic["fan_speed"] as? String {
                                infoObj.fanSpeed = tempFanSpeed
                            }
                            if let tempFanSpeedArr = jsonDic["fanspeed"] as? NSArray {
                                infoObj.fanSpeed = nil
                                //We are nilling fanspeed because when fan speed JSON (NSArray) is there we don't need fanspeed. And on basis of checking that if infoObj.fanSpeed = nil we have decided which logic of code needs to be executed in AC class. so nilling infoObj.fanSpeed is very important.
                                infoObj.fanSpeedArray = tempFanSpeedArr
                            }
                            
                            modeStr = jsonDic["mode"]! as! String
                            
                            if jsonDic["is_fahrenheit"] as! String == "1"  {
                                isUnitChanging = true
                            }
                            
                            if jsonDic["is_fanspeed"] as! String == "0"  {
                                isFanSpeed = false
                            }
                            else {
                                print("fan true \(isFanSpeed)")
                            }
                            
                            if jsonDic["is_onoff"] as! String == "0"  {
                                isOnOff = false
                            }
                            
                            if jsonDic["is_actemp"] as! String == "0"  {
                                isACTemp = false
                            }
                            
                            if jsonDic["room_temp"] as! String == "0"  {
                                isRoomTemp = false
                            }
                            
                            if let tempposition = jsonDic["position"] as? String {
                                position = Int(tempposition)!
                            }
                            if let defUnit = jsonDic["default_temperature"]  as? String {
                                defaultUnit = defUnit
                            }
                        }
                    } catch {
                        //FIXME: MISSING IMPLEMENTATION.
                    }
                    
                    infoObj.unitId = unitId
                    infoObj.moduleId = moduleId
                    infoObj.minTemp = minTemp
                    infoObj.maxTemp = maxTemp
                    //                        infoObj.fanSpeed = fanSpeed
                    infoObj.displayName = displayName
                    infoObj.isFanSpeed = isFanSpeed
                    infoObj.isOnOff = isOnOff
                    infoObj.isACTemp = isACTemp
                    infoObj.isUnitChanging = isUnitChanging
                    infoObj.isRoomTemp = isRoomTemp
                    infoObj.modeStr = modeStr
                    infoObj.position = position
                    infoObj.defaultUnit = defaultUnit
                    
                    infoObjArr.append(infoObj)
                    
                }
            }
            infoObjArr.sort {
                item1, item2 in
                let str1 = item1.position
                let str2 = item2.position
                
                return str1 < str2
            }
            
            return infoObjArr
        }
        
    }
    
    public func getFansInArrayForRoomId(_ roomId: String) -> [Dictionary<String, String>] {
        
        let sqlQuery = "SELECT automation_id, value from fan where room_id = \(roomId)"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            let fanArr = [Dictionary<String, String>]()
            
            return fanArr
        } else {
            var fanDic: Dictionary<String, String>
            var fanArr = [Dictionary<String, String>]()
            var unitId = "", moduleId = "", displayName = "", fanSpeed = ""
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let key = rs.stringForColumnIndex(0)!
                    let value = rs.stringForColumnIndex(1)!
                    let data = value.data(using: String.Encoding.utf8)
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,AnyObject>
                        
                        if let jsonDic = json[key] as? NSDictionary {
                            if let tempUnitId = jsonDic["automation_id"] as? String {
                                unitId = tempUnitId
                            }
                            
                            if let tempModId = jsonDic["room_controller_id"] as? String {
                                moduleId = tempModId
                            }
                            
                            if let tempdisplayName = jsonDic["display_name"] as? String {
                                displayName = tempdisplayName
                            }
                            
                            if let tempfanSpeed = jsonDic["fan_speed"] as? String {
                                fanSpeed = tempfanSpeed
                            }
                            
                            fanDic = [
                                "unitId": unitId,
                                "moduleId": moduleId,
                                "displayName": displayName,
                                "fanSpeed": fanSpeed
                            ]
                            
                            fanArr.append(fanDic)
                        }
                    } catch {
                        //FIXME: HANDLE EXCEPTION.
                    }
                }
            }
            
            return fanArr
        }
        
    }
    
    public func getAcModesInArrayForRoomId(_ roomId: String) -> String {
        let sqlQuery = "SELECT mode from ac_info_master where model in (Select driver_name from user_device_configuration_mc where room_id = \(roomId))"
        var modeStr = ""
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return modeStr
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    modeStr = rs.stringForColumnIndex(0) ?? ""
                }
            }
        }
        
        return modeStr
    }
    
    public func getModesWithImages(_ modeStr: String) -> [Dictionary<String, String>] {
        
        let sqlQuery = "SELECT mode, default_icon FROM `CD_ac_mode`, CD_icon WHERE id IN (\(modeStr)) AND CD_ac_mode.icon_id = CD_icon.icon_id"
        
        
        var modeDic: Dictionary<String, String>
        var modeArr = [Dictionary<String, String>]()
        var modeName = "", modeImgName = ""
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return modeArr
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    modeName = rs.stringForColumnIndex(0) ?? ""
                    modeImgName = rs.stringForColumnIndex(1) ?? ""
                    modeDic = [
                        "modeName": modeName,
                        "modeImgName": modeImgName
                    ]
                    if modeName != "eco" {
                        modeArr.append(modeDic)
                    }
                }
            }
        }
        
        return modeArr
    }
    
    public func getAllTVChannelCategory(_ room_id: String) -> [[String:String]] {
        
        var categoryArry: [[String:String]] = []
        let sqlQuery = "Select TCM.id, CTC.id as category_id, CTC.name from tvchannel_category_mapping as TCM, CD_tvchannel_category as CTC where TCM.room_id = '\(room_id)' and TCM.category_id = CTC.id AND TCM.is_disable = 0 ORDER BY TCM.position"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return categoryArry
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let main_cat_Id = rs.stringForColumnIndex(0) ?? ""
                    let cat_Id = rs.stringForColumnIndex(1) ?? ""
                    let name = rs.stringForColumnIndex(2) ?? ""
                    categoryArry.append(["CategoryId" : cat_Id, "lang_Id": main_cat_Id, "Name" : name])
                }
            }
            if categoryArry.count > 0 {
                if checkIfFavouritesTableExists() {
                    categoryArry.insert(["CategoryId" : "-200", "lang_Id": "-200", "Name" : "Favourites"], at: 1)
                }
            }
            return categoryArry
        }
        
    }
    
    public func checkIfFavouritesTableExists() -> Bool {
        //favourite table is common in both v1 & v2 sqlites
        var exists = false
        let query = "SELECT name FROM sqlite_master WHERE type='table' AND name='favourite'"
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return exists
        } else {
            defer{
                database.close()
            }
            
            if let rs = database.query(query) {
                while rs.next() {
                    exists = true
                    self.createFavTblinConfigDB()
                    return exists
                }
            }
        }
        
        return exists
    }
    
    public func createFavTblinConfigDB() {
        //CREATE TABLE favourite ( fid integer NOT NULL primary key autoincrement, vendor_id integer NOT NULL, user_id integer NOT NULL DEFAULT '1', feature_id integer NOT NULL, room_id integer NOT NULL DEFAULT '0', item_id integer NOT NULL )
        var exists = false
        let query = "SELECT name FROM sqlite_master WHERE type='table' AND name='favourite'"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
        } else {
            defer{
                database.close()
            }
            
            if let rs = database.query(query) {
                while rs.next() {
                    exists = true
                }
            }
        }
        if !exists {
            let query = "CREATE TABLE favourite ( fid integer NOT NULL primary key autoincrement, vendor_id integer NOT NULL, user_id integer NOT NULL DEFAULT '1', feature_id integer NOT NULL, room_id integer NOT NULL DEFAULT '0', item_id integer NOT NULL )"
            let database = SQDatabase.init(path: self.configurationDatabasePath)
            
            if !database.open() {
                
            } else {
                defer{
                    database.close()
                }
                
                if let rs = database.query(query) {
                    while rs.next() {
                        print("created")
                    }
                }
            }
        }
    }
    
    public func addFavChannelInFavTable(_ item_id: String, room room_id: String, user userId: String, fid: String?) -> String {
        var newfid = ""
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        if !database.open() {
            return newfid
        } else {
            defer{
                database.close()
            }
            
            var insert = "INSERT INTO favourite (fid, vendor_id,user_id,feature_id,room_id,item_id) VALUES (\(fid ?? ""),'\(self.deviceVendorID)',\(userId),'11','\(room_id)','\(item_id)')"
            
            if fid == nil {
                insert = "INSERT INTO favourite (vendor_id,user_id,feature_id,room_id,item_id) VALUES ('\(self.deviceVendorID)',\(userId),'11','\(room_id)','\(item_id)')"
            }
            print("insert q \(insert)")
            if database.update(insert)
            {
                newfid = self.readFid(item_id)
            }
            
            return newfid
            
        }
    }
    
    public func readFid(_ item_id: String) -> String {
        var fid = ""
        let sqlQuery = "Select fid from favourite where item_id = '\(item_id)'"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        if !database.open() {
            return fid
        } else {
            var countFlag = false
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    if let tempfid = rs.stringForColumnIndex(0) {
                        fid = tempfid
                    }
                }
            }
            
            return fid
        }
    }
    
    public func delFavChannelFromFavTable(fid: String) {
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        if !database.open() {
            return
        } else {
            defer{
                database.close()
            }
            
            let insert = "Delete from favourite where fid = '\(fid)'"
            print("del q \(insert)")
            if database.update(insert)
            {
                print("DElete succesfully fid \(fid)")
            }
        }
    }
    
    public func getLanguagesInCategory(_ cat_id: String, room room_id: String) -> [Dictionary<String, String>] {
        
        var langArry: [Dictionary<String, String>] = []
        let sqlQuery = "Select TLM.language_id, CTL.name from tvchannel_language_mapping as TLM, CD_tvchannel_language as CTL where TLM.room_id = '\(room_id)' and TLM.language_id = CTL.id and TLM.category_map_id = '\(cat_id)' AND TLM.is_disable = 0 ORDER BY TLM.position"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return langArry
        } else {
            var countFlag = false
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    var lang_Id = "0"
                    var name = ""
                    
                    if let tempLangId = rs.stringForColumnIndex(0) {
                        lang_Id = tempLangId
                    }
                    
                    if let tempname   = rs.stringForColumnIndex(1) {
                        name = tempname
                    }
                    
                    langArry.append(["lang_Id" : lang_Id, "name" : name])
                    countFlag = true
                }
            }
            
            if countFlag {
                langArry.insert(["lang_Id" : "-1001", "name" : "All Languages"], at: 0)
            }
            else {
                if cat_id == "-200" //Favourites category
                {
                    langArry.insert(["lang_Id" : "-1001", "name" : "All Languages"], at: 0)
                }
            }
            
            return langArry
        }
        
    }
    
    public func getTVChannelsInfoForRoomId(_ roomId: String) -> [ChannelInfoData] {
        
        
        
        var sqlQuery = ""
        
        if checkIfFavouritesTableExists() {
            sqlQuery = "SELECT tc.category_id, tc.id, tc.channel_name, tc.selected_icon, tc.language_id, f.fid, f.vendor_id, f.user_id, tc.channel_id FROM tvchannel tc LEFT JOIN favourite f ON tc.channel_id = f.item_id WHERE tc.room_id = \(roomId) GROUP BY channel_number ORDER BY position"
        }
        else {
            sqlQuery = "SELECT category_id, id, channel_name, selected_icon, language_id FROM tvchannel WHERE room_id = \(roomId) GROUP BY channel_number ORDER BY position"
        }
        
        var infoArray: [ChannelInfoData] = []
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return infoArray
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = ChannelInfoData()
                    
                    if let channelCatId = rs.stringForColumnIndex(0) {
                        infoObj.channelCatId = channelCatId
                    }
                    
                    if let channelNo = rs.stringForColumnIndex(1) {
                        infoObj.channelNo = channelNo
                    }
                    
                    if let channelName = rs.stringForColumnIndex(2) {
                        infoObj.channelName = channelName
                    }
                    
                    if let channelIconName = rs.stringForColumnIndex(3) {
                        infoObj.channelIconName = channelIconName
                    }
                    
                    if let channelLanguageId = rs.stringForColumnIndex(4) {
                        infoObj.channelLanguageId = channelLanguageId
                    }
                    
                    if let fid = rs.stringForColumnIndex(5) {
                        infoObj.fid = fid
                    }
                    
                    var isValidFav = false
                    var favUserId = ""
                    var favVendorId = ""
                    
                    if let tempfavVendorId = rs.stringForColumnIndex(6) {
                        favVendorId = tempfavVendorId
                    }
                    
                    if let favuser = rs.stringForColumnIndex(7) {
                        favUserId = favuser
                    }
                    
                    if let channelId = rs.stringForColumnIndex(8) {
                        infoObj.channel_id = channelId
                    }
                    
                    if let userId = self.loggedInUserInfo?.userId {
                        if (favVendorId == self.deviceVendorID && favUserId == "1") || favUserId == userId {
                            //                                print("In login isvalid for \(infoObj.channelNo)")
                            isValidFav = true
                        }
                    }
                    else {
                        if favVendorId == self.deviceVendorID && favUserId == "1" {
                            //                                print("isvalid for \(infoObj.channelNo)")
                            isValidFav = true
                        }
                    }
                    
                    if !isValidFav {
                        infoObj.fid = nil
                    }
                    
                    infoArray.append(infoObj)
                }
            }
            
            return infoArray
        }
        
    }
    
    public func getTVChannelsInfoForCatId(_ catId: String, roomId: String) -> [ChannelInfoData] {
        
        
        var sqlQuery = ""
        
        if checkIfFavouritesTableExists() {
            sqlQuery = "SELECT tc.category_id, tc.id, tc.channel_name, tc.selected_icon, tc.language_id, f.fid, f.vendor_id, f.user_id, tc.channel_id FROM tvchannel tc LEFT JOIN favourite f ON tc.channel_id = f.item_id WHERE tc.room_id = \(roomId) AND tc.category_id = \(catId) GROUP BY channel_number ORDER BY position"
        }
        else {
            sqlQuery = "SELECT category_id, id, channel_name, selected_icon, language_id FROM tvchannel WHERE room_id = \(roomId) AND category_id = \(catId) ORDER BY position"
        }
        
        print("sql \(sqlQuery)")
        var infoArray: [ChannelInfoData] = []
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return infoArray
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = ChannelInfoData()
                    
                    if let channelCatId = rs.stringForColumnIndex(0) {
                        infoObj.channelCatId = channelCatId
                    }
                    
                    if let channelNo = rs.stringForColumnIndex(1) {
                        infoObj.channelNo = channelNo
                    }
                    
                    if let channelName = rs.stringForColumnIndex(2) {
                        infoObj.channelName = channelName
                    }
                    
                    if let channelIconName = rs.stringForColumnIndex(3) {
                        infoObj.channelIconName = channelIconName
                    }
                    
                    if let channelLanguageId = rs.stringForColumnIndex(4) {
                        infoObj.channelLanguageId = channelLanguageId
                    }
                    
                    if let fid = rs.stringForColumnIndex(5) {
                        infoObj.fid = fid
                    }
                    
                    var isValidFav = false
                    var favUserId = ""
                    var favVendorId = ""
                    
                    if let tempfavVendorId = rs.stringForColumnIndex(6) {
                        favVendorId = tempfavVendorId
                    }
                    
                    if let favuser = rs.stringForColumnIndex(7) {
                        favUserId = favuser
                    }
                    
                    if let channelId = rs.stringForColumnIndex(8) {
                        infoObj.channel_id = channelId
                    }
                    
                    if let userId = self.loggedInUserInfo?.userId {
                        if (favVendorId == self.deviceVendorID && favUserId == "1") || favUserId == userId {
                            //                                print("In login isvalid for \(infoObj.channelNo)")
                            isValidFav = true
                        }
                    }
                    else {
                        if favVendorId == self.deviceVendorID && favUserId == "1" {
                            //                                print("isvalid for \(infoObj.channelNo)")
                            isValidFav = true
                        }
                    }
                    
                    if !isValidFav {
                        infoObj.fid = nil
                    }
                    
                    infoArray.append(infoObj)
                }
            }
            
            return infoArray
        }
        
    }
    
    public func getFavTVChannelsInfoForRoomId(_ roomId: String) -> [ChannelInfoData] {
        var infoArray: [ChannelInfoData] = []
        
        var sqlQuery = ""
        sqlQuery = "SELECT tc.category_id, tc.id, tc.channel_name, tc.selected_icon, tc.language_id, f.fid, f.vendor_id, f.user_id, tc.channel_id FROM tvchannel tc JOIN favourite f ON tc.channel_id = f.item_id WHERE tc.room_id = \(roomId) GROUP BY channel_number ORDER BY fid"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return infoArray
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = ChannelInfoData()
                    
                    if let channelCatId = rs.stringForColumnIndex(0) {
                        infoObj.channelCatId = channelCatId
                    }
                    
                    if let channelNo = rs.stringForColumnIndex(1) {
                        infoObj.channelNo = channelNo
                    }
                    
                    if let channelName = rs.stringForColumnIndex(2) {
                        infoObj.channelName = channelName
                    }
                    
                    if let channelIconName = rs.stringForColumnIndex(3) {
                        infoObj.channelIconName = channelIconName
                    }
                    
                    if let channelLanguageId = rs.stringForColumnIndex(4) {
                        infoObj.channelLanguageId = channelLanguageId
                    }
                    
                    if let fid = rs.stringForColumnIndex(5) {
                        infoObj.fid = fid
                    }
                    
                    var isValidFav = false
                    var favUserId = ""
                    var favVendorId = ""
                    
                    if let tempfavVendorId = rs.stringForColumnIndex(6) {
                        favVendorId = tempfavVendorId
                    }
                    
                    if let favuser = rs.stringForColumnIndex(7) {
                        favUserId = favuser
                    }
                    
                    if let channelId = rs.stringForColumnIndex(8) {
                        infoObj.channel_id = channelId
                    }
                    
                    if let userId = self.loggedInUserInfo?.userId {
                        if (favVendorId == self.deviceVendorID && favUserId == "1") || favUserId == userId {
                            print("In login isvalid for \(infoObj.channelNo)")
                            isValidFav = true
                        }
                    }
                    else {
                        if favVendorId == self.deviceVendorID && favUserId == "1" {
                            //                                print("isvalid for \(infoObj.channelNo)")
                            isValidFav = true
                        }
                    }
                    
                    if !isValidFav {
                        infoObj.fid = nil
                    }
                    else {
                        infoArray.append(infoObj)
                    }
                }
            }
            
            return infoArray
        }
        
    }
    
    public func getAllFavChannelsForUser(_ userId: String) -> [Dictionary<String, String>] {
        var channelNoArr = [Dictionary<String, String>]()
        
        var sqlQuery = "SELECT item_id, fid from favourite where (user_id = '1' or user_id = '\(userId)' )  and vendor_id = '\(deviceVendorID)'"
        
        if userId == "1" {
            sqlQuery = "SELECT item_id, fid from favourite where (user_id = '1') and vendor_id = '\(deviceVendorID)'"
        }
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return channelNoArr
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    
                    let number = rs.stringForColumnIndex(0) ?? "0"
                    let fid = rs.stringForColumnIndex(1) ?? ""
                    channelNoArr.append(["channel_id" : number, "fid" : fid])
                }
            }
            return channelNoArr
        }
    }
    
    public func getTVRemoteOptionsV2(_ roomId: String, unitId: String) -> String {
        var jsonString = ""
        
        let sqlQuery = "SELECT value FROM device_remote WHERE entertainment_id = \(unitId) AND room_id = \(roomId)"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return jsonString
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    
                    if let value = rs.stringForColumnIndex(0) {
                        jsonString = value
                    }
                }
            }
            
            return jsonString
        }
    }
    
    public func getTVRemoteOptionsV1(_ roomId: String) -> (String, String) {
        var details = ("", "")
        
        let sqlQuery = "SELECT button_display, button_command FROM set_top_box WHERE model in(Select driver_name from user_device_configuration_mc where driver_type like 'stb' AND room_id = \(roomId))"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return details
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    
                    if let displayName = rs.stringForColumnIndex(0) {
                        details.0 = displayName
                    }
                    
                    if let cmd = rs.stringForColumnIndex(1) {
                        details.1 = cmd
                    }
                }
            }
            
            return details
        }
    }
    
    public func getTVChannelSearched(_ roomId: String, searchStr: String) -> [ChannelInfoData] {
        
        let sqlQuery = "SELECT tc.category_id, tc.id, tc.channel_name, tc.selected_icon, tc.language_id, f.fid, f.vendor_id, f.user_id, tc.channel_id FROM tvchannel tc LEFT JOIN favourite f ON tc.channel_id = f.item_id WHERE (tc.channel_name LIKE '\(searchStr)%' OR tc.channel_name LIKE '% \(searchStr)%') AND tc.room_id = \(roomId) GROUP BY channel_number ORDER BY position"
        
        var infoArray: [ChannelInfoData] = []
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return infoArray
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = ChannelInfoData()
                    
                    if let channelCatId = rs.stringForColumnIndex(0) {
                        infoObj.channelCatId = channelCatId
                    }
                    
                    if let channelNo = rs.stringForColumnIndex(1) {
                        infoObj.channelNo = channelNo
                    }
                    
                    if let channelName = rs.stringForColumnIndex(2) {
                        infoObj.channelName = channelName
                    }
                    
                    if let channelIconName = rs.stringForColumnIndex(3) {
                        infoObj.channelIconName = channelIconName
                    }
                    
                    if let channelLanguageId = rs.stringForColumnIndex(4) {
                        infoObj.channelLanguageId = channelLanguageId
                    }
                    if let fid = rs.stringForColumnIndex(5) {
                        infoObj.fid = fid
                    }
                    
                    var isValidFav = false
                    var favUserId = ""
                    var favVendorId = ""
                    
                    if let tempfavVendorId = rs.stringForColumnIndex(6) {
                        favVendorId = tempfavVendorId
                    }
                    
                    if let favuser = rs.stringForColumnIndex(7) {
                        favUserId = favuser
                    }
                    
                    if let channelId = rs.stringForColumnIndex(8) {
                        infoObj.channel_id = channelId
                    }
                    
                    if let userId = self.loggedInUserInfo?.userId {
                        if (favVendorId == self.deviceVendorID && favUserId == "1") || favUserId == userId {
                            //                                print("In login isvalid for \(infoObj.channelNo)")
                            isValidFav = true
                        }
                    }
                    else {
                        if favVendorId == self.deviceVendorID && favUserId == "1" {
                            //                                print("isvalid for \(infoObj.channelNo)")
                            isValidFav = true
                        }
                    }
                    
                    if !isValidFav {
                        infoObj.fid = nil
                    }
                    
                    infoArray.append(infoObj)
                }
            }
            
            return infoArray
        }
        
        
    }
    
    public func getDriverNameForCurrentRoomSTB(_ roomId: String) -> String {
        var driverName = "NA"
        let sqlQuery = "SELECT driver_name FROM user_device_configuration_mc WHERE driver_type = 'STB' AND room_id = \(roomId)"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return driverName
        } else {
            if let rs = database.query(sqlQuery){
                while rs.next() {
                    if let name = rs.stringForColumnIndex(0) {
                        driverName = name
                    }
                }
            }
        }
        
        return driverName
    }
    
    public func getAllSurveillanceLocations() -> [SurveillanceInfoData] {
        
        var surveillanceArry = [SurveillanceInfoData]()
        var survillanceId = [String]()
        let path = applicationBundleAppContentDirectoryURL.appendingPathComponent("roomInfo").path as String
        if path != "" {
            let archive = FileManager.default.contents(atPath: path)
            
            if archive != nil {
                
                var roomInfoDictionary =  Dictionary<String, Any>()
                var permissions = [String:Any]()
                var roomLevel = [String:Any]()
                var homeLevel = [String:Any]()
                roomInfoDictionary = NSKeyedUnarchiver.unarchiveObject(with: archive!)! as! Dictionary<String, Any>
                permissions             = roomInfoDictionary["permission"] as! [String:Any]
                
                if(permissions["homelevel"] != nil)
                {
                    homeLevel               = permissions["homelevel"] as! [String:Any]
                }
                
                if(homeLevel.count > 0)
                {
                    
                    let homeLevelPermission = homeLevel["home_permissions"] as! [String:Any]
                    let homePermission_surveillance = homeLevelPermission["surveillance"] as! [String]
                    
                    
                    if(homePermission_surveillance.count > 0)
                    {
                        survillanceId = homePermission_surveillance
                    }
                }
            }
        }
        let sqlQuery = "SELECT url, is_online, location, offline_image_name, location_id FROM surveillance where location_id in ( '\(survillanceId.joined(separator: "','"))') ORDER BY position ASC"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return surveillanceArry
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = SurveillanceInfoData()
                    infoObj.imageURL = rs.stringForColumnIndex(0) ?? "-1"
                    
                    if let isOnline = rs.stringForColumnIndex(1) {
                        if isOnline != "" {
                            infoObj.isOnline = isOnline.toBool()
                        }
                    }
                    
                    
                    infoObj.roomName = rs.stringForColumnIndex(2)!
                    infoObj.offlineImageName = rs.stringForColumnIndex(3)!
                    infoObj.roomId = rs.stringForColumnIndex(4)!
                    let lightSqlQuery = "SELECT automation_id FROM light WHERE room_id = \(infoObj.roomId)"
                    var lightArr = [String]()
                    if let res = database.query(lightSqlQuery) {
                        while res.next() {
                            lightArr.append(res.stringForColumnIndex(0)!)
                        }
                    }
                    infoObj.lightId = lightArr.joined(separator: "|")
                    
                    
                    
                    //Featching mood id's
                    let moodSqlQuery = "SELECT automation_id FROM mood WHERE room_id = \(infoObj.roomId)"
                    var moodArr = [String]()
                    if let res = database.query(moodSqlQuery) {
                        while res.next() {
                            moodArr.append(res.stringForColumnIndex(0)!)
                        }
                    }
                    infoObj.moodId = moodArr.joined(separator: "|")
                    
                    
                    surveillanceArry.append(infoObj)
                }
            }
            
            return surveillanceArry
        }
        
        
    }
    
    public func getLightsInSurveillance(_ lightIds: String, room_id: String, type: String = "light") -> [UtilityInfoData] {
        
        
        var lightsArry = [UtilityInfoData]()
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open(){
            return lightsArry
        } else {
            
            
            let sqlQuery = "SELECT automation_id, value from \(type) where automation_id in (\(lightIds))"
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let key = rs.stringForColumnIndex(0)!
                    var value = rs.stringForColumnIndex(1)!
                    value = value.replacingOccurrences(of: "\\", with: "")
                    let data = value.data(using: String.Encoding.utf8)
                    
                    do {
                        let jsonVal = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,AnyObject>
                        
                        if let jsonDic = jsonVal[key] as? NSDictionary {
                            
                            let roomControllerId = jsonDic["room_controller_id"]!
                            let automationId = jsonDic["automation_id"]!
                            let driverType = jsonDic["device_category"]!
                            let displayName = jsonDic["display_name"]!
                            
                            
                            let infoObj = UtilityInfoData()
                            infoObj.unitId = automationId as! String
                            infoObj.moduleId = roomControllerId as! String
                            infoObj.driverType = driverType as! String
                            infoObj.displayName = displayName as! String
                            //infoObj.driverName = rs.stringForColumnIndex(4) ?? ""
                            lightsArry.append(infoObj)
                        }
                    }
                    catch {
                        return lightsArry
                    }
                }
            }
        }
        
        
        
        return lightsArry
    }
    
    public func getAllUtilitiesInfo() -> [UtilityInfoData] {
        
        var utilityArry = [UtilityInfoData]()
        var sqlQuery = "Select utility_id, room_controller_id, display_name, type, default_icon,options from utility"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        if !database.open() {
            return utilityArry
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = UtilityInfoData()
                    if let unitId = rs.stringForColumnIndex(0) {
                        infoObj.unitId = unitId
                    } else {
                        infoObj.unitId = "-1"
                    }
                    
                    if let moduleId = rs.stringForColumnIndex(1) {
                        infoObj.moduleId = moduleId
                    } else {
                        infoObj.moduleId = "-1"
                    }
                    infoObj.displayName = rs.stringForColumnIndex(2) ?? ""
                    infoObj.driverName = rs.stringForColumnIndex(3) ?? ""
                    infoObj.imageName = rs.stringForColumnIndex(4) ?? ""
                    if  let jsonValue = rs.stringForColumnIndex(5){
                        print(JSON.init(parseJSON: jsonValue))
                        let json = JSON.init(parseJSON: jsonValue)
                        if json["buttons"] != JSON.null {
                            infoObj.buttonInfo = json["buttons"]
                            var tempArr = json["buttons"].arrayValue
                            tempArr.sort {
                                item1, item2 in
                                let str1 = item1["position"]
                                let str2 = item2["position"]
                                return str1 > str2
                            }
                            let type =  json["button_type"]
                            switch type{
                            case "toggle":
                                infoObj.buttonType = .toggle
                            case "button":
                                infoObj.buttonType = .button
                            default:
                                break
                            }
                            infoObj.buttonsInfoArr = tempArr
                        }
                    }
                    utilityArry.append(infoObj)
                }
            }else{
                sqlQuery = "Select utility_id, room_controller_id, display_name, type, default_icon from utility"
                if !database.open() {
                    return utilityArry
                }else {
                    if let rs = database.query(sqlQuery) {
                        while rs.next() {
                            let infoObj = UtilityInfoData()
                            
                            if let unitId = rs.stringForColumnIndex(0) {
                                infoObj.unitId = unitId
                            } else {
                                infoObj.unitId = "-1"
                            }
                            
                            if let moduleId = rs.stringForColumnIndex(1) {
                                infoObj.moduleId = moduleId
                            } else {
                                infoObj.moduleId = "-1"
                            }
                            
                            infoObj.displayName = rs.stringForColumnIndex(2) ?? ""
                            infoObj.driverName = rs.stringForColumnIndex(3) ?? ""
                            infoObj.imageName = rs.stringForColumnIndex(4) ?? ""
                            utilityArry.append(infoObj)
                        }
                    }
                }
            }
            
            return utilityArry
        }
        
    }
    
    public func getAdvanceControl(_ isFromEnt: Bool) -> [AdSettingsInfoData] {
        
        var advanceControlArry = [AdSettingsInfoData]()
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return advanceControlArry
        } else {
            var roomIds = roomManager.currentRoomID
            
            if roomManager.currentRoomSubroomInfo != nil {
                //     if isFromEnt {
                // If sub room is availalbe then query get the advanced controll of selected subroom only
                
                for dictionary in roomManager.currentRoomSubroomInfo {
                    if dictionary["SubRoomId"]! == roomManager.currentSubRoomID
                    {
                        roomIds = dictionary["SubRoomId"]!
                    }
                    
                }
                //   }
                //I comment this line beacuse advance control show according to subroom, if subroom availble else advance control show according room.
                //                    roomIds += ("," + roomManager.currentSubRoomID)
                /*for dictionary in roomManager.currentRoomSubroomInfo {
                 roomIds += ("," + dictionary["SubRoomId"]!)
                 }*/
            }
            
            var sqlQuery = "SELECT advanced_control_id, value from advanced_control where is_roomlevel = '0' and room_id IN (\(roomIds))"
            
            if isFromEnt {
                sqlQuery = "SELECT advanced_control_id, value from advanced_control where is_roomlevel = '1' and room_id IN (\(roomIds))"
            }
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoData = AdSettingsInfoData()
                    let advId = rs.stringForColumnIndex(0) ?? ""
                    let jsonValue = rs.stringForColumnIndex(1)!
                    let json = JSON.init(parseJSON: jsonValue)
                    
                    if json[advId] != JSON.null {
                        let subJson = json[advId]
                        infoData.unitId = subJson["advanced_control_id"].stringValue
                        infoData.moduleId = subJson["room_controller_id"].stringValue
                        infoData.buttonInfo = subJson["buttons"]
                        
                        var tempArr = subJson["buttons"].arrayValue
                        tempArr.sort {
                            item1, item2 in
                            
                            let str1 = item1["position"]
                            let str2 = item2["position"]
                            
                            return str1 < str2
                        }
                        
                        infoData.buttonsInfoArr = tempArr
                        infoData.displayName = subJson["label"].stringValue
                        infoData.position = subJson["position"].intValue
                        
                        let isInActive = subJson["is_active"].boolValue
                        
                        if !isInActive {
                            advanceControlArry.append(infoData)
                        }
                        
                        advanceControlArry.sort {
                            item1, item2 in
                            
                            let str1 = item1.position
                            let str2 = item2.position
                            
                            return str1 < str2
                        }
                    }
                }
            }
            
            return advanceControlArry
        }
        
    }
    //
    public func getModuleType(_ roomID : String) -> Bool{
        let sqlQuery = "select paragon_module_type_id from paragon_module_info where room_id='\(roomID)'"
        
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return false
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let moduleID = rs.stringForColumnIndex(0) ?? ""
                    if moduleID == "3"{
                        return true
                    }else{
                        return false
                    }
                }
            }
        }
        return  false
    }
    
    public func getDigitalMedia(_ mediatype : Int = 1) -> [AdSettingsInfoData] {
        
        // mediatype =  2 -> video 1 -> audio
        
        var advanceControlArry = [AdSettingsInfoData]()
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return advanceControlArry
        } else {
            var roomIds = roomManager.currentRoomID
            
            if roomManager.currentRoomSubroomInfo != nil {
                for dictionary in roomManager.currentRoomSubroomInfo  {
                    roomIds += ("," + dictionary["SubRoomId"]!)
                }
            }
            //entertainment.av_category -> 1 & 2  / 2= video 1= audio
            let sqlQuery = "SELECT entertainment.entertainment_id, entertainment.display_name, entertainment.room_controller_id, entertainment.is_volumebar, CD_icon.default_icon, CD_icon.selected_icon, CD_digitalmedia.is_controllable,CD_digitalmedia.driver FROM entertainment, CD_digitalmedia, CD_icon where CD_digitalmedia.driver_file_name = entertainment.driver_name and CD_digitalmedia.icon_id = CD_icon.icon_id and entertainment.av_category = \(mediatype) and room_id in (\(roomIds))"
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoData = AdSettingsInfoData()
                    let unitID = rs.stringForColumnIndex(0)!
                    let displayName = rs.stringForColumnIndex(1) ?? ""
                    let moduleID = rs.stringForColumnIndex(2) ?? ""
                    let imageName = rs.stringForColumnIndex(4) ?? ""
                    let selectedimageName = rs.stringForColumnIndex(5) ?? ""
                    let controllable = rs.stringForColumnIndex(6) ?? ""
                    let driver = rs.stringForColumnIndex(7) ?? ""
                    
                    //1 //0
                    infoData.unitId = unitID
                    infoData.moduleId = moduleID
                    infoData.displayName = displayName
                    infoData.showVolumeBar = rs.stringForColumnIndex(3)!.toBool()
                    infoData.param1 = imageName
                    infoData.param2 = selectedimageName
                    infoData.param3 = controllable
                    infoData.driverName = driver
                    infoData.deviceCategoryName = "digitalmedia"
                    advanceControlArry.append(infoData)
                }
            }
            
            return advanceControlArry
        }
        
    }
    
    public func getMediaForAppleTv(_ unitID : String, roomId : String) -> [AdSettingsInfoData]{
        
        var advanceControlArry = [AdSettingsInfoData]()
        var roomIds = roomManager.currentRoomID
        
        if roomManager.currentRoomSubroomInfo != nil {
            for dictionary in roomManager.currentRoomSubroomInfo  {
                roomIds += ("," + dictionary["SubRoomId"]!)
            }
        }
        
        let sqlQuery = "select user_device_config_id, display_name, paragon_module_id, scn_id,user_device_param_1, user_device_param_2, user_device_param_3, isset_advance_ctrl, image_id, user_device_param_4, scn_modules, is_volumebar from user_device_configuration_mc where driver_name like 'appletv' and user_device_config_id=(\(unitID)) and  room_id=(\(roomId)) "//and scn_modules='digitalmedia'
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return advanceControlArry
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoData = AdSettingsInfoData()
                    let unitID = rs.stringForColumnIndex(0)
                    let displayName = rs.stringForColumnIndex(1)
                    let moduleID = rs.stringForColumnIndex(2)
                    let sceneID = rs.stringForColumnIndex(3)
                    let param1 = rs.stringForColumnIndex(4)
                    let param2 = rs.stringForColumnIndex(5)
                    let param3 = rs.stringForColumnIndex(6)
                    let isADvCtrl = rs.stringForColumnIndex(7)
                    _  = rs.stringForColumnIndex(8)
                    let param4 = rs.stringForColumnIndex(9)
                    let sceneModule = rs.stringForColumnIndex(10)
                    
                    infoData.unitId = unitID!
                    infoData.moduleId = moduleID!
                    infoData.sceneId = sceneID!
                    infoData.displayName = displayName!
                    infoData.param1 = param1!
                    infoData.param2 = param2!
                    infoData.param3 = param3!
                    infoData.param4 = param4!
                    infoData.isSetAdvCtrl = isADvCtrl!
                    infoData.sceneModule = sceneModule!
                    infoData.showVolumeBar = Int(rs.intForColumnIndex(11)!).toBool()
                    advanceControlArry.append(infoData)
                }
            }
        }
        
        return advanceControlArry
    }
    
    public func getDiskPlayer() -> [AdSettingsInfoData]
    {
        var advanceControlArry = [AdSettingsInfoData]()
        var roomIds = roomManager.currentRoomID
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return advanceControlArry
        } else {
            var roomIds = roomManager.currentRoomID
            
            if roomManager.currentRoomSubroomInfo != nil {
                for dictionary in roomManager.currentRoomSubroomInfo  {
                    roomIds += ("," + dictionary["SubRoomId"]!)
                }
            }
            
            let sqlQuery = "SELECT entertainment.entertainment_id, entertainment.display_name, entertainment.room_controller_id, entertainment.is_volumebar,entertainment.driver_name, entertainment.device_category FROM entertainment where entertainment.device_category = \"discplayer\" and room_id in (\(roomIds))"
            
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoData = AdSettingsInfoData()
                    let unitID = rs.stringForColumnIndex(0)!
                    let displayName = rs.stringForColumnIndex(1) ?? ""
                    let moduleID = rs.stringForColumnIndex(2) ?? ""
                    let deviceCategoryName = rs.stringForColumnIndex(5) ?? ""
                    let imageName = "VideoBluRay"
                    let selectedimageName = "VideoBluRay"
                    let driver = rs.stringForColumnIndex(3) ?? ""
                    
                    //1 //0
                    /*
                     infoData.displayName = "Blu-ray"
                     //            infoData.imageName = "VideoBluRay"
                     */
                    
                    infoData.unitId = unitID
                    infoData.moduleId = moduleID
                    infoData.displayName = displayName
                    infoData.showVolumeBar = rs.stringForColumnIndex(3)!.toBool()
                    infoData.imageName = imageName
                    infoData.deviceCategoryName = deviceCategoryName
                    infoData.param2 = selectedimageName
                    infoData.driverName = driver
                    infoData.deviceBrandName = deviceCategoryName
                    advanceControlArry.append(infoData)
                }
            }
            
            return advanceControlArry
        }
        
    }
    public func getMediaForVideoScreen(_ mediatype : Int = 1) -> [AdSettingsInfoData]
    {
        
        // mediatype =  2 -> video 1 -> audio
        
        var advanceControlArry = [AdSettingsInfoData]()
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return advanceControlArry
        } else {
            var roomIds = roomManager.currentRoomID
            
            if roomManager.currentRoomSubroomInfo != nil {
                for dictionary in roomManager.currentRoomSubroomInfo  {
                    roomIds += ("," + dictionary["SubRoomId"]!)
                }
            }
            //entertainment.av_category -> 1 & 2  / 2= video 1= audio
            let sqlQuery = "SELECT entertainment.entertainment_id, entertainment.display_name, entertainment.room_controller_id, entertainment.is_volumebar, CD_icon.default_icon, CD_icon.selected_icon, CD_digitalmedia.is_controllable,CD_digitalmedia.driver FROM entertainment, CD_digitalmedia, CD_icon where CD_digitalmedia.driver_file_name = entertainment.driver_name and CD_digitalmedia.icon_id = CD_icon.icon_id and entertainment.device_category = \"digitalmedia\" and entertainment.av_category = \(mediatype) and room_id in (\(roomIds))"
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoData = AdSettingsInfoData()
                    let unitID = rs.stringForColumnIndex(0)!
                    let displayName = rs.stringForColumnIndex(1) ?? ""
                    let moduleID = rs.stringForColumnIndex(2) ?? ""
                    let imageName = rs.stringForColumnIndex(4) ?? ""
                    let selectedimageName = rs.stringForColumnIndex(5) ?? ""
                    let controllable = rs.stringForColumnIndex(6) ?? ""
                    let driver = rs.stringForColumnIndex(7) ?? ""
                    
                    //1 //0
                    infoData.unitId = unitID
                    infoData.moduleId = moduleID
                    infoData.displayName = displayName
                    infoData.showVolumeBar = rs.stringForColumnIndex(3)!.toBool()
                    infoData.param1 = imageName
                    infoData.param2 = selectedimageName
                    infoData.param3 = controllable
                    infoData.driverName = driver
                    infoData.deviceCategoryName = "digitalmedia"
                    advanceControlArry.append(infoData)
                }
            }
            
            return advanceControlArry
        }
        
    }
    /*
     This public function will return the brand name of module which will alwasy unique.
     Input : Driver name of the perticular module.
     Table Used: CD_digitalcinema
     */
    public func getDigitalCinemaMediaTypesFor(driver_name: String) -> String {
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return ""
        } else {
            var roomIds = roomManager.currentRoomID
            
            if roomManager.currentRoomSubroomInfo != nil {
                for dictionary in roomManager.currentRoomSubroomInfo  {
                    roomIds += ("," + dictionary["SubRoomId"]!)
                }
            }
            
            let sqlQuery = "SELECT brand FROM CD_digitalcinema where driver_file_name = \"\(driver_name)\""
            
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    
                    let brandName = rs.stringForColumnIndex(0)?.lowercased() ?? ""
                    
                    //1 //0
                    return brandName
                }
            }
            
            return ""
        }
        
    }
    public func getDigitalCinemaWithoutRokuMediaTypes() -> [AdSettingsInfoData] {
        var advanceControlArry = [AdSettingsInfoData]()
        var roomIds = roomManager.currentRoomID
        
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return advanceControlArry
        } else {
            var roomIds = roomManager.currentRoomID
            
            if roomManager.currentRoomSubroomInfo != nil {
                for dictionary in roomManager.currentRoomSubroomInfo  {
                    roomIds += ("," + dictionary["SubRoomId"]!)
                }
            }
            
            let sqlQuery = "SELECT entertainment.entertainment_id, entertainment.display_name, entertainment.room_controller_id, entertainment.is_volumebar, CD_icon.default_icon, CD_icon.selected_icon , CD_digitalcinema.driver, entertainment.device_category, entertainment.driver_name,CD_digitalcinema.brand FROM entertainment, CD_digitalcinema, CD_icon where CD_digitalcinema.driver_file_name = entertainment.driver_name and entertainment.device_category = \"digitalcinema\" and CD_digitalcinema.icon_id = CD_icon.icon_id  and room_id in (\(roomIds)) and CD_digitalcinema.brand NOT LIKE \"roku\""
            
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoData = AdSettingsInfoData()
                    let unitID = rs.stringForColumnIndex(0)!
                    let displayName = rs.stringForColumnIndex(1) ?? ""
                    let moduleID = rs.stringForColumnIndex(2) ?? ""
                    let imageName = rs.stringForColumnIndex(4) ?? ""
                    let selectedimageName = rs.stringForColumnIndex(5) ?? ""
                    let driver = rs.stringForColumnIndex(6) ?? ""
                    let deviceCategoryName = rs.stringForColumnIndex(7) ?? ""
                    
                    let deviceDriverFileName = rs.stringForColumnIndex(8) ?? ""
                    let deviceBrandName = rs.stringForColumnIndex(9) ?? ""
                    //1 //0
                    infoData.unitId = unitID
                    infoData.moduleId = moduleID
                    infoData.displayName = displayName
                    infoData.showVolumeBar = rs.stringForColumnIndex(3)!.toBool()
                    infoData.param1 = imageName
                    infoData.param2 = selectedimageName
                    infoData.driverName = driver
                    infoData.deviceCategoryName = deviceCategoryName
                    infoData.driverFileName = deviceDriverFileName
                    infoData.deviceBrandName = deviceBrandName.lowercased()
                    advanceControlArry.append(infoData)
                }
            }
            
            return advanceControlArry
        }
        
    }
    
    public func getDigitalCinemaMediaTypes() -> [AdSettingsInfoData] {
        var advanceControlArry = [AdSettingsInfoData]()
        var roomIds = roomManager.currentRoomID
        
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return advanceControlArry
        } else {
            var roomIds = roomManager.currentRoomID
            
            if roomManager.currentRoomSubroomInfo != nil {
                for dictionary in roomManager.currentRoomSubroomInfo  {
                    roomIds += ("," + dictionary["SubRoomId"]!)
                }
            }
            
            let sqlQuery = "SELECT entertainment.entertainment_id, entertainment.display_name, entertainment.room_controller_id, entertainment.is_volumebar, CD_icon.default_icon, CD_icon.selected_icon , CD_digitalcinema.driver, entertainment.device_category, entertainment.driver_name,CD_digitalcinema.brand FROM entertainment, CD_digitalcinema, CD_icon where CD_digitalcinema.driver_file_name = entertainment.driver_name and entertainment.device_category = \"digitalcinema\" and CD_digitalcinema.icon_id = CD_icon.icon_id  and room_id in (\(roomIds))"
            
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoData = AdSettingsInfoData()
                    let unitID = rs.stringForColumnIndex(0)!
                    let displayName = rs.stringForColumnIndex(1) ?? ""
                    let moduleID = rs.stringForColumnIndex(2) ?? ""
                    let imageName = rs.stringForColumnIndex(4) ?? ""
                    let selectedimageName = rs.stringForColumnIndex(5) ?? ""
                    let driver = rs.stringForColumnIndex(6) ?? ""
                    let deviceCategoryName = rs.stringForColumnIndex(7) ?? ""
                    
                    let deviceDriverFileName = rs.stringForColumnIndex(8) ?? ""
                    let deviceBrandName = rs.stringForColumnIndex(9) ?? ""
                    //1 //0
                    infoData.unitId = unitID
                    infoData.moduleId = moduleID
                    infoData.displayName = displayName
                    infoData.showVolumeBar = rs.stringForColumnIndex(3)!.toBool()
                    infoData.param1 = imageName
                    infoData.param2 = selectedimageName
                    infoData.driverName = driver
                    infoData.deviceCategoryName = deviceCategoryName
                    infoData.driverFileName = deviceDriverFileName
                    infoData.deviceBrandName = deviceBrandName.lowercased()
                    advanceControlArry.append(infoData)
                }
            }
            
            return advanceControlArry
        }
        
    }
    
    public func getRoomAmbianceCount() -> Int {
        var count = 0
        let sqlQuery = "SELECT count(*) as count  FROM user_device_configuration_mc INNER JOIN itouch_image ON itouch_image.image_id = user_device_configuration_mc.image_id WHERE user_device_configuration_mc.driver_type LIKE 'room_ambiance'  and itouch_image.category = 'mood'"
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return count
        } else {
            if let rs = database.query(sqlQuery){
                while rs.next() {
                    count = Int(rs.stringForColumnIndex(0)!)!
                }
            }
        }
        
        return count
    }
    
    public func getRoomAmbiances(_ roomId: String) -> [AmbienceInfoData] {
        
        var ambienceArry = [AmbienceInfoData]()
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return ambienceArry
        } else {
            let sqlQuery = "SELECT ambience.display_name,ambience.controller_id,ambience.id, CD_icon.default_icon ,CD_icon.selected_icon ,ambience.device_category FROM ambience , CD_icon WHERE ambience.room_id IN (\(roomId))  AND ambience.icon_id = CD_icon.icon_id ORDER BY ambience.position"
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = AmbienceInfoData()
                    infoObj.sceneId             = rs.stringForColumnIndex(2) ?? "-1"
                    infoObj.moduleId            = rs.stringForColumnIndex(1) ?? ""
                    infoObj.displayName         = rs.stringForColumnIndex(0) ?? ""
                    infoObj.defaultImageName    = rs.stringForColumnIndex(3) ?? ""
                    infoObj.selectedImageName   = rs.stringForColumnIndex(4) ?? ""
                    infoObj.type                = rs.stringForColumnIndex(5) ?? ""
                    ambienceArry.append(infoObj)
                }
            }
            
            return ambienceArry
        }
        
        
    }
    
    public func getAllAmbienceInfo() -> [AmbienceInfoData] {
        
        var ambienceArry = [AmbienceInfoData]()
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return ambienceArry
        } else {
            let sqlQuery = "SELECT ambience.display_name,ambience.id, CD_icon.default_icon ,CD_icon.selected_icon, ambience.controller_id,ambience.device_category FROM ambience , CD_icon WHERE ambience.room_id = 0  AND ambience.icon_id = CD_icon.icon_id ORDER BY ambience.position"
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = AmbienceInfoData()
                    infoObj.sceneId = rs.stringForColumnIndex(1) ?? "-1"
                    infoObj.displayName = rs.stringForColumnIndex(0) ?? ""
                    infoObj.defaultImageName = rs.stringForColumnIndex(2) ?? ""
                    infoObj.selectedImageName = rs.stringForColumnIndex(3) ?? ""
                    infoObj.moduleIdString = rs.stringForColumnIndex(4) ?? ""
                    infoObj.type = rs.stringForColumnIndex(5) ?? ""
                    ambienceArry.append(infoObj)
                }
            }
            
            return ambienceArry
        }
        
    }
    
    public func isTypeEnabledForRoomId(_ roomId: String, typeName: String) -> Bool {
        
        let query = "select entertainment_id from entertainment where device_category like '\(typeName)' and room_id = \(roomId)"
        var isPresent = false
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return isPresent
        }
        
        if let rs = database.query(query) {
            while rs.next() {
                isPresent = true
            }
        }
        
        return isPresent
        
    }
    
    
    //MARK: Phots Search
    
    public func getPhotoTags(_ localArr: inout SearchTagsData , userId: String = "none",scope:String = "3") {
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            //FIXME: POSSIBLY MISSING IMPLEMENTATION.
        } else {
            var sqlQuery = "SELECT upper(pd.people),upper(pd.location),upper(pd.keywords),upper(pd.dates) FROM pictures_detail pd JOIN pictures p ON p.picture_id = pd.picture_id WHERE (p.is_private = 3 OR p.user_id = 1) ORDER BY creation_date desc"
            
            if userId != "none" {
                sqlQuery = "SELECT upper(pd.people),upper(pd.location),upper(pd.keywords),upper(pd.dates) FROM pictures_detail pd JOIN  pictures p  ON p.picture_id = pd.picture_id WHERE p.user_id = \(userId) OR p.is_private IN (1,3) ORDER BY creation_date desc"
                
                if(scope == "2")
                {
                    sqlQuery = "SELECT upper(pd.people),upper(pd.location),upper(pd.keywords),upper(pd.dates) FROM pictures_detail pd JOIN pictures p ON p.picture_id = pd.picture_id WHERE p.user_id = \(userId) AND p.is_private = (2) ORDER BY creation_date desc"
                }
            }
            
            //print("query \(sqlQuery)")
            var peopleArr = [String]()
            var locationArr = [String]()
            var tagsArr = [String]()
            var dateArr = [String]()
            let database = SQDatabase.init(path: self.entertainmentDatabasePath)
            
            if !database.open() {
                
            } else {
                if let rs = database.query(sqlQuery) {
                    while rs.next() {
                        let people = rs.stringForColumnIndex(0) ?? ""
                        let location = rs.stringForColumnIndex(1) ?? ""
                        let tags = rs.stringForColumnIndex(2) ?? ""
                        let date = rs.stringForColumnIndex(3) ?? ""
                        peopleArr += (people.components(separatedBy: ","))
                        locationArr += (location.components(separatedBy: ","))
                        tagsArr += (tags.components(separatedBy: ","))
                        dateArr += (date.components(separatedBy: ","))
                    }
                }
            }
            
            peopleArr = Array(Set(peopleArr))
            locationArr = Array(Set(locationArr))
            tagsArr = Array(Set(tagsArr))
            dateArr = Array(Set(dateArr))
            localArr.people = peopleArr
            localArr.location = locationArr
            localArr.keyword = tagsArr
            localArr.date = dateArr
        }
    }
    
    public func getPhotosForSearchedParameters(_ searchPrm: [String] = [String](), userId: String = "1", scope: String = "3", originalPath: String, linkPath: String) -> [PhotoInfoData]? {
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return nil
        } else {
            var array = [PhotoInfoData]()
            
            var sqlQuery = "SELECT filename, user_id, is_private, picture_id FROM pictures WHERE user_id = 1 OR is_private!=2 ORDER BY creation_date desc, picture_id desc"
            
            if searchPrm.count > 0 {
                sqlQuery = "SELECT filename, user_id, is_private, picture_id FROM pictures as p  join picture_search_keyword as pd ON p.picture_id = pd.docid WHERE  picture_search_keyword MATCH '\(searchPrm.joined(separator: ",").replacingOccurrences(of: "'", with: "''"))' AND (p.user_id =1 OR p.is_private!=2) ORDER BY creation_date desc, picture_id desc"
                
                if userId != "1" {
                    sqlQuery = "SELECT filename, user_id, is_private, picture_id FROM pictures as p  join picture_search_keyword as pd ON p.picture_id = pd.docid WHERE  picture_search_keyword MATCH '\(searchPrm.joined(separator: ",").replacingOccurrences(of: "'", with: "''"))' AND (p.user_id = \(userId) OR p.is_private!=2) ORDER BY creation_date desc, picture_id desc"
                    
                    if scope == "2"
                    {
                        sqlQuery = "SELECT filename, user_id, is_private, picture_id FROM pictures as p  join picture_search_keyword as pd ON p.picture_id = pd.docid WHERE  picture_search_keyword MATCH '\(searchPrm.joined(separator: ",").replacingOccurrences(of: "'", with: "''"))' AND (p.user_id = \(userId) AND p.is_private = 2) ORDER BY creation_date desc, picture_id desc"
                    }
                }
            }
            else {
                if userId != "1" {
                    sqlQuery = "SELECT filename, user_id, is_private, picture_id FROM pictures WHERE user_id = \(userId) OR is_private!=2 ORDER BY creation_date desc, picture_id desc"
                    if scope == "2" {
                        sqlQuery = "SELECT filename, user_id, is_private, picture_id FROM pictures WHERE user_id = \(userId) AND is_private = 2 ORDER BY creation_date desc, picture_id desc"
                    }
                }
            }
            
            print("query -- \(sqlQuery)")
            if let rs = database.query(sqlQuery) {
                
                while rs.next() {
                    
                    let photoInfo = PhotoInfoData()
                    let halfpath = rs.stringForColumnIndex(0) ?? ""
                    photoInfo.photoImageName = URL(string: urlEncodeString(halfpath))?.lastPathComponent
                    photoInfo.photoLinkPath = linkPath.appending(halfpath)
                    photoInfo.photoOriginalPath = originalPath.appending(halfpath)
                    photoInfo.user_id = rs.stringForColumnIndex(1) ?? ""
                    photoInfo.privacy = rs.stringForColumnIndex(2) ?? ""
                    photoInfo.photo_id = rs.stringForColumnIndex(3) ?? ""
                    
                    array.append(photoInfo)
                }
            }
            print("arr \(array.count)")
            return array
            
        }
        
    }
    
    public func getFilteredPhotoTags(_ searchPrm: [String] = [String](), userId: String = "none" , scope:String = "3") -> SearchTagsData
    {
        
        let columns = "upper(sk.people),upper(sk.location),upper(sk.keywords),upper(sk.dates)"
        var sqlQuery = "SELECT \(columns) FROM picture_search_keyword sk JOIN  pictures pv  ON pv.picture_id = sk.docid WHERE (pv.is_private = 3 OR pv.user_id = 1) ORDER BY creation_date desc"
        
        if searchPrm.count > 0 {
            sqlQuery = "SELECT \(columns) FROM picture_search_keyword sk JOIN  pictures pv  ON pv.picture_id = sk.docid WHERE sk.picture_search_keyword MATCH '\(searchPrm.joined(separator: ",").replacingOccurrences(of: "'", with: "''") )' and (pv.is_private = 3 OR pv.user_id = 1) ORDER BY creation_date desc"
        }
        
        if userId != "" && userId != "none" {
            sqlQuery = "SELECT \(columns) FROM picture_search_keyword sk JOIN  pictures pv  ON pv.picture_id = sk.docid WHERE (pv.user_id IN (1,\(userId)) OR pv.is_private = 3 ) ORDER BY creation_date desc"
            
            if(scope == "2")
            {
                sqlQuery = "SELECT \(columns) FROM picture_search_keyword sk JOIN  pictures pv  ON pv.picture_id = sk.docid WHERE (pv.user_id IN (\(userId)) AND pv.is_private = 2 ) ORDER BY creation_date desc"
            }
            if searchPrm.count > 0 {
                sqlQuery = "SELECT \(columns) FROM picture_search_keyword sk JOIN  pictures pv  ON pv.picture_id = sk.docid WHERE sk.picture_search_keyword MATCH '\(searchPrm.joined(separator: ",").replacingOccurrences(of: "'", with: "''"))' and (pv.user_id IN (1,\(userId)) OR pv.is_private = 3 ) ORDER BY creation_date desc"
                if(scope == "2")
                {
                    sqlQuery = "SELECT \(columns) FROM picture_search_keyword sk JOIN  pictures pv  ON pv.picture_id = sk.docid WHERE sk.picture_search_keyword MATCH '\(searchPrm.joined(separator: ",").replacingOccurrences(of: "'", with: "''"))' and (pv.user_id IN (\(userId)) AND pv.is_private = 2 ) ORDER BY creation_date desc"
                    
                }
            }
        }
        
        //        print("query \(sqlQuery)")
        
        let collectionKeywords = SearchTagsData()
        var peopleArr = [String]()
        var locationArr = [String]()
        var tagsArr = [String]()
        var dateArr = [String]()
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        if !database.open() {
            return collectionKeywords
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let people = rs.stringForColumnIndex(0) ?? ""
                    let location = rs.stringForColumnIndex(1) ?? ""
                    let tags = rs.stringForColumnIndex(2) ?? ""
                    let dates = rs.stringForColumnIndex(3) ?? ""
                    
                    if (people.count) > 0 {
                        peopleArr += (people.components(separatedBy: ","))
                    }
                    
                    if (location.count) > 0 {
                        locationArr += (location.components(separatedBy: ","))
                    }
                    
                    if (tags.count) > 0 {
                        tagsArr += (tags.components(separatedBy: ","))
                    }
                    
                    if (dates.count) > 0 {
                        dateArr += (dates.components(separatedBy: ","))
                    }
                }
            }
        }
        
        peopleArr = Array(Set(peopleArr))
        locationArr = Array(Set(locationArr))
        tagsArr = Array(Set(tagsArr))
        dateArr = Array(Set(dateArr))
        tagsArr = tagsArr.filter {
            !searchPrm.contains($0)
        }
        locationArr = locationArr.filter {
            !searchPrm.contains($0)
        }
        peopleArr = peopleArr.filter {
            !searchPrm.contains($0)
        }
        dateArr = dateArr.filter {
            !searchPrm.contains($0)
        }
        collectionKeywords.people = peopleArr
        collectionKeywords.location = locationArr
        collectionKeywords.keyword = tagsArr
        collectionKeywords.date = dateArr
        
        return collectionKeywords
    }
    
    public func getPhotoDetailsById(_ photoid: String) -> PVInfoData {
        
        let photoInfo = PVInfoData()
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return photoInfo
        } else {
            defer{
                database.close()
            }
            
            let sqlQuery = "SELECT pd.picture_detail_id,pd.people,pd.location,pd.keywords,pd.dates,p.is_private,p.user_id FROM pictures_detail as pd left join pictures as p ON pd.picture_id = p.picture_id WHERE pd.picture_id = \(photoid)"
            
            print("sqlquery \(sqlQuery)")
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    photoInfo.id = rs.stringForColumnIndex(0) ?? ""
                    var people = rs.stringForColumnIndex(1) ?? ""
                    people = people.uppercased()
                    var location = rs.stringForColumnIndex(2) ?? ""
                    location = location.uppercased()
                    var tags = rs.stringForColumnIndex(3) ?? ""
                    tags = tags.uppercased()
                    let dates = rs.stringForColumnIndex(4) ?? ""
                    let is_private = rs.stringForColumnIndex(5) ?? ""
                    let owner_id = rs.stringForColumnIndex(6) ?? ""
                    let dateArr = dates.components(separatedBy: ",")
                    
                    if dateArr.count == 1 {
                        photoInfo.date.append((dateArr[0]))
                    } else if dateArr.count == 2 {
                        photoInfo.date += dateArr
                    } else if dateArr.count >= 3 {
                        var filtered = dateArr.filter {
                            $0.components(separatedBy: " ").count == 3
                        }
                        //filtered = filtered[0].components(separatedBy: " ")
                        if filtered.count > 0
                        {
                            filtered = filtered[0].components(separatedBy: " ")
                            photoInfo.date += filtered
                        }
                        
                    }
                    
                    if people != "" {
                        photoInfo.people = people.components(separatedBy: ",")
                        
                    }
                    if location != "" {
                        photoInfo.location = location.components(separatedBy: ",")
                    }
                    if tags != "" {
                        photoInfo.keywords = tags.components(separatedBy: ",")
                    }
                    
                    photoInfo.is_private = is_private
                    photoInfo.owner_id = owner_id
                }
            }
        }
        
        return photoInfo
    }
    
    public func getPhotoSuggestion() -> [[String]] {
        var locationTagArray = [String]()
        var keywordTagArray = [String]()
        var peopleTagArray = [String]()
        var tagArray = [[String]]()
        let sqlQuery = "SELECT LOWER(picture_subcat_name),picture_parent_cat FROM pictures_subcategory"
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return tagArray
        } else {
            defer{
                database.close()
            }
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    if rs.stringForColumnIndex(1) == "2" {
                        locationTagArray.append(rs.stringForColumnIndex(0)!.uppercased())
                    } else if rs.stringForColumnIndex(1) == "3" {
                        keywordTagArray.append(rs.stringForColumnIndex(0)!.uppercased())
                    } else if rs.stringForColumnIndex(1) == "1" {
                        peopleTagArray.append(rs.stringForColumnIndex(0)!.uppercased())
                    }
                }
            }
            
            tagArray = [locationTagArray,keywordTagArray,peopleTagArray]
        }
        return tagArray
    }
    
    public func getAllPhotosNames() -> [FileInfoData] {
        var fileinfoArr = [FileInfoData]()
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return fileinfoArr
        } else {
            defer{
                database.close()
            }
            
            let sqlQuery = "SELECT filename FROM pictures"
            var halfSourcePath = ""
            if isDemoMode{
                halfSourcePath = demoContentHostUrl + "/" + kDirectoryPhotos
            }else{
                halfSourcePath = "http://" + databaseInterface.homeServerAddress  + "/" + kDirectoryPhotos
            }
            let localStoragePath = applicationBundleAppContentDirectoryURL.appendingPathComponent(kDirectoryPhotos)
            
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    
                    let filename = rs.stringForColumnIndex(0) ?? ""
                    //small
                    let fileInfo1 = FileInfoData()
                    fileInfo1.sourcePath = halfSourcePath + "/small/" + filename
                    fileInfo1.destinationPath = localStoragePath.appendingPathComponent("small/\(filename)").path
                    
                    fileinfoArr.append(fileInfo1)
                    
                    //medium
                    let fileInfo2 = FileInfoData()
                    fileInfo2.sourcePath = halfSourcePath + "/medium/" + filename
                    fileInfo2.destinationPath = localStoragePath.appendingPathComponent("medium/\(filename)").path
                    fileinfoArr.append(fileInfo2)
                    
                    
                    //large
                    let fileInfo3 = FileInfoData()
                    fileInfo3.sourcePath = halfSourcePath + "/ipad/" + filename
                    fileInfo3.destinationPath = localStoragePath.appendingPathComponent("ipad/\(filename)").path
                    fileinfoArr.append(fileInfo3)
                    
                }
            }
        }
        
        return fileinfoArr
    }
    
    public func setPrivacyLevelForPhotoId(_ photoId: String, privacyLevel: String, ownerId: String) -> Bool {
        var isSuccess = false
        let sqlQuery = "UPDATE pictures set is_private = \(privacyLevel), user_id = \(ownerId) WHERE picture_id = '\(photoId)'"
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open(){
            return isSuccess
        } else {
            defer{
                database.close()
            }
            
            if database.update(sqlQuery) {
                isSuccess = true
            }
            
            return isSuccess
        }
    }
    
    public func updatePhotoMetaInfo(_ photoId: String,peopleTag: String,locationTag: String,keywordTag: String , dateOfShoot: String)-> Bool {
        var isSuccess = false
        let sqlQuery = "UPDATE picture_search_keyword set people = '\(peopleTag)',location = '\(locationTag)',keywords = '\(keywordTag)',dates = '\(dateOfShoot)' WHERE rowid = '\(photoId)'"
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open(){
            return isSuccess
        } else {
            defer{
                database.close()
            }
            
            if database.update(sqlQuery) {
                
                let query2 = "UPDATE pictures_detail set people = '\(peopleTag)',location = '\(locationTag)',keywords = '\(keywordTag)',dates = '\(dateOfShoot)' WHERE picture_id = '\(photoId)'"
                
                if database.update(query2) {
                    isSuccess = true
                }
            }
            
            return isSuccess
        }
    }
    
    // This will fetch the details of feature updates
    public func getFeatureVersionInfo() -> [FeatureVersionInfo] {
        var versionArray = [FeatureVersionInfo]()
        let query = "SELECT packages, version, old_version FROM package_version"
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if database.open() {
            if let rs = database.query(query) {
                while rs.next() {
                    let infoObj = FeatureVersionInfo()
                    infoObj.packages = rs.stringForColumnIndex(0) ?? ""
                    infoObj.version = rs.stringForColumnIndex(1) ?? ""
                    infoObj.old_version = rs.stringForColumnIndex(2) ?? ""
                    versionArray.append(infoObj)
                }
            }else{
                return versionArray
            }
        }
        return versionArray
    }
    
    
    //MARK:-
    
    //MARK:- User Management
    public func deleteuserInfo(user_id : String)
    {
        
        let sqlQuery = "DELETE FROM user WHERE user_id = '\(user_id)' "
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            //            return isSuccess
        } else {
            defer{
                database.close()
            }
            
            if database.update(sqlQuery) {
                //            isSuccess = true
            }
        }
        
        //      return isSuccess
    }
    
    public var loggedInUserInfo: UserInfoData? {
        set {
            if newValue != nil {
                let archievedValue = NSKeyedArchiver.archivedData(withRootObject: newValue!)
                UserDefaults.standard.set(archievedValue, forKey: "LoggedInUserInfo")
            } else {
                UserDefaults.standard.set(nil, forKey: "LoggedInUserInfo")
            }
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: kLoggedInUserInfoChanged), object: nil)
        }
        
        get {
            if let archievedValue = UserDefaults.standard.object(forKey: "LoggedInUserInfo") as? Data,
                let userInfo = NSKeyedUnarchiver.unarchiveObject(with: archievedValue) as? UserInfoData {
                return userInfo
            }
            
            return nil
        }
    }
    
    public func updateUserDisplayNameLocally(userId : String , displayName: String)-> Bool {
        let sqlQuery = "UPDATE user SET display_name = '\(displayName)' WHERE user_id = \(userId) "
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        var isSuccess = false
        if !database.open() {
            return isSuccess
        } else {
            if database.update(sqlQuery) {
                
                isSuccess = true
            }
            
            return isSuccess
            
        }
    }
    
    public func updateUserImage(userId : String , imageName: String)-> Bool {
        let sqlQuery = "UPDATE user SET image = '\(imageName)' WHERE user_id = \(userId) "
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        var isSuccess = false
        if !database.open() {
            return isSuccess
        } else {
            if database.update(sqlQuery) {
                
                isSuccess = true
            }
            
            return isSuccess
            
        }
    }
    public func updateUserPasswordLocally(userId : String , newPassword: String)-> Bool {
        
        let sqlQuery = "UPDATE user SET pwd = '\(newPassword)' WHERE user_id = \(userId) "
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        var isSuccess = false
        if !database.open() {
            return isSuccess
        } else {
            if database.update(sqlQuery) {
                databaseInterface.loggedInUserInfo?.userPassword = newPassword
                isSuccess = true
            }
            
            return isSuccess
            
        }
        
        
    }
    public func getUsersInfoById(userId : String) -> UserInfoData! {
        let userInfoArry = UserInfoData()
        let sqlQuery = "SELECT * FROM user WHERE user_id = \(userId)"
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return userInfoArry
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    
                    userInfoArry.userId = rs.stringForColumnIndex(0) ?? "-1"
                    userInfoArry.userName = rs.stringForColumnIndex(1) ?? ""
                    userInfoArry.userPassword = rs.stringForColumnIndex(2) ?? ""
                    userInfoArry.userIsAdmin = Int(rs.int64ForColumnIndex(3)!).toBool()
                    userInfoArry.userIconName = rs.stringForColumnIndex(4) ?? ""
                    userInfoArry.userDisplayName = rs.stringForColumnIndex(5) ?? ""
                    
                }
            }
        }
        
        return userInfoArry
    }
    public func getUsersInfo() -> [UserInfoData]! {
        var userInfoArry = [UserInfoData]()
        let sqlQuery = "SELECT * FROM user WHERE user_id != 1 ORDER BY name"
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return userInfoArry
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let userInfo = UserInfoData()
                    userInfo.userId = rs.stringForColumnIndex(0) ?? "-1"
                    userInfo.userName = rs.stringForColumnIndex(1) ?? ""
                    userInfo.userPassword = rs.stringForColumnIndex(2) ?? ""
                    userInfo.userIsAdmin = Int(rs.int64ForColumnIndex(3)!).toBool()
                    userInfo.userIconName = rs.stringForColumnIndex(4) ?? ""
                    userInfo.userDisplayName = rs.stringForColumnIndex(5) ?? ""
                    userInfoArry.append(userInfo)
                }
            }
        }
        
        return userInfoArry
    }
    
    public func getSIPServerDetails() -> [String:String]? {
        var infoDic: [String:String]?
        
        let path = applicationBundleAppContentDirectoryURL.appendingPathComponent("roomInfo").path as String
        if path != ""
        {
            let archive = FileManager.default.contents(atPath: path)
            
            if archive != nil
            {
                
                let roomInfoDictionary      = NSKeyedUnarchiver.unarchiveObject(with: archive!)! as! Dictionary<String, Any>
                if(roomInfoDictionary["sip_configuration"] != nil)
                {
                    let sip_configuration  = roomInfoDictionary["sip_configuration"] as! [String:Any]
                    if (sip_configuration.count > 0)
                    {
                        infoDic = ["UserName": String(describing: sip_configuration["sip_id"]!) ,
                                   "Password": String(describing: sip_configuration["password"]!),
                                   "Domain": String(describing: sip_configuration["ip"]!),
                                   "Network": String(describing: sip_configuration["network"]!)
                        ]
                        
                    }
                }
            }
        }
        
        
        
        return infoDic
    }
    
    public func getInfoForCallID(_ callerId: String) -> [String:String]? {
        
        var infoDic: [String:String]?
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open(){
            return infoDic
        } else {
            let sqlQuery = "SELECT door_phone.url, door_phone.dtmf, door_lock.doorlock_id, door_lock.display_name FROM door_phone, door_lock WHERE door_lock.extension_number = '\(callerId)' and door_lock.extension_number = door_phone.extension_number"
            print(sqlQuery)
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let camURL = rs.stringForColumnIndex(0) ?? ""
                    let unitId = rs.stringForColumnIndex(2) ?? "-1"
                    let moduleId = "1"
                    let displayName = rs.stringForColumnIndex(3) ?? ""
                    let doorDTMF = rs.stringForColumnIndex(1) ?? "-1"
                    infoDic = ["StreamURL": camURL,
                               "UnitId": unitId,
                               "ModuleId": moduleId,
                               "DisplayName": displayName,
                               "DTMFCode": doorDTMF
                    ]
                }
            }
            
            if infoDic == nil {
                let sqlQuery = "SELECT url FROM door_phone WHERE extension_number = '\(callerId)'"
                
                if let rs2 = database.query(sqlQuery) {
                    while rs2.next() {
                        let vidURL = rs2.stringForColumnIndex(0) ?? ""
                        infoDic = ["StreamURL" : vidURL]
                    }
                }
            }
            
            return infoDic
        }
        
    }
    
    public var homeServerAddress: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "HomeServerAddress")
            UserDefaults.standard.synchronize()
        }
        
        get {
            return UserDefaults.standard.string(forKey: "HomeServerAddress") ?? ""
        }
    }
    
    public var entertainmentServerAddress: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "EntertainmentServerAddress")
        }
        
        get {
            return UserDefaults.standard.string(forKey: "EntertainmentServerAddress") ?? ""
        }
    }
    
    public func getDownloadDetailsForType(_ type: String, array: inout [FileInfoData]) {
        var sqlQuery: String!
        var isAutomationData = true
        let folderName = "iPadContent"
        var serverPath = ""
        sqlQuery = ""
        var hostname = ""
        if isDemoMode{
            hostname = demoContentHostUrl
        }else{
            hostname = "http://" + homeServerAddress
        }
        
        switch type {
        case kDirectoryRoomsPartnerLogo:
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'partnerlogo' AND userdevice_type = 'ipad'"
            
            
        case kDirectoryRoomsIconsDefault:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/casaroomicon/default/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'roomicon' AND state = 'default' AND userdevice_type = 'ipad'"
            
            
        case kDirectoryRoomsIconsSelected:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/casaroomicon/selected/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'roomicon' AND state = 'selected' AND userdevice_type = 'ipad'"
            
            
            
        case kDirectoryTVChannelThumbs:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/tvchannels/"
            sqlQuery = "SELECT DISTINCT img.selected_image FROM favourites AS fav, itouch_image AS img WHERE fav.channel_image_id = img.image_id AND fav.unfavorites = 0"
            
            
            var fileName = [String]()
            let path = applicationBundleAppContentDirectoryURL.appendingPathComponent("roomInfo").path as String
            if path != "" {
                let archive = FileManager.default.contents(atPath: path)
                var permissions = [String:Any]()
                var roomLevel = [String:Any]()
                
                
                if archive != nil {
                    var roomInfoDictionary =  Dictionary<String, Any>()
                    var flooreInfoDictionary = [[String:Any]]()
                    roomInfoDictionary = NSKeyedUnarchiver.unarchiveObject(with: archive!)! as! Dictionary<String, Any>
                    permissions             = roomInfoDictionary["permission"] as! [String:Any]
                    if(permissions["roomlevel"] != nil)
                    {
                        roomLevel               = permissions["roomlevel"] as! [String:Any]
                        flooreInfoDictionary    = roomLevel["floors"] as! [[String:Any]]
                    }
                    
                    var room_id: String = ""
                    
                    for flooreInfo in flooreInfoDictionary
                    {
                        //print(flooreInfo)
                        //
                        var infoArray: [ChannelInfoData] = []
                        let rooms = flooreInfo["rooms"] as? [[String: Any]]
                        
                        
                        for room in rooms!
                        {
                            if room["is_subroom"] as! Int == 1
                            {
                                let subrooms = room["subrooms"] as! [[String:Any]]
                                for subroom in subrooms
                                {
                                    if(room_id != "")
                                    {
                                        room_id += "," + (subroom["room_id"] as! String)
                                    }
                                    else
                                    {
                                        room_id += (subroom["room_id"] as! String)
                                    }
                                }
                            }
                            else
                            {
                                if(room_id != "")
                                {
                                    room_id += "," + (room["room_id"] as! String)
                                }
                                else
                                {
                                    room_id += (room["room_id"] as! String)
                                }
                            }
                            
                        }
                        
                    }
                    //                    sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'tvchannel' AND userdevice_type = 'ipad'"
                    
                    let sqlQuery = "SELECT DISTINCT selected_icon FROM tvchannel WHERE room_id IN (\(room_id)) GROUP BY selected_icon"
                    
                    let database = SQDatabase.init(path: self.configurationDatabasePath)
                    
                    if !database.open() {
                        
                    } else {
                        if let rs = database.query(sqlQuery) {
                            while rs.next() {
                                fileName.append("(path LIKE \"%" + rs.stringForColumnIndex(0)! + "\" )")
                                
                            }
                        }
                    }
                }
            }
            
            let allfiles = fileName.joined(separator: " OR ")
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'tvchannel' AND userdevice_type = 'ipad' AND (\(allfiles))"
            
            
            
        case kDirectoryFMChannelThumbs:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/fm/default/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'fm' AND userdevice_type = 'ipad'"
            
            
        case kDirectoryLightsImages:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/lightspanoramic/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'lightpanoramic' AND userdevice_type = 'ipad'"
            
            
        case "ipadProLightImages":
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/iprolightspanoramic/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'lightpanoramic' AND userdevice_type = 'ipadpro'"
            
            
        case "colorLightSpectrum":
            /// this will only be in V2
            serverPath = ""
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'colorlight' AND userdevice_type = 'ipad'"
            
        case kDirectoryLightsIconPanoDefault:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/lights/panoramic/default/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'light' AND userdevice_type = 'ipad' AND state = 'default'  AND panoramic = 'panoramic'"
            
            
        case kDirectoryLightsIconPanoSelected:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/lights/panoramic/selected/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'light' AND userdevice_type = 'ipad' AND state = 'selected'  AND panoramic = 'panoramic'"
            
            
        case kDirectoryLightsIconNonPanoDefault:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/lights/nonpanoramic/default/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'light' AND userdevice_type = 'ipad' AND state = 'default'  AND panoramic = 'nonpanoramic'"
            
            
        case kDirectoryLightsIconNonPanoSelected:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/lights/nonpanoramic/selected/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'light' AND userdevice_type = 'ipad' AND state = 'selected'  AND panoramic = 'nonpanoramic'"
            
            
        case kDirectoryMoodsIconDefault:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/casamoods/default/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type IN ('mood','curtainmood') AND userdevice_type = 'ipad' AND state = 'default'"
            
            
        case kDirectoryMoodsIconSelected:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/casamoods/selected/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type IN ('mood','curtainmood') AND userdevice_type = 'ipad' AND state = 'selected'"
            
            
        case kDirectoryCurtainsImages:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/curtainspanoramic/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'curtainpanoramic' AND userdevice_type = 'ipad'"
            
            
        case "ipadProCurtainsImages":
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/iprocurtainspanoramic/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'curtainpanoramic' AND userdevice_type = 'ipadpro'"
            
            
            
        case kDirectoryUtilitiesIcons:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/casautility/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'utility' AND userdevice_type = 'ipad'"
            
            
        case kDirectorySurveillanceImages:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/surveillance/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'surveillance' AND userdevice_type = 'ipad'"
            
            
        case "Fire Alarm":
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/firealarm/default/"
            
            sqlQuery = "SELECT DISTINCT selected_image FROM itouch_image WHERE category LIKE 'firealarm'"
            
            
        case kDirectoryACModeImages:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/ac/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'acmode' AND userdevice_type = 'ipad'"
            
            
        case kDirectoryAmbienceIconDefault:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/casahome_moods/default/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'mood' AND userdevice_type = 'ipad' AND state = 'default'"
            
            
        case kDirectoryAmbienceIconSelected:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/casahome_moods/selected/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE entity_type = 'mood' AND userdevice_type = 'ipad' AND state = 'selected'"
            
            
        case kDirectoryUsersImagesLarge:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/userimages/"
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/userimages/"
            }
            
            sqlQuery = "SELECT DISTINCT image FROM user"
            
            
            
        case kDirectoryUsersImagesSmall:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/userimages/ipadimage/"
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/userimages/ipadimage/"
            }
            sqlQuery = "SELECT DISTINCT image FROM user"
            
            
        case kDirectoryMoviePosters:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/" + ((UIScreen.main.scale >= 2) ? "ipadimages/" : "poster/")
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/" + ((UIScreen.main.scale >= 2) ? "ipadimages/" : "poster/")
            }
            sqlQuery = "SELECT DISTINCT filepath FROM movie_information where filepath !=' ' and filepath !=''"
            
            
            
        case kDirectoryPersonalVideos:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/video_images/" + ((UIScreen.main.scale >= 2) ? "retina/" : "")
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/video_images/" + ((UIScreen.main.scale >= 2) ? "retina/" : "")
            }
            sqlQuery = "SELECT DISTINCT snapshot FROM tbl_personal_video WHERE snapshot!=''"
            
            
        case kDirectoryMusicSubcatThumbs:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/music/subcategory/"
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/music/subcategory/"
            }
            
            if UIScreen.main.scale >= 2 {
                serverPath += "retina/"
            }
            
            sqlQuery = "SELECT DISTINCT music_subcat_location FROM music_subcategory where music_subcat_location !=' ' and music_subcat_location !='' AND `music_subcat_id` IN(SELECT DISTINCT music_subcat_id FROM user_subcategory_music)"
            
        case kDirectoryMusicSongThumbs:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/music/songs/small/"
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/music/songs/small/"
            }
            if UIScreen.main.scale >= 2 {
                serverPath += "retina/"
            }else{
                serverPath += "nonretina/"
            }
            sqlQuery = "SELECT DISTINCT song_thumb FROM song_detail where song_thumb !=' ' and song_thumb !=''"
            
        case kDirectoryMusicSongLargeThumbs:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/music/songs/large/"
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/music/songs/large/"
            }
            if UIScreen.main.scale >= 2 {
                serverPath += "retina/"
            }else{
                serverPath += "nonretina/"
            }
            sqlQuery = "SELECT DISTINCT song_thumb FROM song_detail where song_thumb !=' ' and song_thumb !=''"
            
            
        case kDirectoryTVShowsEpisodesPosters:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/tv_shows/episode/"
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/tv_shows/episode/"
            }
            
            if UIScreen.main.scale >= 2 {
                serverPath += "retina/"
            }
            
            sqlQuery = "SELECT DISTINCT episode_image FROM tv_episode"
            
            
        case kDirectoryTVShowsSeriesPosters:
            isAutomationData = false
            if isDemoMode{
                serverPath = demoContentHostUrl + "/mymovie/images/tv_shows/series/"
            }else{
                serverPath = "http://" + entertainmentServerAddress + "/mymovie/images/tv_shows/series/"
            }
            
            if UIScreen.main.scale >= 2 {
                serverPath += "retina/"
            }
            
            sqlQuery = "SELECT DISTINCT poster_path FROM tv_series"
            
            
        case kDirectoryDigitalMediaInner:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/digitalmedia/inner/"
            
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE (entity_type = 'digitalmedia' OR entity_type = 'digitalcinema')  AND state = 'inner' AND userdevice_type = 'ipad'"
            
            
        case kDirectoryDigitalMediaOuter:
            serverPath = hostname + "/" + serverContentDirectoryName + "/" + folderName + "/digitalmedia/outer/"
            
            sqlQuery = "SELECT DISTINCT path FROM content WHERE (entity_type = 'digitalmedia' OR entity_type = 'digitalcinema')  AND state = 'outer' AND userdevice_type = 'ipad'"
            
            
        default:
            return
        }
        
        self.fetchQuery(sqlQuery, forType: type, withSourcePath: serverPath, array: &array, isAutomationData: isAutomationData)
    }
    
    private func fetchQuery(_ sqlQuery: String, forType type: String, withSourcePath serverPath: String, array: inout [FileInfoData], isAutomationData isAuto: Bool) {
        
        var localArray = [FileInfoData]()
        let localStoragePath = applicationBundleAppContentDirectoryURL.appendingPathComponent(type)
        if isBackgroundUpdateRunning == false {
            try? FileManager.default.removeItem(at: localStoragePath)
        }
        
        try? FileManager.default.createDirectory(atPath: localStoragePath.path, withIntermediateDirectories: true, attributes: nil)
        
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(isAuto ? self.configurationDatabase : self.entertainmentDatabase, sqlQuery, -1, &statement, nil) == SQLITE_OK {
            while sqlite3_step(statement) == SQLITE_ROW {
                let fileInfoObj = FileInfoData()
                let imageName = String(cString: sqlite3_column_text(statement, 0))
                fileInfoObj.sourcePath = serverPath + imageName
                fileInfoObj.destinationPath = localStoragePath.appendingPathComponent(imageName).path
                fileInfoObj.moduleType = type
                if type != kDirectoryUsersImagesSmall && type != kDirectoryUsersImagesLarge
                {
                    let splitedPath = imageName.components(separatedBy: "/")
                    if(splitedPath.count > 1)
                    {
                        if isDemoMode{
                            fileInfoObj.sourcePath = demoContentHostUrl + imageName
                        }else{
                            fileInfoObj.sourcePath = "http://" + self.homeServerAddress + imageName
                        }
                        fileInfoObj.destinationPath = localStoragePath.appendingPathComponent(splitedPath.last!).path
                    }
                }
                //                print("serverpath")
                //                print(fileInfoObj.sourcePath)
                //                print(fileInfoObj.destinationPath)
                
                localArray.append(fileInfoObj)
            }
        }
        
        sqlite3_finalize(statement)
        array.append(contentsOf: localArray)
    }
    
    /**
     Retrieves all Movie Posters need to be deleted from iPad library. This is needed because few movies would be deleted from server. So in iPad library they should also be deleted because they are no more needed now.
     - Parameters: timeStamp
     The last update time
     
     - returns: Poster set
     */
    public func getMoviePostersToDelete(_ timeStamp: String) -> Set<String> {
        var posterSet = Set<String>()
        let sqlQuery = "SELECT image_name FROM time_based_push_update where upload_type = 'movies' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 0"
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return posterSet
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let fileName = rs.stringForColumnIndex(0)
                    if fileName != "" {
                        posterSet.insert(fileName!)
                    }
                }
            }
        }
        
        return posterSet
    }
    
    /**
     Checks if 'time_based_push_update' table exists or not to manage backward compatibility. The 'time_based_push_update' is a new table added for incremental update
     
     - returns:   True if table exists and false if does not exists
     */
    public func queryDBToCheckTblTimebasePushupdate() -> Bool {
        var tableExists = false
        let sqlQuery = "SELECT name FROM sqlite_master WHERE type='table' AND name='time_based_push_update';"
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        if !database.open() {
            return tableExists
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    tableExists = true
                }
            }
        }
        return tableExists
    }
    
    /**
     Retrieves all Movie Posters need to be updated in incremental update.
     - Parameters: timeStamp
     The last update time
     
     - returns: Poster set
     */
    public func getMoviePosterSet(_ timeStamp: String) -> [Dictionary<String, String>] {
        var posterSet = [Dictionary<String, String>]()
        var sqlQuery = ""
        if UIScreen.main.scale >= 2 {
            // Fetch Retina Images
            sqlQuery = "SELECT retina_path, image_name FROM time_based_push_update where upload_type = 'movies' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 1"
        }
        else {
            //Non Retina Images
            sqlQuery = "SELECT non_retina_path, image_name FROM time_based_push_update where upload_type = 'movies' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 1"
        }
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return posterSet
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let path = rs.stringForColumnIndex(0) ?? ""
                    let fileName = rs.stringForColumnIndex(1) ?? ""
                    if fileName != "" {
                        let dic = ["path" : path, "fileName" : fileName]
                        posterSet.append(dic)
                    }
                }
            }
        }
        
        return posterSet
    }
    
    /**
     Retrieves all Music song Posters need to be deleted from iPad library. This is needed because few songs would be deleted from server. So in iPad library their posters should also be deleted because they are no more needed now.
     - Parameters: timeStamp
     The last update time
     
     - returns: Poster set
     */
    public func getMusicSongsPosterToDelete(_ timeStamp: String) -> Set<String> {
        var posterSet = Set<String>()
        let sqlQuery = "SELECT image_name FROM time_based_push_update where upload_for = 'music' AND upload_type = 'songs' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 0"
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return posterSet
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let fileName = rs.stringForColumnIndex(0)
                    if fileName != "" {
                        posterSet.insert(fileName!)
                    }
                }
            }
        }
        
        return posterSet
    }
    
    /**
     Retrieves all Music song Posters need to be updated in incremental update.
     - Parameters: timeStamp
     The last update time
     
     - returns: Poster set
     */
    public func getMusicSongsPosterSet(_ timeStamp: String) -> [Dictionary<String, String>] {
        var posterSet = [Dictionary<String, String>]()
        var sqlQuery = ""
        if UIScreen.main.scale >= 2 {
            // Fetch Retina Images
            sqlQuery = "SELECT DISTINCT retina_path, image_name, retina_small FROM time_based_push_update where upload_for = 'music' AND upload_type = 'songs' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 1"
        }
        else {
            //Non Retina Images
            sqlQuery = "SELECT DISTINCT non_retina_path, image_name, non_retina_small FROM time_based_push_update where upload_for = 'music' AND upload_type = 'songs' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 1"
        }
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return posterSet
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let path = rs.stringForColumnIndex(0) ?? ""
                    let fileName = rs.stringForColumnIndex(1) ?? ""
                    let small_path = rs.stringForColumnIndex(2) ?? ""
                    if fileName != "" {
                        let dic = ["path" : path, "fileName" : fileName, "small_path" : small_path]
                        posterSet.append(dic)
                    }
                }
            }
        }
        
        return posterSet
    }
    
    /**
     Retrieves all Music subcat Posters need to be deleted from iPad library. This is needed because few sub categories would be deleted from server. So in iPad library their posters should also be deleted because they are no more needed now.
     
     - Parameters: timeStamp
     The last update time
     
     - returns: Poster set
     */
    
    public func getMusicSubcatThumbsToDelete(_ timeStamp: String) -> Set<String> {
        var posterSet = Set<String>()
        let sqlQuery = "SELECT image_name FROM time_based_push_update where upload_for = 'music' AND upload_type = 'subcat' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 0"
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return posterSet
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let fileName = rs.stringForColumnIndex(0)
                    if fileName != "" {
                        posterSet.insert(fileName!)
                    }
                }
            }
        }
        
        return posterSet
    }
    
    public func getMusicSubcatThumbs(_ timeStamp: String) -> [Dictionary<String, String>] {
        var posterSet = [Dictionary<String, String>]()
        var sqlQuery = ""
        if UIScreen.main.scale >= 2 {
            // Fetch Retina Images
            sqlQuery = "SELECT retina_path, image_name FROM time_based_push_update where upload_for = 'music' AND upload_type = 'subcat' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 1"
        }
        else {
            //Non Retina Images
            sqlQuery = "SELECT non_retina_path, image_name FROM time_based_push_update where upload_for = 'music' AND upload_type = 'subcat' AND updated_time > REPLACE('\(timeStamp)','T',' ') AND operation = 1"
        }
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return posterSet
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let path = rs.stringForColumnIndex(0) ?? ""
                    let fileName = rs.stringForColumnIndex(1) ?? ""
                    if fileName != "" {
                        let dic = ["path" : path, "fileName" : fileName]
                        posterSet.append(dic)
                    }
                }
            }
        }
        
        return posterSet
    }
    
    
    
    
    
    public func getMaxTimeOfLastPushUpdate() -> String? {
        var timeStamp: String?
        let sqlQuery = "SELECT max(updated_time) FROM time_based_push_update"
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return timeStamp
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    if let maxtime = rs.stringForColumnIndex(0) {
                        timeStamp = maxtime
                    }
                }
            }
        }
        
        return timeStamp
    }
    
    public func getPostersSet(_ type: String) -> Set<String> {
        var postersSet = Set<String>()
        var sqlQuery: String!
        
        if type == kDirectoryMoviePosters {
            sqlQuery = "SELECT DISTINCT filepath FROM movie_information"
        }
        
        if type == kDirectoryPersonalVideos {
            sqlQuery = "SELECT DISTINCT snapshot FROM tbl_personal_video"
        }
        
        if type == kDirectoryTVShowsSeriesPosters {
            sqlQuery = "SELECT DISTINCT poster_path FROM tv_series"
        }
        if type == kDirectoryTVShowsEpisodesPosters {
            sqlQuery = "SELECT DISTINCT episode_image FROM tv_episode"
        }
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        if !database.open() {
            return postersSet
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let fileName = rs.stringForColumnIndex(0)
                    if fileName != "" {
                        postersSet.insert(fileName!)
                    }
                }
            }
        }
        
        return postersSet
    }
    
    public func getRoomImageSet(_ type: String) -> Set<String> {
        var postersSet = Set<String>()
        var SQLQuery: String!
        
        if type == kDirectoryRoomsIconsImages {
            SQLQuery = "SELECT DISTINCT upload_icon_name FROM room_info WHERE upload_icon_name != 'NA'"
        }
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return postersSet
        } else {
            if let rs = database.query(SQLQuery) {
                while rs.next() {
                    let fileName = rs.stringForColumnIndex(0)
                    if fileName != "" {
                        postersSet.insert(fileName!)
                    }
                }
            }
        }
        
        return postersSet
    }
    
    public func setRoomImage(_ imgName: String , roomId: Int) -> Set<String> {
        var postersSet = Set<String>()
        var sqlQuery: String!
        sqlQuery = "UPDATE room_info SET  upload_icon_name = '\(imgName)' WHERE room_id = \(roomId) "
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return postersSet
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let fileName = rs.stringForColumnIndex(0)
                    if fileName != "" {
                        postersSet.insert(fileName!)
                    }
                }
            }
        }
        
        return postersSet
    }
    
    public func getUserName(_ userId : Int) -> String{
        
        let sqlQuery = "SELECT name FROM user WHERE user_id = '\(userId)'"
        
        let database = SQDatabase.init(path: self.entertainmentDatabasePath)
        
        var userName = ""
        if !database.open() {
            return userName
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let fileName = rs.stringForColumnIndex(0)
                    if fileName != "" {
                        userName = fileName!
                    }
                }
                
                return userName
            }
            
            return userName
        }
    }
    
    public func musicOfflinePlaylistOperation(_ query: String) {
        let database = SQDatabase.init(path: self.offlineMusicPlaylistsDatabaseURL.path)
        
        if !database.open() {
            //FIXME: UNABLE TO OPEN DATABASE. HANDLE ERROR.
        } else {
            if database.update(query) {
                //FIXME: HANDLE SUCCESS OR FAILURE.
            }
        }
    }
    
    //web radio Search
    public func getRadioChannelsearchResult(strArr: [String]) -> [WebRadioStationInfo]{
        var searchStation = [WebRadioStationInfo]()
        var searchTag = ""
        
        for i in 0..<strArr.count {
            if i == 0 {// search
                searchTag = "name like '%\(strArr[0])%'"
            }
            else {
                searchTag = searchTag.appending("AND name like '%\(strArr[i])%'")
            }
        }
        let query = "SELECT id,name,description,logo,subgenre_id,mime_type_id,website,lang_id,country_id,is_favorite,type,pop_count,iso_lang_list,geo_blocked_countries from web_radio_searchdata where \(searchTag)"
        let database = SQDatabase.init(path: applicationBundleAppContentDirectoryURL.appendingPathComponent("OfflineWebRadioSearch.sqlite").path)
        
        if !database.open() {
            //FIXME: UNABLE TO OPEN DATABASE. HANDLE ERROR.
        } else {
            if let rs = database.query(query) {
                while rs.next() {
                    let stationInfo = WebRadioStationInfo()
                    stationInfo.stationId       = rs.intForColumnIndex(0) ?? -1
                    stationInfo.categoryType  = rs.stringForColumnIndex(10) ?? ""
                    stationInfo.stationNAme = rs.stringForColumnIndex(1) ?? ""
                    stationInfo.stationDescrition = rs.stringForColumnIndex(2) ?? ""
                    stationInfo.stattionLogoUrl = rs.stringForColumnIndex(3) ?? ""
                    stationInfo.type    = rs.stringForColumnIndex(7) ?? ""
                    stationInfo.stationWebsite    = rs.stringForColumnIndex(6) ?? ""
                    stationInfo.stationPlayUrl = rs.stringForColumnIndex(9) ?? ""
                    stationInfo.stationImageName =  stationInfo.stattionLogoUrl.components(separatedBy: "/").last!
                    searchStation.append(stationInfo)
                }
                return searchStation
            }
            return searchStation
        }
        return searchStation
    }
    
    
    //MARK: Music offline DB Methods
    /**
     Get All playlist Data form local DB MusicOfflinePlaylist.sqlite
     - Parameter parentId:   The string of folder or playlist parent id.(Root lavel parent id is 0 )
     - Returns: A new array of MusicPlaylistInfoData contain all the result playlist.
     */
    public func getAllPlaylistoffline(parentId : String) -> [DriveExplodedData]{
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: self.offlineMusicPlaylistNewDatabaseURL.path) {
            try? fileManager.copyItem(at: Bundle.main.url(forResource: "MusicOfflinePlaylist", withExtension: "sqlite")!, to: self.offlineMusicPlaylistNewDatabaseURL)
        }
        let sqlQuery = "SELECT * FROM playlist_info WHERE parent_id = '\(parentId)'"
        let database = SQDatabase.init(path: self.offlineMusicPlaylistNewDatabaseURL.path)
        print(self.offlineMusicPlaylistNewDatabaseURL.path)
        var infoArray = [DriveExplodedData]()
        if !database.open() {
            return infoArray
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let playlistinfo = DriveExplodedData()
                    let fileType = rs.stringForColumnIndex(8)
                    playlistinfo.itemId = rs.stringForColumnIndex(0) ?? ""
                    if fileType == "playlist"{
                        playlistinfo.type = fileType ?? ""
                        playlistinfo.fileActualName = rs.stringForColumnIndex(1) ?? ""
                        playlistinfo.file_full_Path = rs.stringForColumnIndex(2) ?? ""
                        playlistinfo.fileSongArr = [rs.stringForColumnIndex(3) ?? ""]
                        playlistinfo.fileName = rs.stringForColumnIndex(4) ?? ""
                        if let access = rs.stringForColumnIndex(5){
                            if access == "1"{
                                playlistinfo.isPrivate = false
                            }
                            else{
                                playlistinfo.isPrivate = true
                            }
                        }
                        playlistinfo.userID = rs.stringForColumnIndex(6) ?? ""
                        playlistinfo.parentid = rs.stringForColumnIndex(0) ?? ""
                    }else{
                        playlistinfo.parentid = rs.stringForColumnIndex(0) ?? ""
                        playlistinfo.type = fileType ?? ""
                        playlistinfo.folderName = rs.stringForColumnIndex(1) ?? ""
                    }
                    infoArray.append(playlistinfo)
                }
                return infoArray
            }
            return infoArray
        }
    }
    
    /**
     Get the Playlist Song from offline Music DB.
     
     - Parameter playlistName:   The stirng  contain Playlist Name.
     
     - Returns: Array of songs
     */
    
    public func getPlaylistSongoffline(playlistName : String) -> [String]{
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: self.offlineMusicPlaylistNewDatabaseURL.path) {
            try? fileManager.copyItem(at: Bundle.main.url(forResource: "MusicOfflinePlaylist", withExtension: "sqlite")!, to: self.offlineMusicPlaylistNewDatabaseURL)
        }
        let sqlQuery = "SELECT songs FROM playlist_info WHERE display_name = '\(playlistName)'"
        let database = SQDatabase.init(path: self.offlineMusicPlaylistNewDatabaseURL.path)
        print(self.offlineMusicPlaylistNewDatabaseURL.path)
        var songs = [String]()
        if !database.open() {
            return songs
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    songs =  [rs.stringForColumnIndex(0) ?? ""]
                }
                return songs
            }
            return songs
        }
    }
    
    
    /**
     Insert new folder or playlist info in local DB MusicOfflinePlaylist.sqlite
     
     - Parameter dict:   The Dict  contain info to store in DB.
     
     - Returns: Status of DB true or false.
     */
    public func insertNewInfoInMusicOfflinePlaylist(dict : [String : AnyObject]) -> Bool{
        var status = false
        let query = "INSERT INTO playlist_info (files, file_full_path,songs,display_name,privacy,user_id,parent_id,type) VALUES ('\(dict["files"] as! String)', '\(dict["file_full_path"] as! String)', '\(dict["songs"] as! String)', '\(dict["displayname"] as! String)', '\(dict["privacy"] as! String)', '\(dict["userid"] as! String)', '\(dict["parentid"] as! String)', '\(dict["type"] as! String)')"
        let fileURL = self.offlineMusicPlaylistNewDatabaseURL
        let database = SQDatabase.init(path: fileURL.path)
        
        if !database.open() {
            return status
        } else {
            if database.update(query) {
                status = true
            }
            
            return status
        }
    }
    
    /**
     update playlist info in Database table like add songs,rename.privacy.
     - Parameter dict:   The Dict  contain info to store in DB.
     
     - Returns: Status of DB true or false.
     */
    public func updateMusicInfo(updateValue : String,playlistName : String,updateParameters : String,type : String,uniqueValue : String ) -> Bool{
        let sqlQuery = "UPDATE playlist_info SET \(updateParameters) = '\(updateValue)' WHERE (\(uniqueValue) = '\(playlistName)'AND type = '\(type)')"
        let database = SQDatabase.init(path: self.offlineMusicPlaylistNewDatabaseURL.path)
        var isSuccess = false
        if !database.open() {
            return isSuccess
        } else {
            if database.update(sqlQuery) {
                
                isSuccess = true
            }
            
            return isSuccess
            
        }
    }
    
    /**
     Delete playlist info in Database table like add songs,rename.privacy.
     - Parameter itemID:   The name of item to delete
     - Parameters uniqueValue : unique identifier to delete item
     - Returns: Status of DB true or false.
     */
    public func deletePlaylistOrFolderOffline(name : String,uniqueValue : String) -> Bool{
        let sqlQuery = "Delete from playlist_info where \(uniqueValue)= '\(name)'"
        let database = SQDatabase.init(path: self.offlineMusicPlaylistNewDatabaseURL.path)
        var isSuccess = false
        if !database.open() {
            return isSuccess
        } else {
            if database.update(sqlQuery) {
                
                isSuccess = true
            }
            
            return isSuccess
            
        }
    }
    
    
    public func checkIfAnyTableExists() -> Bool {
        //favourite table is common in both v1 & v2 sqlites
        var exists = false
        var query = ""
        
        query = "SELECT * FROM room_controller"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return exists
        } else {
            defer{
                database.close()
            }
            
            if let rs = database.query(query) {
                while rs.next() {
                    exists = true
                    return exists
                }
            }
        }
        
        return exists
    }
    
    public func getRooomNameFormID(_ roomId : String) -> String{
        var roomName = ""
        let sqlQuery = "SELECT room_name FROM room_info where room_id='\(roomId)'"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return roomName
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    roomName = rs.stringForColumnIndex(0) ?? ""
                }
            }
        }
        
        return roomName
    }
    public func getParentRooomNameFormSubRoomID(_ roomId : String) -> String{
        var roomName = ""
        let sqlQuery = "SELECT ( SELECT room_name FROM room_info as `ac` WHERE `ap`.parent_room_id = `ac`.`room_id` ) as `rn` FROM room_info as `ap` WHERE  ap.room_id ='\(roomId)'"
        
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        if !database.open() {
            return roomName
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    roomName = rs.stringForColumnIndex(0) ?? ""
                }
            }
        }
        
        return roomName
    }
    //MARK: Pool Spa Methods
    public func getAllPoolSpaCategories(_ roomId: String) -> [PoolSpaCategory] {
        let sqlQuery = "SELECT automation_id, value FROM pool_spa where room_id = '\(roomId)'"
        var infoArray: [PoolSpaCategory] = []
        let database = SQDatabase.init(path: self.configurationDatabasePath)
        
        var temp1infoObjArr = [PoolSpaCategory]()
        var temp2infoObjArr = [PoolSpaCategory]()
        
        if !database.open() {
            return infoArray
        } else {
            if let rs = database.query(sqlQuery) {
                while rs.next() {
                    let infoObj = PoolSpaCategory()
                    var isDisble = false
                    var isDial = false
                    
                    if let automationId = rs.stringForColumnIndex(0) {
                        infoObj.unitId = automationId
                    }
                    
                    if let jsonVal = rs.stringForColumnIndex(1) {
                        infoObj.JsonVal = JSON.init(parseJSON: jsonVal)
                    }
                    
                    let jsonVal = infoObj.JsonVal[infoObj.unitId]
                    for item in jsonVal {
                        let key = item.0
                        let val = item.1
                        if key == "display_name" {
                            infoObj.title = val.stringValue
                        }
                        if key == "default_operation" {
                            infoObj.isSwitchOn = val.boolValue
                        }
                        if key == "is_disable" {
                            isDisble = val.boolValue
                        }
                        if key == "category" {
                            infoObj.deviceType = val.stringValue
                        }
                        
                        if key == "position" {
                            infoObj.position = val.intValue
                        }
                        
                        if key == "is_dial" {
                            isDial = val.boolValue
                            infoObj.isDial = isDial
                        }
                    }
                    
                    if !isDisble {
                        if !isDial && infoObj.deviceType != "switch" {
                            temp1infoObjArr.append(infoObj)
                        }
                        else {
                            temp2infoObjArr.append(infoObj)
                        }
                    }
                }
            }
            
            print("temp1 \(temp1infoObjArr.count) & \(temp2infoObjArr.count)")
            temp1infoObjArr.sort {
                item1, item2 in
                let str1 = item1.position
                let str2 = item2.position
                
                return str1 < str2
            }
            temp2infoObjArr.sort {
                item1, item2 in
                let str1 = item1.position
                let str2 = item2.position
                
                return str1 < str2
            }
            
            infoArray.append(contentsOf: temp1infoObjArr)
            infoArray.append(contentsOf: temp2infoObjArr)
            return infoArray
        }
        
    }
 }
 
 
