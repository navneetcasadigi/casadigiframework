// Copyright (c) 2017 CasaDigi. All rights reserved.

import Foundation

public class FeatureVersionInfo {
    public var packages: String = ""
    public var version: String = ""
    public var old_version: String = ""

    public init() {
        
    }
}

public class PackageVersionInfo {
    public var package: String = ""
    public var currentVersion: String = ""

    
    public init() {
        
    }
}

public class DeviceInfo {
    public var unitId: String = "0" //Feature ID (like VOD, MOD)
    public var moduleId: String = "0" // IP Address
    public var sceneId: String = "0" // Mood ID
    public var showVolumeBar: Bool = true
    public var remoteControlType: String = "NA" //DVD specific
    public var colorBgImage: String = ""

    
    public init() {
        
    }
}

public class ACDeviceInfo: DeviceInfo {
    public var minTemp: String?
    public var maxTemp: String?
    public var fanSpeed: String?
    public var displayName: String?
    public var isFanSpeed: Bool = true
    public var isOnOff: Bool = true
    public var isACTemp: Bool = true
    public var isUnitChanging: Bool = true
    public var isRoomTemp: Bool = true
    public var modeStr: String?
    public var position = 0
    public var defaultUnit = "C"
    public var isEcoMode = false
    public var fanSpeedArray: NSArray?

    
    public override init() {
        
    }
}

public class CurtainsInfoData {
    public var centerX: Double = 0.0
    public var centerY: Double = 0.0
    public var displayName: String = ""
    public var driverType: String = ""
    public var unitId: String = "0"
    public var moduleId: String = "0"
    public var groupCount: String = "0"
    public var curtainIds: String = "NA"
    public var room_image_name: String = ""
    public var position: Int = 0
    public var typeId: Int = 3 {
        didSet {
            if typeId == 0 {
                typeId = 3
            }
        }
    }
    
    public init() {
        
    }
}

public class ChannelInfoData: NSObject, NSCoding {
    public var channelCatId: String = ""
    public var channelNo: String = ""
    public var channelName: String = ""
    public var channelIconName: String = ""
    public var channelLanguageId: String = ""
    public var isFavourite = false
    public var fid: String? //This is just required to backend for removing from favourites. iPad has no need of this
    public var channel_id: String = "" //This key will remain constant for a channel in all room (In all STBs)
    
    required convenience public init(coder aDecoder: NSCoder) {
        self.init()
        self.channelCatId = aDecoder.decodeObject(forKey: "cat_id") as! String
        self.channelNo = aDecoder.decodeObject(forKey: "channel_no") as! String
        self.channelName = aDecoder.decodeObject(forKey: "channel_name") as! String
        self.channelIconName = aDecoder.decodeObject(forKey: "channel_icon") as! String
        self.channelLanguageId = aDecoder.decodeObject(forKey: "channel_lang_id") as! String
        self.fid = aDecoder.decodeObject(forKey: "f_id") as? String
        self.channel_id = aDecoder.decodeObject(forKey: "channel_id") as! String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(channelCatId, forKey: "cat_id")
        aCoder.encode(channelNo, forKey: "channel_no")
        aCoder.encode(channelName, forKey: "channel_name")
        aCoder.encode(channelIconName, forKey: "channel_icon")
        aCoder.encode(channelLanguageId, forKey: "channel_lang_id")
        aCoder.encode(fid, forKey: "f_id")
        aCoder.encode(channel_id, forKey: "channel_id")
    }

    
    public override init() {
        
    }
}

public class RemoteControlData {
    public var buttonId: String = ""
    public var name: String = ""
    public var command: String = ""
    public var position: Int = 0

    
    public init() {
        
    }
}

public class LanguageMappedChannelInfo: NSObject, NSCoding {
    public var languageId: String = ""
    public var channelsArray: [ChannelInfoData]!
    
    required convenience public init(coder aDecoder: NSCoder) {
        self.init()
        
        self.languageId = aDecoder.decodeObject(forKey: "lang_id") as! String
        self.channelsArray = aDecoder.decodeObject(forKey: "channel_array") as! [ChannelInfoData]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(languageId, forKey: "lang_id")
        aCoder.encode(channelsArray, forKey: "channel_array")
    }

    
    public override init() {
        
    }
}

public class TVShowsInfoData {
    public var sId: String = ""
    public var showId: String = ""
    public var showName: String = ""
    public var showPosterName: String = ""
    public var showNetwork: String = ""
    public var showSeasonCount: String = ""
    public var showLastAired: String = ""
    public var showAverageVote: String = ""
    public var showEpisodeCount: String = ""
    public var showLanguages: String = ""
    public var showDescription: String = ""
    public var showAdded_date: String = ""
    public var showPrivacy: String = "" // 2 for private & 3 for public
    public var showGenres: String = ""
    public var showCategory: String = ""
    public var showCast: String = ""
    public var isNew: Bool = false

    
    public init() {
        
    }
}

public class InsetLabel: UILabel {
    override public func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)))
    }
}

public class PVSearchCat {
    public var catName: String = ""
    public var catId: String = "0"

    
    public init() {
        
    }
}

public class PVSearchCatData {
    public var dataName: String = ""
    public var dataId: String = "0"

    
    public init() {
        
    }
}

public class FilterAppliedData {
    public var langName: String = ""
    public var privacy: String = ""

    
    public init() {
        
    }
}

public class SeasonInfoData {
    public var seasonId: String = ""
    public var seasonName: String = ""
    public var seasonNo: String = ""
    public var seasonEpisodeCount: String = "0"

    
    public init() {
        
    }
}

public class EpisodeInfoData {
    public var episodeId: String = ""
    public var episodeName: String = ""
    public var episodeNumber: String = ""
    public var episodeSeasonNumber: String = ""
    public var episodeDescription: String = ""
    public var episodeVoteAvg: String = ""
    public var episodeFileLocation: String = ""
    public var episodePosterName: String = ""
    public var episodeCrew: String = ""
    public var episodeGuestStars: String = ""
    public var episodeDirectors: String = ""
    public var episodeWriters: String = ""
    public var episodeDuration: String = ""
    public var episodeAirDate: String = ""

    
    public init() {
        
    }
}

public class ModlueInfoForVolume {
    public var moduleName: String = ""
    public var unitID: String = ""
    public var moduleId: String = ""
    public var volumeLevel: String = "0"
    public var isMute: Bool = false

    
    public init() {
        
    }
}

public class MovieInfoData: NSObject {
    public var movie_id: String = ""
    public var movie_poster: String = ""
    public var movie_director: String = ""
    public var movie_writer: String = ""
    public var movie_fileLocation: String = ""
    public var movie_overview: String = ""
    public var movie_parental_rating: String = ""
    public var movie_release_date: String = ""
    public var movie_name: String = ""
    public var movie_runtime: String = ""
    public var movie_dimension: String = ""
    public var movie_artist: String = ""
    public var movie_added_date: String = ""
    public var movie_privacy: String = ""
    public var movie_selected: String = ""
    public var movie_genre: String = ""
    public var movie_language: String = ""
    
    public override init() {
        
    }
}

public class MusicInfoData {
    public var musicId: String = ""
    public var musicAlbumId: String = ""
    public var musicTitle: String = ""
    public var musicAlbum: String = ""
    public var musicThumbImg: String = "MusicDefault"
    public var musicThumbImgLarge: String = "MusicDefault"
    public var musicFileName: String = ""
    public var musicDuration: String = ""
    public var musicArtist: String = ""
    public var musicYear: String = ""
    public var musicSubCatIds: String = ""
    public var musicAddedDate: String = ""
    public var musicReleaseDate: String = ""
    public var musicComposer : String = ""
    public var musicLyrisist : String = ""
    public var musicTrackNumber : String = ""
    public var isNew : Bool = false
    

    
    public init() {
        
    }
}

public class ThumbInfoData {
    public var thumbId: String = ""
    public var thumbTitle: String = ""
    public var thumbImg: String = "MusicDefault"
    public var thumbReleaseYear: Int = 0
    public var thumbDateAdded: String = ""
    public var albumArtists : String = ""
    public var thumbUpdateDate : String = ""
    public var isNew: Bool = false
    

    
    public init() {
        
    }
}

public class AlbumInfoData {
    public var albumId: String = ""
    public var albumTitle: String = ""
    public var albumYear: String = ""
    public var albumThumbImg: String = "MusicDefault"
    public var albumSongsList = [MusicInfoData]()
    public var albumDateAdded : String = ""

    
    public init() {
        
    }
}

public class PlaylistInfoData {
    public var playlistId = "0"
    public var displayName = ""
    public var name: String {
        set (newValue) {
            displayName = newValue.replacingOccurrences(of: ".m3u", with: "")
        }
        
        get {
            if displayName.contains("%")
            {
                return (displayName + ".m3u")
            }
            // return displayName
            return (displayName + ".m3u").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
    }
    public var isPrivate: Bool = false
    public var userName: String = ""
    
    public init(name playlistName: String, isPrivate type: Bool) {
        name = playlistName
        isPrivate = type
    }
    
    public init(id: String, name playlistName: String, isPrivate type: Bool) {
        playlistId = id
        name = playlistName
        isPrivate = type
    }
    
    public init(name playlistName: String, username user_name: String, isPrivate type: Bool) {
        name = playlistName
        isPrivate = type
        userName = user_name
    }
    
    public func accessInString() -> String {
        return isPrivate ? "private" : "public"
    }

    
    public init() {
        
    }
}

public class WebRadioStationInfo : NSObject{
    public var stationId : Int = -1
    public var categoryType : String = ""
    public var stationNAme : String = ""
    public var sub_Id : Int = -1
    public var stationDescrition : String = ""
    public var stattionLogoUrl : String = ""
    public var is_favorite : Bool = false
    public var type : String = ""
    public var stationWebsite : String = ""
    public var stationImageName : String =  ""
    public var stationPlayUrl : String = ""
    public var isStationFav : Bool = false
    public var fid: String? //This is just required to backend for removing from favourites. iPad has no need of this
    
    override public init() {
        
    }
}

public class WebRadioLanguageInfo : NSObject{
    public var langId : String = ""
    public var langType : String = ""
    public var langNAme : String = ""
    
    override public init() {
        
    }
}

public class WebRadioCategoryInfo : NSObject{
    
    public var id : Int = -1
    public var categoryType : String = ""
    public var categoryNAme : String = ""
    public var regionID : Int = -1
    public var genreID : Int = -1
    
    public init(name catName: String, catType type: String) {
        self.categoryNAme = catName
        self.categoryType = type
    }
    
    override public init() {
        super.init()
        
    }
}

public class DriveExplodedData: NSObject {
    public var name: String {
        set (newValue) {
            fileName = newValue.replacingOccurrences(of: ".m3u", with: "")//.removingPercentEncoding!
        }
        
        get {
            return (fileName! + ".m3u").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
    }
    
    public var itemId : String = ""
    public var parentid : String = ""
    public var fileorignalName : String? = nil
    public var fileName: String? = nil
    public var isChecked: Bool = false
    public var fileSize:NSNumber = 0
    public var folderName: String? = nil
    public var folderOrignalName = ""
    public var file_full_Path: String = ""
    public var file_trailer_Path: String = ""
    public var folderChildrenArr = [Dictionary<String, AnyObject>]()
    public var fileSongArr = [String]()
    public var isPrivate: Bool = false
    public var userID: String = "1"
    public var fileActualName: String = ""
    public var type : String = ""
    public init(name folderNme: String, isPrivate type: Bool) {
        self.folderName = folderNme
    }
    
    public init(playListNme: String, isPrivate type: Bool) {
        self.fileName = playListNme
        self.isPrivate = type
    }
    
    override public init() {
    
    }
    
    public func accessInString() -> String {
        return isPrivate ? "private" : "public"
    }
}

public class PhotosDriveData {
    public var albumName: String? = nil
    public var folderName: String? = nil
    public var file_full_Path: String = ""
    public var folderChildrenArr = [Dictionary<String, AnyObject>]()
    public var photosArray: [PhotoInfoData]?

    
    public init() {
        
    }
}

public class PhotoInfoData {
    public var photoImageName: String?
    public var photoLinkPath: String?
    public var photoOriginalPath: String?
    public var photoAlbumPath: String?
    public var isPhotoSelected = false
    public var photo_id = ""
    public var user_id = ""
    public var privacy = ""

    
    public init() {
        
    }
}

public class RokuChannelInfo {
    public var channelName: String? // Name of Channel
    public var channelId: String? // Roku ID of Channel
    public var channelPosition: Int = 0 // Position of Channel
    public var channelFId: String = "" // Unique id used for favorite feature. It is a local Database row id.
    public var isFavourite: Bool = false // is Channel favourite

    
    public init() {
        
    }
}

public class ZonesInfoData {
    public var zoneRoomId: String = ""
    public var zoneName: String = ""
    public var zoneFloorName: String = ""
    public var zoneUnitId: String = "-1"
    public var zoneModuleId: String = "-1"
    public var zoneFMUnitId: String = ""
    public var zoneAirPlayUnitId: String = ""
    public var zoneRadioUnitId: String = ""
    public var zoneVolumeLevel: String = "0"
    public var zoneIsMute: Bool = false
    public var zoneIsReachable: Bool = true

    
    public init() {
        
    }
}

public class SubtitleInfo {
    public var name: String = ""
    public var ip: String = ""
    public var type: String = "source"
    public var isChecked: Bool = false
    public var deviceName: String = ""
    public var file_full_Path: String = ""
    public var languageChoosen: String = ""
    public var folderChildrenArr = [Dictionary<String, AnyObject>]()

    
    public init() {
        
    }
}

public class SearchTagsData {
    public var people = [String]()
    public var keyword = [String]()
    public var location = [String]()
    public var date = [String]()

    
    public init() {
        
    }
}

public class PVInfoData {
    public var id: String = "0"
    public var video_name: String = ""
    public var filename: String = ""
    public var iconName: String = ""
    public var description: String = ""
    public var category_name: String = ""
    public var tags: String = ""
    public var duration: String = ""
    public var title: String = ""
    public var owner_id: String = ""
    public var is_private: String = ""
    public var creation_date: String = ""
    public var people = [String]()
    public var location = [String]()
    public var keywords = [String]()
    public var date = [String]()

    
    public init() {
        
    }
}

public class CatSubcatMappedData {
    public var catId: String = ""
    public var catName: String = ""
    public var subCatArr: [Dictionary<String, String>]!

    
    public init() {
        
    }
}

public class FMInfoData {
    public var stationId: String = "0"
    public var moduleId: String = "0"
    public var displayName: String = ""
    public var frequency: String = ""
    public var iconName: String = ""

    
    public init() {
        
    }
}

public class FMGroupInfo {
    public var stationId: String = "-1"
    public var masterRoomId: String = "-1"
    public var roomId: String = "-1"
    
    public init(station stationId: String, master masterId: String, room roomId:String) {
        self.stationId = stationId
        self.masterRoomId = masterId
        self.roomId = roomId
    }

    public init() {
        
    }
}

public class UserInfoData: NSObject, NSCoding {
    public var userId: String = ""
    public var userName: String = ""
    public var userPassword: String = ""
    public var userIconName: String = ""
    public var userDisplayName: String = ""
    public var userIsAdmin: Bool = false
    
    required convenience public init(coder aDecoder: NSCoder) {
        self.init()
        
        self.userId = aDecoder.decodeObject(forKey: "id") as! String
        self.userName = aDecoder.decodeObject(forKey: "username") as! String
        self.userPassword = aDecoder.decodeObject(forKey: "password") as! String
        self.userDisplayName = aDecoder.decodeObject(forKey: "displayname") as! String
        self.userIconName = aDecoder.decodeObject(forKey: "iconname") as! String
        self.userIsAdmin = aDecoder.decodeBool(forKey: "isAdmin")
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(userId, forKey: "id")
        aCoder.encode(userName, forKey: "username")
        aCoder.encode(userPassword, forKey: "password")
        aCoder.encode(userDisplayName, forKey: "displayname")
        aCoder.encode(userIconName, forKey: "iconname")
        aCoder.encode(userIsAdmin, forKey: "isAdmin")
    }
    
    public override init() {
        
    }
}

public class SurveillanceInfoData {
    public var roomId: String = "0"
    public var roomName: String = ""
    public var ipAddress: String = ""
    public var imageURL: String = ""
    public var lightId: String = ""
    public var moodId: String = ""
    public var isOnline: Bool = false
    public var offlineImageName: String = ""

    
    public init() {
        
    }
}

public class AmbienceInfoData {
    public var sceneId: String = "0"
    public var displayName: String = ""
    public var defaultImageName: String = ""
    public var selectedImageName: String = ""
    public var moduleId: String = ""
    public var moduleIdString: String = ""
    public var type: String = ""

    
    public init() {
        
    }
}

public class FileInfoData: NSObject {
    public var sourcePath: String = ""
    public var destinationPath: String = ""
    public var moduleType: String = ""

    public override init() {
        
    }
}

public class UtilityInfoData {
    public var unitId: String = ""
    public var moduleId: String = ""
    public var sceneId: String = ""
    public var displayName: String = ""
    public var driverName: String = ""
    public var driverType: String = ""
    public var imageName: String = ""
    public var isON: Bool = false
    public var isActive: Bool = false
    public var param3: String = ""
    public var param4: String = "NA"
    public var param5: String = "NA"
    public var param6: String = ""
    //Adding some new parametres. Need to get buttons from studio instead of hardcode buttons.
    public var buttonInfo: JSON?
    public var buttonsInfoArr: Array<JSON>?
    public var position: Int = 0
    public var buttonType : utilityButtonType = .button

    
    public init() {
        
    }
}

public class RoomsStatusInfoData {
    public var lightStatus: String = "off"
    public var acStatus: String = "off"
    public var tvStatus: String = "off"

    
    public init() {
        
    }
}


public class AdSettingsInfoData {
    public var unitId: String = "0"
    public var imageName: String = ""
    public var moduleId: String = "0"
    public var sceneId: String = "0"
    public var displayName: String = ""
    public var deviceCategoryName: String = ""
    public var deviceBrandName: String = ""
    public var driverName: String = ""
    public var driverFileName: String = ""
    public var sceneModule: String = "0"
    public var isSetAdvCtrl: String = "0"
    public var titleLeft: String?
    public var titleCenter: String?
    public var titleRight: String?
    public var customCommandLeft: String = ""
    public var customCommandCenter: String = ""
    public var customCommandRight: String = ""
    public var showVolumeBar: Bool = true
    public var buttonInfo: JSON?
    public var buttonsInfoArr: Array<JSON>?
    public var position: Int = 0
    public var param1: String = ""
    public var param2: String = ""
    public var param3: String = ""
    public var param4: String = ""
    public var param6: String = ""

    
    public init() {
        
    }
}

public class PoolSpaCategory {
    public var unitId = "0"
    public var deviceType = ""
    public var JsonVal: JSON = JSON()
    public var title = ""
    public var isSwitchOn: Bool?
    public var position = 0
    public var isDial = false
    public var switchStatus = "off"
    public var temperatureStatus: String?
    public var temperatureUnit: String?
    public var needletemperatureStatus: String?

    
    public init() {
        
    }
}

public enum PlayingSource {
    case music
    case fm
    case airPlay
    case television
    case digitalcinema
    case movies
    case cinemaSever
    case personalVideos
    case tvShows
    case dvd
    case appleTv
    case none
    case webRadio
}

public enum utilityButtonType {
    case toggle
    case button
}
