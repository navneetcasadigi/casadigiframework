//
//  CDDatabaseInterfaceManager.swift
//  CasadigiFramework
//
//  Created by Navneet Singh Gill on 1/22/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit

public protocol CDDatabaseInterfaceManagerDatasource {
    func getLanguageInfoForCDDatabaseInterfaceManager() -> [String: Any]
    func getLanguageCodeForCDDatabaseInterfaceManager() -> String
}

public protocol CDDatabaseInterfaceManagerDelegate {
    func updateUIforCurrentRoom()
}

public class CDDatabaseInterfaceManager: NSObject {
    
    public static let shared = CDDatabaseInterfaceManager()
    
    public var datasource: CDDatabaseInterfaceManagerDatasource?
    public var delegate: CDDatabaseInterfaceManagerDelegate?
    
    func languageInfo() -> [String: Any]? {
        return datasource?.getLanguageInfoForCDDatabaseInterfaceManager()
    }
    
    func languageCode() -> String? {
        return datasource?.getLanguageCodeForCDDatabaseInterfaceManager()
    }
    
}
