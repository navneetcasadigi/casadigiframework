//
//  CasadigiFramework.h
//  CasadigiFramework
//
//  Created by Navneet Singh Gill on 1/14/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CasadigiFramework.
FOUNDATION_EXPORT double CasadigiFrameworkVersionNumber;

//! Project version string for CasadigiFramework.
FOUNDATION_EXPORT const unsigned char CasadigiFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CasadigiFramework/PublicHeader.h>


