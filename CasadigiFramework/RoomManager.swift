// Copyright (c) 2017 CasaDigi. All rights reserved.

import UIKit
import CasadigiFramework

final class RoomManager {
    static let shared: RoomManager = RoomManager()
    
    private init() {
        self.loadAllRooms()
    }

    var roomInfoArray: [[String:Any]]!
    var floorInfoArray: [String]!
    var homeInfoArray: [String]!
    var currentRoomID: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "CurrentRoomID")
        }

        get {
            return UserDefaults.standard.string(forKey: "CurrentRoomID") ?? "1"
        }
    }
    
    var currentSubRoomID: String {
        set {
            UserDefaults.standard.set(newValue, forKey: "CurrentSubRoomID")
             UserDefaults.standard.synchronize()
        }
        
        get {
            return UserDefaults.standard.string(forKey: "CurrentSubRoomID") ?? "0"
        }
    }
    
    var currentRoom: String!
    var currentRoomSubroomInfo: [[String:String]]!
    var currentRoomDashboardSubroomInfo: [[String:Any]]!
    var currentRoomFeature: [String]!
    var currentSubRoomFeature: [String]!
    var currentRoomFeatureInfo: [(String, String)]!
    
    var currentRoomVideoFeatureInfo: [(String, String)]!
    var currentRoomAudioFeatureInfo: [(String, String)]!
    
    var currentRoomVideoFeature: [String]!
    var currentRoomAudioFeature: [String]!

 
    func loadAllRooms() {
        if isConfigurationAvailable {
            let infoTuple = databaseInterface.getRoomsInfoForCurrentDevice()
            databaseInterface.isPartnerLogoPermissionAvailable()
            self.roomInfoArray = infoTuple.roomInfo
            self.floorInfoArray = infoTuple.floorInfo
            self.homeInfoArray = infoTuple.homeFeatureArray
        }
    }
    
    func loadDefaultRoom() {
        for (index, infoDic) in self.roomInfoArray.enumerated() where (infoDic["RoomId"] as! String) == self.currentRoomID {
            self.loadRoomAtIndex(index,subroomId: currentSubRoomID)

            return
        }
        self.loadRoomAtIndex(0)
    }
    
    func loadRoomWithRoomId(_ roomId: String,subroomId:String = "0") {
        for (index, infoDic) in self.roomInfoArray.enumerated() where (infoDic["RoomId"] as! String) == roomId {
            self.loadRoomAtIndex(index,subroomId: subroomId)

            return
        }
    }
    
  

    func loadRoomAtIndex(_ index: Int,subroomId:String = "0") {

        if !self.roomInfoArray.isEmpty {
            let infoDic                 = self.roomInfoArray[index]
            self.currentRoomID          = infoDic["RoomId"] as! String
            self.currentSubRoomID       = subroomId
            self.currentRoom            = infoDic["RoomName"] as! String
            self.currentRoomSubroomInfo = infoDic["SubRoomInfo"] as! [[String:String]]
            self.currentRoomDashboardSubroomInfo = infoDic["DashboardSubRoomInfo"] as! [[String:Any]]
            self.currentRoomFeature     = infoDic["RoomFeatures"] as! [String]
            
            if self.currentSubRoomID != "0"{
                for (_,subroom) in self.currentRoomDashboardSubroomInfo.enumerated(){
                    if subroom["SubRoomId"] as! String == self.currentSubRoomID {
                        let subFeature = subroom["SubRoomFeature"]! as! [String]
                        self.currentRoomFeature.append(contentsOf: subFeature)
                        break
                    }
                }
            }
            
            self.currentRoomFeatureInfo = infoDic["RoomFeaturesInfo"] as! [(String, String)]
            
            CDDatabaseInterfaceManager.shared.delegate?.updateUIforCurrentRoom()
        }
    }
    
    func reloadRooms() {
        self.roomInfoArray?.removeAll()
        self.floorInfoArray?.removeAll()
        
        DispatchQueue.main.async(execute: {
            () -> Void in

            let infoTuple = databaseInterface.getRoomsInfoForCurrentDevice()
            self.roomInfoArray = infoTuple.roomInfo
            self.floorInfoArray = infoTuple.floorInfo
            self.loadDefaultRoom()
        })
    }
    
    func getRoomsInfoForModuleType(_ moduleType: String) -> [[String:String]] {
        var moduleArray = [[String:String]]()
        
        if !self.currentRoomFeatureInfo.isEmpty {
            if self.currentRoomSubroomInfo.count > 0{
                for subRoomInfo in self.currentRoomSubroomInfo {
                    for infoTuple in self.currentRoomFeatureInfo where infoTuple.0 == (subRoomInfo["SubRoomId"]) && moduleType == infoTuple.1 {
                        moduleArray.append([
                            "RoomId": subRoomInfo["SubRoomId"]!,
                            "RoomName": subRoomInfo["SubRoomName"]!
                            ])
                    }
                }
            }else{
                for infoTuple in self.currentRoomFeatureInfo where infoTuple.1 == moduleType {
                    moduleArray.append([
                        "RoomId": infoTuple.0,
                        "RoomName": currentRoom
                        ])
                }
            }
        } else {
            for infoTuple in self.currentRoomFeatureInfo where infoTuple.1 == moduleType {
                moduleArray.append([
                    "RoomId": infoTuple.0,
                    "RoomName": currentRoom
                ])
            }
        }
        
        return moduleArray
    }
    
    func isModuleTypeEnabled(_ moduleType: String) -> Bool {
        if !self.roomInfoArray.isEmpty {
            if self.currentSubRoomID == "0"
            {
                for infoTuple in self.currentRoomFeatureInfo where moduleType == infoTuple.1 {
                    return true
                }
            } else{ // for subroom inplementation
                /*
                 
                 */
                for infoTuple in self.currentRoomFeatureInfo where (moduleType == infoTuple.1) && (infoTuple.0 == self.currentSubRoomID) {
                    return true
                }
            }
        }
        
        return false
    }
    func isVideoModuleTypeEnabled(_ moduleType: String) -> Bool {
        if !self.roomInfoArray.isEmpty {
            if self.currentSubRoomID == "0"
            {
                for infoTuple in self.currentRoomFeatureInfo where moduleType == infoTuple.1 {
                    return true
                }
            } else{ // for subroom inplementation
                /*
                 
                 */
                for infoTuple in self.currentRoomFeatureInfo where (moduleType == infoTuple.1) && (infoTuple.0 == self.currentSubRoomID) {
                    return true
                }
            }
        }
        
        return false
    }
}
