//
//  Extension.swift
//  CasadigiFramework
//
//  Created by Navneet Singh Gill on 1/14/19.
//  Copyright © 2019 Paragon Business Solutions Ltd. All rights reserved.
//

import UIKit
import CasadigiFramework

extension Int {
    func toBool() ->Bool {
        switch self {
        case 0:
            return false
            
        default:
            return true
        }
    }
}

extension String {
    func toBool() -> Bool {
        switch self {
        case "True", "true", "yes", "1":
            return true
            
        default:
            return false
        }
    }
}
